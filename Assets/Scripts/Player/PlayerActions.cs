using UnityEngine;

namespace KickMaster
{
    public class PlayerActions : MonoBehaviour
    {
        [SerializeField] private Animation _legHitAnimation;
        [SerializeField] private Input _input;
        [SerializeField] private PlayerAction[] _actions;

        private void OnEnable()
        {
            _input.FingerUp += OnFingerUp;
        }

        private void OnDisable()
        {
            _input.FingerUp -= OnFingerUp;
        }

        private void OnFingerUp()
        {
            _legHitAnimation.Play();

            foreach (var action in _actions)
            {
                action.ApplyOnce();
            }
        }
    }
}