using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using KickMaster.StateMachine;

namespace KickMaster
{
    public class Kick : PlayerAction
    {
        [SerializeField] private float _force;

        [SerializeField] private List<Kickable> _kickables = new List<Kickable>();
        [SerializeField] private List<Projectile> _projectiles = new List<Projectile>();
        [SerializeField] private List<ThrowingState> _throwingStates = new List<ThrowingState>();

        private void OnEnable()
        {
            foreach (var throwingState in _throwingStates)
            {
                throwingState.ProjectileCreated += AddProjectile;
            }

            foreach (var projectile in _projectiles)
            {
                projectile.Destroyed += RemoveProjectile;
            }

            foreach (var kickable in _kickables)
            {
                kickable.Destroyed += RemoveKickable;
            }
        }

        private void OnDisable()
        {
            foreach (var throwingState in _throwingStates)
            {
                throwingState.ProjectileCreated -= AddProjectile;
            }

            foreach (var projectile in _projectiles)
            {
                projectile.Destroyed -= RemoveProjectile;
            }

            foreach (var kickable in _kickables)
            {
                kickable.Destroyed -= RemoveKickable;
            }
        }

        private void AddProjectile(Projectile projectile)
        {
            _projectiles.Add(projectile);
            projectile.Destroyed += RemoveProjectile;
        }

        private void RemoveProjectile(Projectile projectile)
        {
            projectile.Destroyed -= RemoveProjectile;
            _projectiles.Remove(projectile);
        }

        private void RemoveKickable(Kickable kickable)
        {
            kickable.Destroyed -= RemoveKickable;
            _kickables.Remove(kickable);
        }

        public override void ApplyOnce()
        {
            foreach (var projectile in _projectiles)
            {
                if (projectile.IsInRange(transform) == false)
                {
                    continue;
                }

                Vector3 forceDirection = projectile.transform.position - transform.position;

                projectile.Throw(forceDirection * _force);
            }

            foreach (var kickable in _kickables)
            {
                if (kickable.IsInRange(transform) == false)
                {
                    continue;
                }

                Vector3 forceDirection = kickable.transform.position - transform.position;
                kickable.Kick(forceDirection.normalized * _force);
            }
        }
    }
}
