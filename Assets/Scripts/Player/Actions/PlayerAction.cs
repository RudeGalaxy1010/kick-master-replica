using UnityEngine;

namespace KickMaster
{
    public abstract class PlayerAction : MonoBehaviour
    {
        public abstract void ApplyOnce();
    }
}