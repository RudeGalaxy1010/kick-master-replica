using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    public class Player : MonoBehaviour
    {
        public event UnityAction Died;

        [SerializeField] private int _health;

        public int Health => _health;

        public void TakeDamage(int value)
        {
            _health -= value;

            if (_health <= 0)
            {
                Die();
            }
        }

        public void Kill()
        {
            _health = 0;
            Die();
        }

        private void Die()
        {
            Died?.Invoke();
        }
    }
}
