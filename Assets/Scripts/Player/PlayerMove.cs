using UnityEngine;

namespace KickMaster
{

    [RequireComponent(typeof(CharacterController))]
    public class PlayerMove : MonoBehaviour
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _rotationSpeed;
        [SerializeField] private Joystick _joystick;

        private CharacterController _controller;

        private void Start()
        {
            _controller = GetComponent<CharacterController>();
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            if (_joystick.Direction != Vector2.zero)
            {
                var joystickPosition = new Vector3(_joystick.Direction.x, 0, _joystick.Direction.y);
                _controller.Move(joystickPosition * _moveSpeed * Time.deltaTime);
            }
        }
    }
}
