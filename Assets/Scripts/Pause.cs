using UnityEngine;

namespace KickMaster
{
    public class Pause : MonoBehaviour
    {
        [SerializeField] private Level _level;

        private void Awake()
        {
            ResetPause();
        }

        private void OnEnable()
        {
            _level.Won += SetPause;
            _level.Defeated += SetPause;
        }

        private void OnDisable()
        {
            _level.Won -= SetPause;
            _level.Defeated -= SetPause;
        }

        private void SetPause()
        {
            Time.timeScale = 0;
        }

        private void ResetPause()
        {
            Time.timeScale = 1;
        }
    }
}
