using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    [RequireComponent(typeof(Collider))]
    public abstract class Projectile : MonoBehaviour
    {
        public event UnityAction<Projectile> Destroyed;

        [SerializeField] private float _maxInteractionDistance;
        [SerializeField] private float _destroyDistance;

        private Vector3 _startPosotion;

        private void Awake()
        {
            _startPosotion = transform.position;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Hit(collision.gameObject);
        }

        private void Update()
        {
            if (Vector3.Distance(_startPosotion, transform.position) >= _destroyDistance)
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            Destroyed?.Invoke(this);
        }

        public bool IsInRange(Transform source)
        {
            return Vector3.Distance(transform.position, source.position) <= _maxInteractionDistance;
        }

        public abstract void Throw(Vector3 force);

        protected abstract void Hit(GameObject hit);
    }
}
