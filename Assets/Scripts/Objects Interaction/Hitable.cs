using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    [RequireComponent(typeof(Collider))]
    public class Hitable : MonoBehaviour
    {
        public event UnityAction Hited;

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.TryGetComponent(out Projectile hitObject))
            {
                Hited?.Invoke();
            }
        }
    }
}
