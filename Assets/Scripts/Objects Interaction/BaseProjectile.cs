using UnityEngine;

namespace KickMaster
{ 
    public class BaseProjectile : Projectile
    {
        private Rigidbody _rigidbody;

        protected override void Hit(GameObject hit)
        {
            if (hit.TryGetComponent(out Enemy enemy))
            {
                Destroy(gameObject);
            }
        }

        public override void Throw(Vector3 force)
        {
            if (_rigidbody == null)
            {
                _rigidbody = gameObject.AddComponent<Rigidbody>();
            }

            _rigidbody.AddForce(force, ForceMode.Impulse);
        }
    }
}
