using UnityEngine;

namespace KickMaster
{
    public class BossProjectile : Projectile
    {
        [SerializeField] private int _damage;

        private Rigidbody _rigidbody;

        protected override void Hit(GameObject hit)
        {
            if (hit.TryGetComponent(out Player player))
            {
                player.TakeDamage(_damage);
                Destroy(gameObject);
            }
        }

        public override void Throw(Vector3 force)
        {
            if (gameObject.TryGetComponent(out Rigidbody rigidbody) == false)
            {
                _rigidbody = gameObject.AddComponent<Rigidbody>();
            }

            if (_rigidbody.velocity.magnitude > 0)
            {
                _rigidbody.velocity = Vector3.zero;
                //force = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition + force);
            }

            _rigidbody.AddForce(force, ForceMode.Impulse);
        }
    }
}
