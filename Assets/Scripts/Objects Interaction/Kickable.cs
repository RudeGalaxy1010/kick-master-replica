using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    [RequireComponent(typeof(Rigidbody))]
    public class Kickable : MonoBehaviour
    {
        public event UnityAction<Kickable> Destroyed;
        public event UnityAction Kicked;

        [SerializeField] private float _maxDistance;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnDestroy()
        {
            Destroyed?.Invoke(this);
        }

        public bool IsInRange(Transform source)
        {
            return Vector3.Distance(source.position, transform.position) <= _maxDistance;
        }

        public void Kick(Vector3 force)
        {
            _rigidbody.AddForce(force, ForceMode.Impulse);
            Kicked?.Invoke();
        }
    }
}
