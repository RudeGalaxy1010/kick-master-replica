using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    public class Input : MonoBehaviour
    {
        public event UnityAction FingerUp;

        private bool _isTouch;

        private void Update()
        {
            HandleTouchUp();
        }

        private void HandleTouchUp()
        {
#if UNITY_EDITOR
            if (UnityEngine.Input.GetMouseButton(0))
#else
            if (UnityEngine.Input.touchCount > 0)
#endif
            {
                _isTouch = true;
            }
            else if (_isTouch == true)
            {
                _isTouch = false;
                FingerUp?.Invoke();
            }
        }
    }
}
