using UnityEngine;

namespace KickMaster
{
    public class EndScreens : MonoBehaviour
    {
        [SerializeField] private Level _level;
        [SerializeField] private GameObject _winScreen;
        [SerializeField] private GameObject _defeatScreen;

        private void OnEnable()
        {
            _level.Won += ShowWinScreen;
            _level.Defeated += ShowDefeatScreen;
        }

        private void OnDisable()
        {
            _level.Won -= ShowWinScreen;
            _level.Defeated -= ShowDefeatScreen;
        }

        private void ShowWinScreen()
        {
            _winScreen.SetActive(true);
        }

        private void ShowDefeatScreen()
        {
            _defeatScreen.SetActive(true);
        }
    }
}
