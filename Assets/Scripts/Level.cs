using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace KickMaster
{
    public class Level : MonoBehaviour
    {
        public event UnityAction Won;
        public event UnityAction Defeated;

        [SerializeField] private List<Enemy> _enemies;
        [SerializeField] private Player _player;

        private void OnEnable()
        {
            foreach (var enemy in _enemies)
            {
                enemy.Died += OnEnemyDied;
            }

            _player.Died += OnPlayerDied;
        }

        private void OnDisable()
        {
            foreach (var enemy in _enemies)
            {
                enemy.Died -= OnEnemyDied;
            }

            _player.Died -= OnPlayerDied;
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void OnEnemyDied(Enemy enemy)
        {
            _enemies.Remove(enemy);

            if (_enemies.Count == 0)
            {
                Won?.Invoke();
            }
        }

        private void OnPlayerDied()
        {
            Defeated?.Invoke();
        }
    }
}
