using UnityEngine;

namespace KickMaster.StateMachine
{
    public class DetectionTransition : Transition
    {
        [SerializeField] private float _targetDetectionDistance;

        private void Update()
        {
            if (Vector3.Distance(transform.position, Target.transform.position) <= _targetDetectionDistance)
            {
                NeedTransit = true;
            }
        }
    }
}
