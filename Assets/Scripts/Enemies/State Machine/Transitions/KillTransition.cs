using UnityEngine;

namespace KickMaster.StateMachine
{
    public class KillTransition : Transition
    {
        [SerializeField] private float _transitionRange;
        [SerializeField] protected float _spreadRange;

        private void Start()
        {
            _transitionRange += Random.Range(-_spreadRange, _spreadRange);
        }

        private void Update()
        {
            if (Vector3.Distance(transform.position, Target.transform.position) < _transitionRange)
            {
                NeedTransit = true;
            }
        }
    }
}
