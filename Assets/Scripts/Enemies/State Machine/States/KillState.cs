using System.Collections;
using UnityEngine;

namespace KickMaster.StateMachine
{
    public class KillState : State
    {
        private const float _animationDelay = 1f;

        [SerializeField] private Animator _animator;

        private void OnEnable()
        {
            _animator.SetTrigger("Kick");
            StartCoroutine(KillWithDelay(_animationDelay));
        }

        private IEnumerator KillWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            Target.Kill();
        }
    }
}
