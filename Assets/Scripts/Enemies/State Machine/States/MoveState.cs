using UnityEngine;

namespace KickMaster.StateMachine
{
    public class MoveState : State
    {
        [SerializeField] private float _speed;
        [SerializeField] private Animator _animator;

        private void OnEnable()
        {
            _animator.SetBool("IsWalking", true);
        }

        private void Update()
        {
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, _speed * Time.deltaTime);
        }
    }
}
