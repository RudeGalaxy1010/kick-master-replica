using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace KickMaster.StateMachine
{
    public class ThrowingState : State
    {
        private const float animationDelay = 0.8f;

        public event UnityAction<Projectile> ProjectileCreated;

        [SerializeField] private Projectile _projectilePrefab;
        [SerializeField] private float _delay;
        [SerializeField] private Transform _holdPoint;
        [SerializeField] private float _force;
        [SerializeField] private Animator _animator;

        private Coroutine _throwCoroutine;

        private void OnEnable()
        {
            _throwCoroutine = StartCoroutine(StartThrowing());
        }

        private void OnDisable()
        {
            if (_throwCoroutine != null)
            {
                StopCoroutine(_throwCoroutine);
                _throwCoroutine = null;
            }
        }

        private IEnumerator StartThrowing()
        {
            while (true)
            {
                yield return new WaitForSeconds(_delay - animationDelay);
                _animator.SetTrigger("Throw");
                yield return new WaitForSeconds(animationDelay);
                Projectile projectile = CreateProjectile();
                ThrowProjectile(projectile);
            }
        }

        private Projectile CreateProjectile()
        {
            Projectile projectile = Instantiate(_projectilePrefab, _holdPoint.position, Quaternion.identity);
            ProjectileCreated?.Invoke(projectile);
            return projectile;
        }

        private void ThrowProjectile(Projectile projectile)
        {
            Vector3 forceDirection = Target.transform.position - _holdPoint.transform.position + Vector3.up;
            Vector3 forceValue = forceDirection.normalized * Vector3.Distance(Target.transform.position, _holdPoint.transform.position);
            projectile.Throw(forceValue * _force);
        }
    }
}
