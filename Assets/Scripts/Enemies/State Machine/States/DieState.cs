using System.Collections;
using UnityEngine;

namespace KickMaster.StateMachine
{
    public class DieState : State
    {
        private const float _animationDelay = 4f;

        [SerializeField] private Animator _animator;

        private void OnEnable()
        {
            _animator.SetTrigger("Die");
            StartCoroutine(DieWithDelay(_animationDelay));
        }

        private IEnumerator DieWithDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            GetComponent<Enemy>().Die();
        }
    }
}