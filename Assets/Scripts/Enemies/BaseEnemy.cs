using UnityEngine;

namespace KickMaster
{
    [RequireComponent(typeof(Kickable), typeof(Hitable))]
    public class BaseEnemy : Enemy
    {
        [SerializeField] private Animator _animator;

        private Kickable _kickable;
        private Hitable _hitable;

        private void Awake()
        {
            _hitable = GetComponent<Hitable>();
            _kickable = GetComponent<Kickable>();
        }

        private void OnEnable()
        {
            _kickable.Kicked += Die;
            _hitable.Hited += Die;
        }

        private void OnDisable()
        {
            _kickable.Kicked -= Die;
            _hitable.Hited -= Die;
        }

        public override void Die()
        {
            _health = 0;
            _animator.SetTrigger("Die");
            Destroy(gameObject, 2.5f);
        }
    }
}
