using UnityEngine;
using UnityEngine.Events;

namespace KickMaster
{
    [RequireComponent(typeof(Collider))]
    public abstract class Enemy : MonoBehaviour
    {
        public event UnityAction<Enemy> Died;

        [SerializeField] protected int _health;
        [SerializeField] protected Player _target;

        public Player Target => _target;
        public int Health => _health;

        private void OnDestroy()
        {
            Died?.Invoke(this);
        }

        public abstract void Die();
    }
}