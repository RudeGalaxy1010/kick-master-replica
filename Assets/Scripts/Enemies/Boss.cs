using UnityEngine;

namespace KickMaster
{
    [RequireComponent(typeof(Hitable))]
    public class Boss : Enemy
    {
        private Hitable _hitable;

        private void Awake()
        {
            _hitable = GetComponent<Hitable>();
        }

        private void OnEnable()
        {
            _hitable.Hited += () => TakeDamage(1);
        }

        private void OnDisable()
        {
            _hitable.Hited -= () => TakeDamage(1);
        }

        public void TakeDamage(int value)
        {
            _health -= value;

            if (_health <= 0)
            {
                Die();
            }
        }

        public override void Die()
        {
            Destroy(gameObject, 2f);
        }
    }
}
