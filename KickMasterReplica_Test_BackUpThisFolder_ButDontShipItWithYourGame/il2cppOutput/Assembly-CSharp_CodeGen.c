﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (void);
// 0x00000002 System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (void);
// 0x00000003 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (void);
// 0x00000004 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (void);
// 0x00000005 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (void);
// 0x00000006 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (void);
// 0x00000007 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (void);
// 0x00000008 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (void);
// 0x00000009 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (void);
// 0x0000000A System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (void);
// 0x0000000B UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (void);
// 0x0000000C System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (void);
// 0x0000000D System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (void);
// 0x0000000E System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (void);
// 0x0000000F System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (void);
// 0x00000010 AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (void);
// 0x00000011 System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (void);
// 0x00000012 System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (void);
// 0x00000013 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (void);
// 0x00000014 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (void);
// 0x00000015 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (void);
// 0x00000016 System.Void Joystick::Start()
extern void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (void);
// 0x00000017 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (void);
// 0x00000018 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (void);
// 0x00000019 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (void);
// 0x0000001A System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (void);
// 0x0000001B System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (void);
// 0x0000001C System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (void);
// 0x0000001D UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (void);
// 0x0000001E System.Void Joystick::.ctor()
extern void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (void);
// 0x0000001F System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (void);
// 0x00000020 System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (void);
// 0x00000021 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (void);
// 0x00000022 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (void);
// 0x00000023 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (void);
// 0x00000024 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (void);
// 0x00000025 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (void);
// 0x00000026 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (void);
// 0x00000027 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (void);
// 0x00000028 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (void);
// 0x00000029 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (void);
// 0x0000002A System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (void);
// 0x0000002B System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (void);
// 0x0000002C System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (void);
// 0x0000002D System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (void);
// 0x0000002E System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (void);
// 0x0000002F System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (void);
// 0x00000030 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (void);
// 0x00000031 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (void);
// 0x00000032 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (void);
// 0x00000033 System.Void DemoFree::Start()
extern void DemoFree_Start_mBF4E573159D1922BE6935057FBAA2431CA143555 (void);
// 0x00000034 System.Void DemoFree::Update()
extern void DemoFree_Update_mB7202DC0B8016DB4563824A605990F8E153C1455 (void);
// 0x00000035 System.Void DemoFree::OnGUI()
extern void DemoFree_OnGUI_mC1CA7B562FB8504C3385EA9316A428CA5EE60933 (void);
// 0x00000036 System.Void DemoFree::.ctor()
extern void DemoFree__ctor_mF73EF24C19F0C17A01A23C3021D3A409348CD734 (void);
// 0x00000037 System.Void FreeCameraLogic::Start()
extern void FreeCameraLogic_Start_mAEFB06A72DABA63B02A26D04B6B59F1739D0CBD2 (void);
// 0x00000038 System.Void FreeCameraLogic::SwitchTarget(System.Int32)
extern void FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0 (void);
// 0x00000039 System.Void FreeCameraLogic::NextTarget()
extern void FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D (void);
// 0x0000003A System.Void FreeCameraLogic::PreviousTarget()
extern void FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77 (void);
// 0x0000003B System.Void FreeCameraLogic::Update()
extern void FreeCameraLogic_Update_m6CF3D1604A8FAC90ECFC85B3C893CAAD54C29A0A (void);
// 0x0000003C System.Void FreeCameraLogic::LateUpdate()
extern void FreeCameraLogic_LateUpdate_m9C2597C9E05C7E13E677EF3BAC1E9CD500B4928C (void);
// 0x0000003D System.Void FreeCameraLogic::.ctor()
extern void FreeCameraLogic__ctor_m6898614914A5493AC854E75698C66A94D706ACF3 (void);
// 0x0000003E System.Void SimpleSampleCharacterControl::Awake()
extern void SimpleSampleCharacterControl_Awake_mD46E7A1ACA21666FFA49FBE92A43D056813737E8 (void);
// 0x0000003F System.Void SimpleSampleCharacterControl::OnCollisionEnter(UnityEngine.Collision)
extern void SimpleSampleCharacterControl_OnCollisionEnter_m9D9D4CB0CA4992269C200C11BADBF77AABA4C8EB (void);
// 0x00000040 System.Void SimpleSampleCharacterControl::OnCollisionStay(UnityEngine.Collision)
extern void SimpleSampleCharacterControl_OnCollisionStay_mD367B960863C0010FCF2C481E76D579E7959E49E (void);
// 0x00000041 System.Void SimpleSampleCharacterControl::OnCollisionExit(UnityEngine.Collision)
extern void SimpleSampleCharacterControl_OnCollisionExit_m46D7433CAAD5DCD3D54A465C56D9C81BCBCFAAE4 (void);
// 0x00000042 System.Void SimpleSampleCharacterControl::Update()
extern void SimpleSampleCharacterControl_Update_mA0057186C94EACB27E3E5FB5853B7BD125770FF1 (void);
// 0x00000043 System.Void SimpleSampleCharacterControl::FixedUpdate()
extern void SimpleSampleCharacterControl_FixedUpdate_m95FB469558AACEAF9B3383935A6E2C1D98E1D494 (void);
// 0x00000044 System.Void SimpleSampleCharacterControl::TankUpdate()
extern void SimpleSampleCharacterControl_TankUpdate_m3D7D92631D2653E40AD1EADDFD46395C099B2568 (void);
// 0x00000045 System.Void SimpleSampleCharacterControl::DirectUpdate()
extern void SimpleSampleCharacterControl_DirectUpdate_m107B99848528F674872DB7A277D33514A3863D24 (void);
// 0x00000046 System.Void SimpleSampleCharacterControl::JumpingAndLanding()
extern void SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201 (void);
// 0x00000047 System.Void SimpleSampleCharacterControl::.ctor()
extern void SimpleSampleCharacterControl__ctor_m4761190D7C96376CB6BD11FC408B321CEA199958 (void);
// 0x00000048 System.Void Ground::.ctor()
extern void Ground__ctor_m8594E0C48DFF84D558BEA17A51722E90961ED69A (void);
// 0x00000049 System.Void KickMaster.BaseEnemy::Awake()
extern void BaseEnemy_Awake_mEE6BBFDBAA3F90B021CC5BBFFBCE2820188D0BD1 (void);
// 0x0000004A System.Void KickMaster.BaseEnemy::OnEnable()
extern void BaseEnemy_OnEnable_m3ADDFF00FCE8C08BD3B4AFA5F2E59D59FAB30245 (void);
// 0x0000004B System.Void KickMaster.BaseEnemy::OnDisable()
extern void BaseEnemy_OnDisable_m696F449BB983D8AE7375EB913288C9C8D46F1B44 (void);
// 0x0000004C System.Void KickMaster.BaseEnemy::Die()
extern void BaseEnemy_Die_m299473781EA8B21D60AEE342EDCD3ACB2CDA8787 (void);
// 0x0000004D System.Void KickMaster.BaseEnemy::.ctor()
extern void BaseEnemy__ctor_mC97CA975036B3AE7540028E60285B1775154EF72 (void);
// 0x0000004E System.Void KickMaster.Boss::Awake()
extern void Boss_Awake_mCF43015BC755662F6014620782DB1E9B6202ED0B (void);
// 0x0000004F System.Void KickMaster.Boss::OnEnable()
extern void Boss_OnEnable_mF5E24AE5A7E6C117BE40E289FC6E86E5BDF3AC75 (void);
// 0x00000050 System.Void KickMaster.Boss::OnDisable()
extern void Boss_OnDisable_mA099BDCFBAA8B4A66F6B4C50EEC0B0F63FC3A2D9 (void);
// 0x00000051 System.Void KickMaster.Boss::TakeDamage(System.Int32)
extern void Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47 (void);
// 0x00000052 System.Void KickMaster.Boss::Die()
extern void Boss_Die_m7FE73A8FFBA993FA30CBF52D5A7AA84C136BDF12 (void);
// 0x00000053 System.Void KickMaster.Boss::.ctor()
extern void Boss__ctor_m0C69A1D5F4C285CAD67E74C06433ED8FF59BE7E9 (void);
// 0x00000054 System.Void KickMaster.Boss::<OnEnable>b__2_0()
extern void Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156 (void);
// 0x00000055 System.Void KickMaster.Boss::<OnDisable>b__3_0()
extern void Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD (void);
// 0x00000056 System.Void KickMaster.Enemy::add_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
extern void Enemy_add_Died_m96CD283CAC5DB65801E7397F01499B412C578F44 (void);
// 0x00000057 System.Void KickMaster.Enemy::remove_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
extern void Enemy_remove_Died_m775324A6A3CAC3EF965CEBD934F3BD61BDD1484E (void);
// 0x00000058 KickMaster.Player KickMaster.Enemy::get_Target()
extern void Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294 (void);
// 0x00000059 System.Int32 KickMaster.Enemy::get_Health()
extern void Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68 (void);
// 0x0000005A System.Void KickMaster.Enemy::OnDestroy()
extern void Enemy_OnDestroy_m05D3CB65796ECD271B848EE8A869A39B0825C54C (void);
// 0x0000005B System.Void KickMaster.Enemy::Die()
// 0x0000005C System.Void KickMaster.Enemy::.ctor()
extern void Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD (void);
// 0x0000005D System.Void KickMaster.Input::add_FingerUp(UnityEngine.Events.UnityAction)
extern void Input_add_FingerUp_m4BC4A64D65E35BB62295E3C201DD91FA52A9A389 (void);
// 0x0000005E System.Void KickMaster.Input::remove_FingerUp(UnityEngine.Events.UnityAction)
extern void Input_remove_FingerUp_m2B42E32D216FFC3D5A2167ABBA3B7C0F0E79E21D (void);
// 0x0000005F System.Void KickMaster.Input::Update()
extern void Input_Update_m767E0E4D068044B18454BAE32F4A53832CB8107A (void);
// 0x00000060 System.Void KickMaster.Input::HandleTouchUp()
extern void Input_HandleTouchUp_mE47FD3BE40F65821590376B7D7E1F8C279E71959 (void);
// 0x00000061 System.Void KickMaster.Input::.ctor()
extern void Input__ctor_m38106ACA8F618985D9260FE04C65CD94C965A8D7 (void);
// 0x00000062 System.Void KickMaster.Level::add_Won(UnityEngine.Events.UnityAction)
extern void Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763 (void);
// 0x00000063 System.Void KickMaster.Level::remove_Won(UnityEngine.Events.UnityAction)
extern void Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389 (void);
// 0x00000064 System.Void KickMaster.Level::add_Defeated(UnityEngine.Events.UnityAction)
extern void Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180 (void);
// 0x00000065 System.Void KickMaster.Level::remove_Defeated(UnityEngine.Events.UnityAction)
extern void Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED (void);
// 0x00000066 System.Void KickMaster.Level::OnEnable()
extern void Level_OnEnable_m7DFB053941053ADC8D667FFB2FFE22D3C68F93E0 (void);
// 0x00000067 System.Void KickMaster.Level::OnDisable()
extern void Level_OnDisable_mCA4A40A216AA3DB70B00B1F17B143E356C4C75B4 (void);
// 0x00000068 System.Void KickMaster.Level::Restart()
extern void Level_Restart_m8B6AD2AF91323B954D3768D29AF60CA6613F9ACD (void);
// 0x00000069 System.Void KickMaster.Level::OnEnemyDied(KickMaster.Enemy)
extern void Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18 (void);
// 0x0000006A System.Void KickMaster.Level::OnPlayerDied()
extern void Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82 (void);
// 0x0000006B System.Void KickMaster.Level::.ctor()
extern void Level__ctor_m42177C607252949FB75A1584E63EBD1BE9584FD4 (void);
// 0x0000006C System.Void KickMaster.BaseProjectile::Hit(UnityEngine.GameObject)
extern void BaseProjectile_Hit_m95E6A0D2DF3247E406DFA5E4F37B62934BD4F102 (void);
// 0x0000006D System.Void KickMaster.BaseProjectile::Throw(UnityEngine.Vector3)
extern void BaseProjectile_Throw_m8B2E205E455E9722374583D07A04E10FF590B33E (void);
// 0x0000006E System.Void KickMaster.BaseProjectile::.ctor()
extern void BaseProjectile__ctor_m0CBA0B95C4E07CFD7572288A684A017F9A46028A (void);
// 0x0000006F System.Void KickMaster.BossProjectile::Hit(UnityEngine.GameObject)
extern void BossProjectile_Hit_m7AB0977A90C96E3B7073DB46CB9AE63D4725C347 (void);
// 0x00000070 System.Void KickMaster.BossProjectile::Throw(UnityEngine.Vector3)
extern void BossProjectile_Throw_m7E020484613940C782FF562391DD68314CE0BE67 (void);
// 0x00000071 System.Void KickMaster.BossProjectile::.ctor()
extern void BossProjectile__ctor_m246570CAE5D6F1F06846DADABBFC89C6D61C485E (void);
// 0x00000072 System.Void KickMaster.Hitable::add_Hited(UnityEngine.Events.UnityAction)
extern void Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F (void);
// 0x00000073 System.Void KickMaster.Hitable::remove_Hited(UnityEngine.Events.UnityAction)
extern void Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85 (void);
// 0x00000074 System.Void KickMaster.Hitable::OnCollisionEnter(UnityEngine.Collision)
extern void Hitable_OnCollisionEnter_mDF96EA81BCFC6F3EFB05A5B55C7EFF570C1D41B7 (void);
// 0x00000075 System.Void KickMaster.Hitable::.ctor()
extern void Hitable__ctor_m702F1841738A4D3BF6FB73F51620088B1F281910 (void);
// 0x00000076 System.Void KickMaster.Kickable::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
extern void Kickable_add_Destroyed_m26D1607171283C13B73D0DECE05776DD7FD3894B (void);
// 0x00000077 System.Void KickMaster.Kickable::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
extern void Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018 (void);
// 0x00000078 System.Void KickMaster.Kickable::add_Kicked(UnityEngine.Events.UnityAction)
extern void Kickable_add_Kicked_m990B81847DFB334DE467B5F1A1E290CB133BA8A8 (void);
// 0x00000079 System.Void KickMaster.Kickable::remove_Kicked(UnityEngine.Events.UnityAction)
extern void Kickable_remove_Kicked_mE39AC028E50A7C98DDDA75E64C47C85736FB909E (void);
// 0x0000007A System.Void KickMaster.Kickable::Awake()
extern void Kickable_Awake_m721C43E029BBB17427FA725DE461F86797F7DF60 (void);
// 0x0000007B System.Void KickMaster.Kickable::OnDestroy()
extern void Kickable_OnDestroy_mC51443C777E311F7444B75C85B736D2503ADEA9B (void);
// 0x0000007C System.Boolean KickMaster.Kickable::IsInRange(UnityEngine.Transform)
extern void Kickable_IsInRange_m0AEC3C5E9368DD943DAD6456F6A6669612E8E6B9 (void);
// 0x0000007D System.Void KickMaster.Kickable::Kick(UnityEngine.Vector3)
extern void Kickable_Kick_mF8DB1FD38E7E1778FBFA3DDB8E02BB7D94EC4B9E (void);
// 0x0000007E System.Void KickMaster.Kickable::.ctor()
extern void Kickable__ctor_mE26C76A528EE607F211D1812DEBA0E17160A73D7 (void);
// 0x0000007F System.Void KickMaster.Projectile::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
extern void Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937 (void);
// 0x00000080 System.Void KickMaster.Projectile::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
extern void Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331 (void);
// 0x00000081 System.Void KickMaster.Projectile::Awake()
extern void Projectile_Awake_mFC54ADBB9C25E212EE12B819EA8B8F7D56006BC9 (void);
// 0x00000082 System.Void KickMaster.Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m0816DAC6910136BB80E476A9D939E83CC54E4B88 (void);
// 0x00000083 System.Void KickMaster.Projectile::Update()
extern void Projectile_Update_mAEE8BB28EE8207A8555570793637C73EB0B33159 (void);
// 0x00000084 System.Void KickMaster.Projectile::OnDestroy()
extern void Projectile_OnDestroy_m698170A4ED48DDE50B71F06B41B262A174B96AA3 (void);
// 0x00000085 System.Boolean KickMaster.Projectile::IsInRange(UnityEngine.Transform)
extern void Projectile_IsInRange_m06E3AD8E3349516B0C3AA623100F84073083784F (void);
// 0x00000086 System.Void KickMaster.Projectile::Throw(UnityEngine.Vector3)
// 0x00000087 System.Void KickMaster.Projectile::Hit(UnityEngine.GameObject)
// 0x00000088 System.Void KickMaster.Projectile::.ctor()
extern void Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28 (void);
// 0x00000089 System.Void KickMaster.Pause::Awake()
extern void Pause_Awake_mEB3BFDABBA5C26125648132B45CA55165A1C41FC (void);
// 0x0000008A System.Void KickMaster.Pause::OnEnable()
extern void Pause_OnEnable_m6255763E23DE5B809D100D17936EA75E82ED4B3A (void);
// 0x0000008B System.Void KickMaster.Pause::OnDisable()
extern void Pause_OnDisable_m187B38B8373ADA63FD05B3094E20FF4F83487C87 (void);
// 0x0000008C System.Void KickMaster.Pause::SetPause()
extern void Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A (void);
// 0x0000008D System.Void KickMaster.Pause::ResetPause()
extern void Pause_ResetPause_m12F854B48693949D669F711CA5DDFD71D2FDD98F (void);
// 0x0000008E System.Void KickMaster.Pause::.ctor()
extern void Pause__ctor_m56325ACDF5E0668ED84CCD758AC3A0202B221EC7 (void);
// 0x0000008F System.Void KickMaster.Kick::OnEnable()
extern void Kick_OnEnable_m3242542878250EA24D9E1C711A388413930CE859 (void);
// 0x00000090 System.Void KickMaster.Kick::OnDisable()
extern void Kick_OnDisable_m9E64A173E34DBC9AC413BD78D3431AFDB4BD1D03 (void);
// 0x00000091 System.Void KickMaster.Kick::AddProjectile(KickMaster.Projectile)
extern void Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF (void);
// 0x00000092 System.Void KickMaster.Kick::RemoveProjectile(KickMaster.Projectile)
extern void Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3 (void);
// 0x00000093 System.Void KickMaster.Kick::RemoveKickable(KickMaster.Kickable)
extern void Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923 (void);
// 0x00000094 System.Void KickMaster.Kick::ApplyOnce()
extern void Kick_ApplyOnce_mABEB15CF82185C7363539EA16C24088467756181 (void);
// 0x00000095 System.Void KickMaster.Kick::.ctor()
extern void Kick__ctor_mCA4BC7B933DC9BAE1E298113D625D0E5A143BE6D (void);
// 0x00000096 System.Void KickMaster.PlayerAction::ApplyOnce()
// 0x00000097 System.Void KickMaster.PlayerAction::.ctor()
extern void PlayerAction__ctor_m2F60E6DB5734572075E34991A6DD818B68C9BF8B (void);
// 0x00000098 System.Void KickMaster.Player::add_Died(UnityEngine.Events.UnityAction)
extern void Player_add_Died_mDDA2A70A8728F794B1836BDE39FE0B873EE1F00F (void);
// 0x00000099 System.Void KickMaster.Player::remove_Died(UnityEngine.Events.UnityAction)
extern void Player_remove_Died_mFC24CDAE84983636EADDA30EDEA10C7014D21713 (void);
// 0x0000009A System.Int32 KickMaster.Player::get_Health()
extern void Player_get_Health_m47669E1B8F1F22EFAC58E0684CF7D3FC83787547 (void);
// 0x0000009B System.Void KickMaster.Player::TakeDamage(System.Int32)
extern void Player_TakeDamage_mDBDD4B6DFAE12095463ED5CA009C5D36B1FB757A (void);
// 0x0000009C System.Void KickMaster.Player::Kill()
extern void Player_Kill_mD2AACC78D1E870C57334FB25CBA424C9CB984025 (void);
// 0x0000009D System.Void KickMaster.Player::Die()
extern void Player_Die_m42DB081342293F085036E90E510C58BAA26DD741 (void);
// 0x0000009E System.Void KickMaster.Player::.ctor()
extern void Player__ctor_m6F4748DC2C21349A1C2D98BFBE4F770D74955813 (void);
// 0x0000009F System.Void KickMaster.PlayerActions::OnEnable()
extern void PlayerActions_OnEnable_m31D2FFF2A8ED18941AD50AD8FF85B07CBA0752D1 (void);
// 0x000000A0 System.Void KickMaster.PlayerActions::OnDisable()
extern void PlayerActions_OnDisable_m2F102E98C0FC8B200A760353B66533557097BDD5 (void);
// 0x000000A1 System.Void KickMaster.PlayerActions::OnFingerUp()
extern void PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9 (void);
// 0x000000A2 System.Void KickMaster.PlayerActions::.ctor()
extern void PlayerActions__ctor_mC807036BD35DA2BF8477407CD931AA4261A4863F (void);
// 0x000000A3 System.Void KickMaster.PlayerMove::Start()
extern void PlayerMove_Start_m8664D6832C13BFA99D30C93642E9B2ADF00AD97D (void);
// 0x000000A4 System.Void KickMaster.PlayerMove::Update()
extern void PlayerMove_Update_m95D710E55CFBA4FB1E96503554A5DA7AA7797EAF (void);
// 0x000000A5 System.Void KickMaster.PlayerMove::Move()
extern void PlayerMove_Move_mB2A2C255A530412FC82230FC4C496BFFE0EB2AAE (void);
// 0x000000A6 System.Void KickMaster.PlayerMove::.ctor()
extern void PlayerMove__ctor_m6F4650C14C9995809A00DB2191EE0A0FB5C80304 (void);
// 0x000000A7 System.Void KickMaster.EndScreens::OnEnable()
extern void EndScreens_OnEnable_m38FD85EED973FB006BBA78351DAFBF5890589717 (void);
// 0x000000A8 System.Void KickMaster.EndScreens::OnDisable()
extern void EndScreens_OnDisable_mD6A4BA9F4E297AF3412EC6992301B7C97CB62434 (void);
// 0x000000A9 System.Void KickMaster.EndScreens::ShowWinScreen()
extern void EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429 (void);
// 0x000000AA System.Void KickMaster.EndScreens::ShowDefeatScreen()
extern void EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D (void);
// 0x000000AB System.Void KickMaster.EndScreens::.ctor()
extern void EndScreens__ctor_mCF0C95954053AFE91AC3A318C1DBB9547025CC27 (void);
// 0x000000AC KickMaster.StateMachine.State KickMaster.StateMachine.EnemyStateMachine::get_CurrentState()
extern void EnemyStateMachine_get_CurrentState_mC71C1387E7E632FCDB69969EC93BB43E582D636B (void);
// 0x000000AD System.Void KickMaster.StateMachine.EnemyStateMachine::Start()
extern void EnemyStateMachine_Start_mBCD5CCAEEBC5B893C88291C481B6C9131230F2C2 (void);
// 0x000000AE System.Void KickMaster.StateMachine.EnemyStateMachine::Update()
extern void EnemyStateMachine_Update_mB78E5B02C84801E8D8E0618478DC4A1E2A2B2691 (void);
// 0x000000AF System.Void KickMaster.StateMachine.EnemyStateMachine::ResetState()
extern void EnemyStateMachine_ResetState_m4B33D21E43676185CCA7A69618BD8510FB73FD48 (void);
// 0x000000B0 System.Void KickMaster.StateMachine.EnemyStateMachine::Transit(KickMaster.StateMachine.State)
extern void EnemyStateMachine_Transit_mAC3F54831E0C7B635B3DFD3D8B348D576BA75BAB (void);
// 0x000000B1 System.Void KickMaster.StateMachine.EnemyStateMachine::.ctor()
extern void EnemyStateMachine__ctor_mB6EBA9361DF4A9920479BB3D6D676D88B81F4A61 (void);
// 0x000000B2 KickMaster.Player KickMaster.StateMachine.State::get_Target()
extern void State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F (void);
// 0x000000B3 System.Void KickMaster.StateMachine.State::set_Target(KickMaster.Player)
extern void State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0 (void);
// 0x000000B4 System.Void KickMaster.StateMachine.State::Enter(KickMaster.Player)
extern void State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3 (void);
// 0x000000B5 System.Void KickMaster.StateMachine.State::Exit()
extern void State_Exit_m21D27445EFB2088E0D3CAD411B95DF23C3583918 (void);
// 0x000000B6 KickMaster.StateMachine.State KickMaster.StateMachine.State::GetNextState()
extern void State_GetNextState_m6BC7F69BE96489665470DB3B0126DF69D5E3E9E8 (void);
// 0x000000B7 System.Void KickMaster.StateMachine.State::.ctor()
extern void State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06 (void);
// 0x000000B8 System.Void KickMaster.StateMachine.DieState::OnEnable()
extern void DieState_OnEnable_m381458E2E37ACFBFB89D3A903A61EA4E3E4AD73B (void);
// 0x000000B9 System.Collections.IEnumerator KickMaster.StateMachine.DieState::DieWithDelay(System.Single)
extern void DieState_DieWithDelay_mBC37AC486AD23B02A6CB6F553592B07C792CE6F5 (void);
// 0x000000BA System.Void KickMaster.StateMachine.DieState::.ctor()
extern void DieState__ctor_m6753595B421250F06F5285DFEB0616CDADC97A8D (void);
// 0x000000BB System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::.ctor(System.Int32)
extern void U3CDieWithDelayU3Ed__3__ctor_m3F40ED80C5889B224BCC341C9877B2E5648EB663 (void);
// 0x000000BC System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.IDisposable.Dispose()
extern void U3CDieWithDelayU3Ed__3_System_IDisposable_Dispose_m491E338DF924690B9C5BD63FD728D6014E2BE956 (void);
// 0x000000BD System.Boolean KickMaster.StateMachine.DieState/<DieWithDelay>d__3::MoveNext()
extern void U3CDieWithDelayU3Ed__3_MoveNext_m2416D6D8DB88F7CC482431DCAC0E0FE4D92044F0 (void);
// 0x000000BE System.Object KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDieWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52DD6C9E4AE18BC10E8DD824B6863010C89F9AF1 (void);
// 0x000000BF System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mCBEE65D1315F7F38767FA29FF57029BE8C1EDC4C (void);
// 0x000000C0 System.Object KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_m2DF42E1A0078E5E302AE36FBEA8AA85FAEB3BBE4 (void);
// 0x000000C1 System.Void KickMaster.StateMachine.IdleState::OnEnable()
extern void IdleState_OnEnable_mA4C3B06BA33823C91DCDF5C157EDC73AB8A715C4 (void);
// 0x000000C2 System.Void KickMaster.StateMachine.IdleState::OnDisable()
extern void IdleState_OnDisable_m89BD6D412DD387A95018F318318F7513CB20D6AF (void);
// 0x000000C3 System.Void KickMaster.StateMachine.IdleState::.ctor()
extern void IdleState__ctor_m5D357B6ED2202AD207D2779C2C3A4253EF1EE1A7 (void);
// 0x000000C4 System.Void KickMaster.StateMachine.KillState::OnEnable()
extern void KillState_OnEnable_m3B70C28E06E9BCD56B2B275DEF32C283631FBD8B (void);
// 0x000000C5 System.Collections.IEnumerator KickMaster.StateMachine.KillState::KillWithDelay(System.Single)
extern void KillState_KillWithDelay_mA5485FB293E5DE76F1758611233AB762CFF6D1F7 (void);
// 0x000000C6 System.Void KickMaster.StateMachine.KillState::.ctor()
extern void KillState__ctor_m7BF76288D350ECD0139832398A106BED945366D5 (void);
// 0x000000C7 System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::.ctor(System.Int32)
extern void U3CKillWithDelayU3Ed__3__ctor_mF0D47A051DBE7501949B51D317BA36AAF27ADDBC (void);
// 0x000000C8 System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.IDisposable.Dispose()
extern void U3CKillWithDelayU3Ed__3_System_IDisposable_Dispose_m819CC3C003E5B4E484AE835447713704AC97C702 (void);
// 0x000000C9 System.Boolean KickMaster.StateMachine.KillState/<KillWithDelay>d__3::MoveNext()
extern void U3CKillWithDelayU3Ed__3_MoveNext_mC3B66CEA386DD1C163820461D0E5691B0F1F1457 (void);
// 0x000000CA System.Object KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4CCF15133F9BAB2E1CF858034D74ED7DBC3EE8C5 (void);
// 0x000000CB System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mD4DB2864CA12AB3E6E02E13C81DF1C67A2002FC7 (void);
// 0x000000CC System.Object KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_mC1FAA251C1DCCA428B3595CA81FC4D1A8D37A28B (void);
// 0x000000CD System.Void KickMaster.StateMachine.MoveState::OnEnable()
extern void MoveState_OnEnable_m55085530231E907068A5C4999C4818C5127D46DB (void);
// 0x000000CE System.Void KickMaster.StateMachine.MoveState::Update()
extern void MoveState_Update_mE53404BDC133FC421DDCFBFEA0E4F1D66C79AB06 (void);
// 0x000000CF System.Void KickMaster.StateMachine.MoveState::.ctor()
extern void MoveState__ctor_m75A267FEA996A0BB96A9853035320AABA5EA0A53 (void);
// 0x000000D0 System.Void KickMaster.StateMachine.ThrowingState::add_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
extern void ThrowingState_add_ProjectileCreated_m393F0C3F7147E89601AE303379137A2922597AE0 (void);
// 0x000000D1 System.Void KickMaster.StateMachine.ThrowingState::remove_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
extern void ThrowingState_remove_ProjectileCreated_mCC5531DCD64836A19173BCE84635F2FD5EFDE086 (void);
// 0x000000D2 System.Void KickMaster.StateMachine.ThrowingState::OnEnable()
extern void ThrowingState_OnEnable_mD110C9DD6474AB919B9FD71322070D9CD0660144 (void);
// 0x000000D3 System.Void KickMaster.StateMachine.ThrowingState::OnDisable()
extern void ThrowingState_OnDisable_m20AB89A72C3239391A27A6D8AD3B28ABE5FE2570 (void);
// 0x000000D4 System.Collections.IEnumerator KickMaster.StateMachine.ThrowingState::StartThrowing()
extern void ThrowingState_StartThrowing_m30EADAC80898A10EA73D3345393788483FB1C95E (void);
// 0x000000D5 KickMaster.Projectile KickMaster.StateMachine.ThrowingState::CreateProjectile()
extern void ThrowingState_CreateProjectile_m0C9DE1A31F8FF958DB30D428DF7679343F1821FB (void);
// 0x000000D6 System.Void KickMaster.StateMachine.ThrowingState::ThrowProjectile(KickMaster.Projectile)
extern void ThrowingState_ThrowProjectile_mEA5D18F8A1AA4ED87A1ADB5A7569B20CB8385793 (void);
// 0x000000D7 System.Void KickMaster.StateMachine.ThrowingState::.ctor()
extern void ThrowingState__ctor_m754FEDDAB0F8715293A12F43FD64637AFE744A2B (void);
// 0x000000D8 System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::.ctor(System.Int32)
extern void U3CStartThrowingU3Ed__12__ctor_mF45E77C4F928D1D962A53C49DF89DA1B9424C263 (void);
// 0x000000D9 System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.IDisposable.Dispose()
extern void U3CStartThrowingU3Ed__12_System_IDisposable_Dispose_mF8F2EE26B5D9F53BAA209A3827304D3FC900FE92 (void);
// 0x000000DA System.Boolean KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::MoveNext()
extern void U3CStartThrowingU3Ed__12_MoveNext_mFCA3E740B4019425EA3565D5A91A7ACB93A83B1E (void);
// 0x000000DB System.Object KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartThrowingU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FF01EF5E97F718AF309CAE0D1723385D7F3A18F (void);
// 0x000000DC System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.IEnumerator.Reset()
extern void U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_Reset_m14CE989A29945418838C62997F132A55F1EC0375 (void);
// 0x000000DD System.Object KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_get_Current_m8E96F4A731B971898D69AAA7494B129219CA02A8 (void);
// 0x000000DE KickMaster.Player KickMaster.StateMachine.Transition::get_Target()
extern void Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4 (void);
// 0x000000DF System.Void KickMaster.StateMachine.Transition::set_Target(KickMaster.Player)
extern void Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216 (void);
// 0x000000E0 KickMaster.Enemy KickMaster.StateMachine.Transition::get_Enemy()
extern void Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40 (void);
// 0x000000E1 System.Void KickMaster.StateMachine.Transition::set_Enemy(KickMaster.Enemy)
extern void Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8 (void);
// 0x000000E2 KickMaster.StateMachine.State KickMaster.StateMachine.Transition::get_TargetState()
extern void Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D (void);
// 0x000000E3 System.Boolean KickMaster.StateMachine.Transition::get_NeedTransit()
extern void Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B (void);
// 0x000000E4 System.Void KickMaster.StateMachine.Transition::set_NeedTransit(System.Boolean)
extern void Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819 (void);
// 0x000000E5 System.Void KickMaster.StateMachine.Transition::Init(KickMaster.Player)
extern void Transition_Init_m3735DD0482CD276E2D9D25FF21691DA308A116A0 (void);
// 0x000000E6 System.Void KickMaster.StateMachine.Transition::OnEnable()
extern void Transition_OnEnable_m6AF3C89201035C25B2D0BFFF99F88FE8899CB2B4 (void);
// 0x000000E7 System.Void KickMaster.StateMachine.Transition::.ctor()
extern void Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482 (void);
// 0x000000E8 System.Void KickMaster.StateMachine.DetectionTransition::Update()
extern void DetectionTransition_Update_m58A3C0047D22C0E812D5FB76FF1DD42DB3AF6885 (void);
// 0x000000E9 System.Void KickMaster.StateMachine.DetectionTransition::.ctor()
extern void DetectionTransition__ctor_mE8A9C454E703208425D65AC19FEBFFEAC36A082F (void);
// 0x000000EA System.Void KickMaster.StateMachine.DieTransition::Update()
extern void DieTransition_Update_m43C77F1C2B998BCF75C3BFCF82B1470F09E80F90 (void);
// 0x000000EB System.Void KickMaster.StateMachine.DieTransition::.ctor()
extern void DieTransition__ctor_m7F6F7AB1F55BB8B108BDF0481ED3C8A70014CE2E (void);
// 0x000000EC System.Void KickMaster.StateMachine.KillTransition::Start()
extern void KillTransition_Start_mD1324118480434BD696BAE1943FB750AA5BD1D13 (void);
// 0x000000ED System.Void KickMaster.StateMachine.KillTransition::Update()
extern void KillTransition_Update_m10C19A1A0C55BE6926E0C4F40D35811C8F6B8E15 (void);
// 0x000000EE System.Void KickMaster.StateMachine.KillTransition::.ctor()
extern void KillTransition__ctor_m89F4DCDB15CCA6F1DDA856B1FACD8421FF204879 (void);
// 0x000000EF System.Void KickMaster.StateMachine.TargetDieTransition::Update()
extern void TargetDieTransition_Update_m04E4872FF447999033E9875E403166646B16D5FE (void);
// 0x000000F0 System.Void KickMaster.StateMachine.TargetDieTransition::.ctor()
extern void TargetDieTransition__ctor_m7935B940CAF806DEC4C66F878357289A7292EEB8 (void);
static Il2CppMethodPointer s_methodPointers[240] = 
{
	JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7,
	JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2,
	JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23,
	JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404,
	JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1,
	JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B,
	JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41,
	JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91,
	Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA,
	Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE,
	Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC,
	Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77,
	Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505,
	Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC,
	Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19,
	Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388,
	Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6,
	Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40,
	Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A,
	Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613,
	Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86,
	Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C,
	Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6,
	Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9,
	Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C,
	Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342,
	Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987,
	Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C,
	Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024,
	Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B,
	DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A,
	DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9,
	DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72,
	DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136,
	DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C,
	DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506,
	DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB,
	FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24,
	FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81,
	FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54,
	FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D,
	FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C,
	VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99,
	VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A,
	VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE,
	VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D,
	VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5,
	VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526,
	VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB,
	VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852,
	DemoFree_Start_mBF4E573159D1922BE6935057FBAA2431CA143555,
	DemoFree_Update_mB7202DC0B8016DB4563824A605990F8E153C1455,
	DemoFree_OnGUI_mC1CA7B562FB8504C3385EA9316A428CA5EE60933,
	DemoFree__ctor_mF73EF24C19F0C17A01A23C3021D3A409348CD734,
	FreeCameraLogic_Start_mAEFB06A72DABA63B02A26D04B6B59F1739D0CBD2,
	FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0,
	FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D,
	FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77,
	FreeCameraLogic_Update_m6CF3D1604A8FAC90ECFC85B3C893CAAD54C29A0A,
	FreeCameraLogic_LateUpdate_m9C2597C9E05C7E13E677EF3BAC1E9CD500B4928C,
	FreeCameraLogic__ctor_m6898614914A5493AC854E75698C66A94D706ACF3,
	SimpleSampleCharacterControl_Awake_mD46E7A1ACA21666FFA49FBE92A43D056813737E8,
	SimpleSampleCharacterControl_OnCollisionEnter_m9D9D4CB0CA4992269C200C11BADBF77AABA4C8EB,
	SimpleSampleCharacterControl_OnCollisionStay_mD367B960863C0010FCF2C481E76D579E7959E49E,
	SimpleSampleCharacterControl_OnCollisionExit_m46D7433CAAD5DCD3D54A465C56D9C81BCBCFAAE4,
	SimpleSampleCharacterControl_Update_mA0057186C94EACB27E3E5FB5853B7BD125770FF1,
	SimpleSampleCharacterControl_FixedUpdate_m95FB469558AACEAF9B3383935A6E2C1D98E1D494,
	SimpleSampleCharacterControl_TankUpdate_m3D7D92631D2653E40AD1EADDFD46395C099B2568,
	SimpleSampleCharacterControl_DirectUpdate_m107B99848528F674872DB7A277D33514A3863D24,
	SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201,
	SimpleSampleCharacterControl__ctor_m4761190D7C96376CB6BD11FC408B321CEA199958,
	Ground__ctor_m8594E0C48DFF84D558BEA17A51722E90961ED69A,
	BaseEnemy_Awake_mEE6BBFDBAA3F90B021CC5BBFFBCE2820188D0BD1,
	BaseEnemy_OnEnable_m3ADDFF00FCE8C08BD3B4AFA5F2E59D59FAB30245,
	BaseEnemy_OnDisable_m696F449BB983D8AE7375EB913288C9C8D46F1B44,
	BaseEnemy_Die_m299473781EA8B21D60AEE342EDCD3ACB2CDA8787,
	BaseEnemy__ctor_mC97CA975036B3AE7540028E60285B1775154EF72,
	Boss_Awake_mCF43015BC755662F6014620782DB1E9B6202ED0B,
	Boss_OnEnable_mF5E24AE5A7E6C117BE40E289FC6E86E5BDF3AC75,
	Boss_OnDisable_mA099BDCFBAA8B4A66F6B4C50EEC0B0F63FC3A2D9,
	Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47,
	Boss_Die_m7FE73A8FFBA993FA30CBF52D5A7AA84C136BDF12,
	Boss__ctor_m0C69A1D5F4C285CAD67E74C06433ED8FF59BE7E9,
	Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156,
	Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD,
	Enemy_add_Died_m96CD283CAC5DB65801E7397F01499B412C578F44,
	Enemy_remove_Died_m775324A6A3CAC3EF965CEBD934F3BD61BDD1484E,
	Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294,
	Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68,
	Enemy_OnDestroy_m05D3CB65796ECD271B848EE8A869A39B0825C54C,
	NULL,
	Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD,
	Input_add_FingerUp_m4BC4A64D65E35BB62295E3C201DD91FA52A9A389,
	Input_remove_FingerUp_m2B42E32D216FFC3D5A2167ABBA3B7C0F0E79E21D,
	Input_Update_m767E0E4D068044B18454BAE32F4A53832CB8107A,
	Input_HandleTouchUp_mE47FD3BE40F65821590376B7D7E1F8C279E71959,
	Input__ctor_m38106ACA8F618985D9260FE04C65CD94C965A8D7,
	Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763,
	Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389,
	Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180,
	Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED,
	Level_OnEnable_m7DFB053941053ADC8D667FFB2FFE22D3C68F93E0,
	Level_OnDisable_mCA4A40A216AA3DB70B00B1F17B143E356C4C75B4,
	Level_Restart_m8B6AD2AF91323B954D3768D29AF60CA6613F9ACD,
	Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18,
	Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82,
	Level__ctor_m42177C607252949FB75A1584E63EBD1BE9584FD4,
	BaseProjectile_Hit_m95E6A0D2DF3247E406DFA5E4F37B62934BD4F102,
	BaseProjectile_Throw_m8B2E205E455E9722374583D07A04E10FF590B33E,
	BaseProjectile__ctor_m0CBA0B95C4E07CFD7572288A684A017F9A46028A,
	BossProjectile_Hit_m7AB0977A90C96E3B7073DB46CB9AE63D4725C347,
	BossProjectile_Throw_m7E020484613940C782FF562391DD68314CE0BE67,
	BossProjectile__ctor_m246570CAE5D6F1F06846DADABBFC89C6D61C485E,
	Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F,
	Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85,
	Hitable_OnCollisionEnter_mDF96EA81BCFC6F3EFB05A5B55C7EFF570C1D41B7,
	Hitable__ctor_m702F1841738A4D3BF6FB73F51620088B1F281910,
	Kickable_add_Destroyed_m26D1607171283C13B73D0DECE05776DD7FD3894B,
	Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018,
	Kickable_add_Kicked_m990B81847DFB334DE467B5F1A1E290CB133BA8A8,
	Kickable_remove_Kicked_mE39AC028E50A7C98DDDA75E64C47C85736FB909E,
	Kickable_Awake_m721C43E029BBB17427FA725DE461F86797F7DF60,
	Kickable_OnDestroy_mC51443C777E311F7444B75C85B736D2503ADEA9B,
	Kickable_IsInRange_m0AEC3C5E9368DD943DAD6456F6A6669612E8E6B9,
	Kickable_Kick_mF8DB1FD38E7E1778FBFA3DDB8E02BB7D94EC4B9E,
	Kickable__ctor_mE26C76A528EE607F211D1812DEBA0E17160A73D7,
	Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937,
	Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331,
	Projectile_Awake_mFC54ADBB9C25E212EE12B819EA8B8F7D56006BC9,
	Projectile_OnCollisionEnter_m0816DAC6910136BB80E476A9D939E83CC54E4B88,
	Projectile_Update_mAEE8BB28EE8207A8555570793637C73EB0B33159,
	Projectile_OnDestroy_m698170A4ED48DDE50B71F06B41B262A174B96AA3,
	Projectile_IsInRange_m06E3AD8E3349516B0C3AA623100F84073083784F,
	NULL,
	NULL,
	Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28,
	Pause_Awake_mEB3BFDABBA5C26125648132B45CA55165A1C41FC,
	Pause_OnEnable_m6255763E23DE5B809D100D17936EA75E82ED4B3A,
	Pause_OnDisable_m187B38B8373ADA63FD05B3094E20FF4F83487C87,
	Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A,
	Pause_ResetPause_m12F854B48693949D669F711CA5DDFD71D2FDD98F,
	Pause__ctor_m56325ACDF5E0668ED84CCD758AC3A0202B221EC7,
	Kick_OnEnable_m3242542878250EA24D9E1C711A388413930CE859,
	Kick_OnDisable_m9E64A173E34DBC9AC413BD78D3431AFDB4BD1D03,
	Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF,
	Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3,
	Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923,
	Kick_ApplyOnce_mABEB15CF82185C7363539EA16C24088467756181,
	Kick__ctor_mCA4BC7B933DC9BAE1E298113D625D0E5A143BE6D,
	NULL,
	PlayerAction__ctor_m2F60E6DB5734572075E34991A6DD818B68C9BF8B,
	Player_add_Died_mDDA2A70A8728F794B1836BDE39FE0B873EE1F00F,
	Player_remove_Died_mFC24CDAE84983636EADDA30EDEA10C7014D21713,
	Player_get_Health_m47669E1B8F1F22EFAC58E0684CF7D3FC83787547,
	Player_TakeDamage_mDBDD4B6DFAE12095463ED5CA009C5D36B1FB757A,
	Player_Kill_mD2AACC78D1E870C57334FB25CBA424C9CB984025,
	Player_Die_m42DB081342293F085036E90E510C58BAA26DD741,
	Player__ctor_m6F4748DC2C21349A1C2D98BFBE4F770D74955813,
	PlayerActions_OnEnable_m31D2FFF2A8ED18941AD50AD8FF85B07CBA0752D1,
	PlayerActions_OnDisable_m2F102E98C0FC8B200A760353B66533557097BDD5,
	PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9,
	PlayerActions__ctor_mC807036BD35DA2BF8477407CD931AA4261A4863F,
	PlayerMove_Start_m8664D6832C13BFA99D30C93642E9B2ADF00AD97D,
	PlayerMove_Update_m95D710E55CFBA4FB1E96503554A5DA7AA7797EAF,
	PlayerMove_Move_mB2A2C255A530412FC82230FC4C496BFFE0EB2AAE,
	PlayerMove__ctor_m6F4650C14C9995809A00DB2191EE0A0FB5C80304,
	EndScreens_OnEnable_m38FD85EED973FB006BBA78351DAFBF5890589717,
	EndScreens_OnDisable_mD6A4BA9F4E297AF3412EC6992301B7C97CB62434,
	EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429,
	EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D,
	EndScreens__ctor_mCF0C95954053AFE91AC3A318C1DBB9547025CC27,
	EnemyStateMachine_get_CurrentState_mC71C1387E7E632FCDB69969EC93BB43E582D636B,
	EnemyStateMachine_Start_mBCD5CCAEEBC5B893C88291C481B6C9131230F2C2,
	EnemyStateMachine_Update_mB78E5B02C84801E8D8E0618478DC4A1E2A2B2691,
	EnemyStateMachine_ResetState_m4B33D21E43676185CCA7A69618BD8510FB73FD48,
	EnemyStateMachine_Transit_mAC3F54831E0C7B635B3DFD3D8B348D576BA75BAB,
	EnemyStateMachine__ctor_mB6EBA9361DF4A9920479BB3D6D676D88B81F4A61,
	State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F,
	State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0,
	State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3,
	State_Exit_m21D27445EFB2088E0D3CAD411B95DF23C3583918,
	State_GetNextState_m6BC7F69BE96489665470DB3B0126DF69D5E3E9E8,
	State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06,
	DieState_OnEnable_m381458E2E37ACFBFB89D3A903A61EA4E3E4AD73B,
	DieState_DieWithDelay_mBC37AC486AD23B02A6CB6F553592B07C792CE6F5,
	DieState__ctor_m6753595B421250F06F5285DFEB0616CDADC97A8D,
	U3CDieWithDelayU3Ed__3__ctor_m3F40ED80C5889B224BCC341C9877B2E5648EB663,
	U3CDieWithDelayU3Ed__3_System_IDisposable_Dispose_m491E338DF924690B9C5BD63FD728D6014E2BE956,
	U3CDieWithDelayU3Ed__3_MoveNext_m2416D6D8DB88F7CC482431DCAC0E0FE4D92044F0,
	U3CDieWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52DD6C9E4AE18BC10E8DD824B6863010C89F9AF1,
	U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mCBEE65D1315F7F38767FA29FF57029BE8C1EDC4C,
	U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_m2DF42E1A0078E5E302AE36FBEA8AA85FAEB3BBE4,
	IdleState_OnEnable_mA4C3B06BA33823C91DCDF5C157EDC73AB8A715C4,
	IdleState_OnDisable_m89BD6D412DD387A95018F318318F7513CB20D6AF,
	IdleState__ctor_m5D357B6ED2202AD207D2779C2C3A4253EF1EE1A7,
	KillState_OnEnable_m3B70C28E06E9BCD56B2B275DEF32C283631FBD8B,
	KillState_KillWithDelay_mA5485FB293E5DE76F1758611233AB762CFF6D1F7,
	KillState__ctor_m7BF76288D350ECD0139832398A106BED945366D5,
	U3CKillWithDelayU3Ed__3__ctor_mF0D47A051DBE7501949B51D317BA36AAF27ADDBC,
	U3CKillWithDelayU3Ed__3_System_IDisposable_Dispose_m819CC3C003E5B4E484AE835447713704AC97C702,
	U3CKillWithDelayU3Ed__3_MoveNext_mC3B66CEA386DD1C163820461D0E5691B0F1F1457,
	U3CKillWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4CCF15133F9BAB2E1CF858034D74ED7DBC3EE8C5,
	U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mD4DB2864CA12AB3E6E02E13C81DF1C67A2002FC7,
	U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_mC1FAA251C1DCCA428B3595CA81FC4D1A8D37A28B,
	MoveState_OnEnable_m55085530231E907068A5C4999C4818C5127D46DB,
	MoveState_Update_mE53404BDC133FC421DDCFBFEA0E4F1D66C79AB06,
	MoveState__ctor_m75A267FEA996A0BB96A9853035320AABA5EA0A53,
	ThrowingState_add_ProjectileCreated_m393F0C3F7147E89601AE303379137A2922597AE0,
	ThrowingState_remove_ProjectileCreated_mCC5531DCD64836A19173BCE84635F2FD5EFDE086,
	ThrowingState_OnEnable_mD110C9DD6474AB919B9FD71322070D9CD0660144,
	ThrowingState_OnDisable_m20AB89A72C3239391A27A6D8AD3B28ABE5FE2570,
	ThrowingState_StartThrowing_m30EADAC80898A10EA73D3345393788483FB1C95E,
	ThrowingState_CreateProjectile_m0C9DE1A31F8FF958DB30D428DF7679343F1821FB,
	ThrowingState_ThrowProjectile_mEA5D18F8A1AA4ED87A1ADB5A7569B20CB8385793,
	ThrowingState__ctor_m754FEDDAB0F8715293A12F43FD64637AFE744A2B,
	U3CStartThrowingU3Ed__12__ctor_mF45E77C4F928D1D962A53C49DF89DA1B9424C263,
	U3CStartThrowingU3Ed__12_System_IDisposable_Dispose_mF8F2EE26B5D9F53BAA209A3827304D3FC900FE92,
	U3CStartThrowingU3Ed__12_MoveNext_mFCA3E740B4019425EA3565D5A91A7ACB93A83B1E,
	U3CStartThrowingU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FF01EF5E97F718AF309CAE0D1723385D7F3A18F,
	U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_Reset_m14CE989A29945418838C62997F132A55F1EC0375,
	U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_get_Current_m8E96F4A731B971898D69AAA7494B129219CA02A8,
	Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4,
	Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216,
	Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40,
	Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8,
	Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D,
	Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B,
	Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819,
	Transition_Init_m3735DD0482CD276E2D9D25FF21691DA308A116A0,
	Transition_OnEnable_m6AF3C89201035C25B2D0BFFF99F88FE8899CB2B4,
	Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482,
	DetectionTransition_Update_m58A3C0047D22C0E812D5FB76FF1DD42DB3AF6885,
	DetectionTransition__ctor_mE8A9C454E703208425D65AC19FEBFFEAC36A082F,
	DieTransition_Update_m43C77F1C2B998BCF75C3BFCF82B1470F09E80F90,
	DieTransition__ctor_m7F6F7AB1F55BB8B108BDF0481ED3C8A70014CE2E,
	KillTransition_Start_mD1324118480434BD696BAE1943FB750AA5BD1D13,
	KillTransition_Update_m10C19A1A0C55BE6926E0C4F40D35811C8F6B8E15,
	KillTransition__ctor_m89F4DCDB15CCA6F1DDA856B1FACD8421FF204879,
	TargetDieTransition_Update_m04E4872FF447999033E9875E403166646B16D5FE,
	TargetDieTransition__ctor_m7935B940CAF806DEC4C66F878357289A7292EEB8,
};
static const int32_t s_InvokerIndices[240] = 
{
	3451,
	3451,
	2795,
	2795,
	2750,
	2750,
	3451,
	3451,
	3395,
	3395,
	3441,
	3395,
	2844,
	3395,
	2844,
	3339,
	2795,
	3299,
	2750,
	3299,
	2750,
	3451,
	2813,
	2813,
	600,
	3451,
	1226,
	2813,
	2593,
	3451,
	3395,
	2844,
	3451,
	2813,
	2813,
	600,
	3451,
	3451,
	3451,
	2813,
	2813,
	3451,
	3395,
	2844,
	2795,
	3451,
	2813,
	2813,
	600,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2795,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2813,
	2813,
	2813,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2795,
	3451,
	3451,
	3451,
	3451,
	2813,
	2813,
	3359,
	3339,
	3451,
	0,
	3451,
	2813,
	2813,
	3451,
	3451,
	3451,
	2813,
	2813,
	2813,
	2813,
	3451,
	3451,
	3451,
	2813,
	3451,
	3451,
	2813,
	2887,
	3451,
	2813,
	2887,
	3451,
	2813,
	2813,
	2813,
	3451,
	2813,
	2813,
	2813,
	2813,
	3451,
	3451,
	1988,
	2887,
	3451,
	2813,
	2813,
	3451,
	2813,
	3451,
	3451,
	1988,
	0,
	0,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	2813,
	2813,
	2813,
	3451,
	3451,
	0,
	3451,
	2813,
	2813,
	3339,
	2795,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3359,
	3451,
	3451,
	3451,
	2813,
	3451,
	3359,
	2813,
	2813,
	3451,
	3359,
	3451,
	3451,
	2481,
	3451,
	2795,
	3451,
	3299,
	3359,
	3451,
	3359,
	3451,
	3451,
	3451,
	3451,
	2481,
	3451,
	2795,
	3451,
	3299,
	3359,
	3451,
	3359,
	3451,
	3451,
	3451,
	2813,
	2813,
	3451,
	3451,
	3359,
	3359,
	2813,
	3451,
	2795,
	3451,
	3299,
	3359,
	3451,
	3359,
	3359,
	2813,
	3359,
	2813,
	3359,
	3299,
	2750,
	2813,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
	3451,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	240,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
