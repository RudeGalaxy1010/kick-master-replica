﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtualActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtualActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};

// System.Action`1<System.Int32>
struct Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8;
// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252;
// System.Collections.Generic.List`1<KickMaster.Enemy>
struct List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19;
// System.Collections.Generic.List`1<KickMaster.Kickable>
struct List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<KickMaster.Projectile>
struct List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F;
// System.Collections.Generic.List`1<KickMaster.StateMachine.ThrowingState>
struct List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D;
// System.Collections.Generic.List`1<KickMaster.StateMachine.Transition>
struct List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.Events.UnityAction`1<KickMaster.Enemy>
struct UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27;
// UnityEngine.Events.UnityAction`1<KickMaster.Kickable>
struct UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A;
// UnityEngine.Events.UnityAction`1<KickMaster.Projectile>
struct UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1;
// UnityEngine.Animator[]
struct AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t94A9D70F63D095AFF2A9B4613012A5F7F3141787;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// KickMaster.Enemy[]
struct EnemyU5BU5D_t31FF45D580365260697954C28FBBE4CD258CBC8C;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// KickMaster.Kickable[]
struct KickableU5BU5D_tBC5D57DBB50CDB20C7FBFB8B8D712990EE85C8C4;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// KickMaster.PlayerAction[]
struct PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE;
// KickMaster.Projectile[]
struct ProjectileU5BU5D_t3B1D6DA3F8714316D0E823D41F749A8638C280CD;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// KickMaster.StateMachine.ThrowingState[]
struct ThrowingStateU5BU5D_t9097B28505A5E5064A7361D675AD4C472EC4CD4D;
// UnityEngine.Transform[]
struct TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24;
// KickMaster.StateMachine.Transition[]
struct TransitionU5BU5D_t68BA8F934E8F5630BAAAC381059754F1B04C2ECB;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.Animation
struct Animation_t6593B06C39E3B139808B19F2C719C860F3F61040;
// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883;
// KickMaster.BaseEnemy
struct BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365;
// KickMaster.BaseProjectile
struct BaseProjectile_t4DA45DEFCD7C1378B3DB667EAC4C2B9FED1D7D00;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832;
// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
// KickMaster.Boss
struct Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412;
// KickMaster.BossProjectile
struct BossProjectile_t544BE8E12437763EC7481788B2B11751B8F22C9A;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A;
// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76;
// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// DemoFree
struct DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC;
// KickMaster.StateMachine.DetectionTransition
struct DetectionTransition_tBEE948ABDBDFD2F492ACAD4A9D093339769038D8;
// KickMaster.StateMachine.DieState
struct DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3;
// KickMaster.StateMachine.DieTransition
struct DieTransition_tA7FF60FF626EE5BE5A9E6EBF5243BC9EC6DE7684;
// DynamicJoystick
struct DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10;
// KickMaster.EndScreens
struct EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6;
// KickMaster.Enemy
struct Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584;
// KickMaster.StateMachine.EnemyStateMachine
struct EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707;
// FixedJoystick
struct FixedJoystick_t7AA7F128A16A375A847AD0C7067993A6CC95DD7F;
// FloatingJoystick
struct FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// FreeCameraLogic
struct FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// Ground
struct Ground_t3A6E265B327D38866FB20EEC69AB4FE1D6A0598C;
// KickMaster.Hitable
struct Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// KickMaster.StateMachine.IdleState
struct IdleState_t58E9FE55FF2340984CDD43A38A3FB6BDF803CA68;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// KickMaster.Input
struct Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB;
// Joystick
struct Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A;
// JoystickPlayerExample
struct JoystickPlayerExample_tD6DD431AB5B17F44428C240223A7B161AECC474B;
// JoystickSetterExample
struct JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E;
// KickMaster.Kick
struct Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D;
// KickMaster.Kickable
struct Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9;
// KickMaster.StateMachine.KillState
struct KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632;
// KickMaster.StateMachine.KillTransition
struct KillTransition_t681FD7B428DEEF8F9F72FDC0B203922F7E7D8BFA;
// KickMaster.Level
struct Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// KickMaster.StateMachine.MoveState
struct MoveState_tB658AA74A33C09F34C680910AE3A80B959935CA0;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// KickMaster.Pause
struct Pause_t8089C16F9BD7090C84055E8237A1A845E865867F;
// KickMaster.Player
struct Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5;
// KickMaster.PlayerAction
struct PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02;
// KickMaster.PlayerActions
struct PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F;
// KickMaster.PlayerMove
struct PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB;
// KickMaster.Projectile
struct Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// SimpleSampleCharacterControl
struct SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// KickMaster.StateMachine.State
struct State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93;
// System.String
struct String_t;
// KickMaster.StateMachine.TargetDieTransition
struct TargetDieTransition_t0935666A05E1F13EAAB8A8DEF693D2FCB5050637;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// KickMaster.StateMachine.ThrowingState
struct ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// KickMaster.StateMachine.Transition
struct Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// VariableJoystick
struct VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC;
// KickMaster.StateMachine.DieState/<DieWithDelay>d__3
struct U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0;
// KickMaster.StateMachine.KillState/<KillWithDelay>d__3
struct U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24;
// KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12
struct U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C;

IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral024C60BC25F851E8B9F5531CC461C68CD60D3F30;
IL2CPP_EXTERN_C String_t* _stringLiteral08EF1409295A748F17C961491378A3B6215AA838;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351;
IL2CPP_EXTERN_C String_t* _stringLiteral2D052436E75E077A15CF127A860870F6B99DFB7D;
IL2CPP_EXTERN_C String_t* _stringLiteral3C95DC36BC35028D124872E120F7701E290A66F6;
IL2CPP_EXTERN_C String_t* _stringLiteral51D71BFB388793426FCAD75AE8863C67386257EE;
IL2CPP_EXTERN_C String_t* _stringLiteral5F23E0C606C3923E7D0C5C95679CF58C71B45C12;
IL2CPP_EXTERN_C String_t* _stringLiteral6141244FEB8CC92D7C3D8EC0BC1EABD9FA4BB959;
IL2CPP_EXTERN_C String_t* _stringLiteral61576FC535CE67A4234F725315CB796CEC925011;
IL2CPP_EXTERN_C String_t* _stringLiteral62DCA95296ED08F48DDF8762225138C798CBEA96;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral926A646AB5A68B4F9CE13C5B42BF00FE91E8C2D9;
IL2CPP_EXTERN_C String_t* _stringLiteral9EE0F8466DB982097F181BF01C645F3FC0C20D63;
IL2CPP_EXTERN_C String_t* _stringLiteralD5FEB5D60EDA237BAB0D4BA09C512761205128CA;
IL2CPP_EXTERN_C String_t* _stringLiteralDB546722AF594A8BDD41950B5C210E49FB115982;
IL2CPP_EXTERN_C String_t* _stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4;
IL2CPP_EXTERN_C String_t* _stringLiteralF8A646A0F5BE4AC603D518083703FBC3E2AF75F2;
IL2CPP_EXTERN_C String_t* _stringLiteralFDA32DC2E96C00474CE484C62A98501A5FB8786E;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInParent_TisCanvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_m5FB554DD7C0F662DAB84C0F292B221CAE3F0A5B3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A_m96B48A644EDC97C5C82F154D1FEA551B2E392040_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisKickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9_m909A71EC628FD746DBAE6837FEC8E63E358A6333_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_mFEF05EA9F9117D5DD9F989DAC47DD639ED62F6C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisPlayer_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5_m0A68D908EF4D6E6D6F7D73B2FA3EAB82A6BD1024_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_mEEDC9F8E08419F16EA0E30DD83FBAC6238635A28_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_mCA867A56E93C824FB9C272E0DBB67655CB275941_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7C6294F5B3C8ECA89999B8E4578ABBEB1A3B77C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m4873E203826D9E39239EDDF86822BF6E8C4DDB1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m48E0BD9D0F4E6BF369753EFFE945A863DAFF8ABC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m8EBB6F6ED60A4A01CC524FEFACF137C9C335DC2B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m0CDD6F02F45026B4267E7117C5DDC188F87EE7BE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mAC29D9D5108AA6D94BA97759E1E1680504442F9F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mAEC3C87A864E42B46F6D42A695E4F28235434A1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mEE327D7F83780A435BB6EDDDA3F1898A8EF032E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m21D8D394A4B96B49B33EC956B7AFE650AB1572AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectsOfType_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m70A4338B4AA009BCDF8C81C96967144DC94383FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_m74F486DA40756F8EFD2B5EAE87CB856E511F4BF9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mCBEE65D1315F7F38767FA29FF57029BE8C1EDC4C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mD4DB2864CA12AB3E6E02E13C81DF1C67A2002FC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_Reset_m14CE989A29945418838C62997F132A55F1EC0375_RuntimeMethod_var;
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759;
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411;
struct GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE;
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};

// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE  : public RuntimeObject
{
};

struct EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE_StaticFields
{
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___Value_0;
};

// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ColliderU5BU5D_t94A9D70F63D095AFF2A9B4613012A5F7F3141787* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ColliderU5BU5D_t94A9D70F63D095AFF2A9B4613012A5F7F3141787* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<KickMaster.Enemy>
struct List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	EnemyU5BU5D_t31FF45D580365260697954C28FBBE4CD258CBC8C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	EnemyU5BU5D_t31FF45D580365260697954C28FBBE4CD258CBC8C* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<KickMaster.Kickable>
struct List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	KickableU5BU5D_tBC5D57DBB50CDB20C7FBFB8B8D712990EE85C8C4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	KickableU5BU5D_tBC5D57DBB50CDB20C7FBFB8B8D712990EE85C8C4* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<KickMaster.Projectile>
struct List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ProjectileU5BU5D_t3B1D6DA3F8714316D0E823D41F749A8638C280CD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ProjectileU5BU5D_t3B1D6DA3F8714316D0E823D41F749A8638C280CD* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<KickMaster.StateMachine.ThrowingState>
struct List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ThrowingStateU5BU5D_t9097B28505A5E5064A7361D675AD4C472EC4CD4D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ThrowingStateU5BU5D_t9097B28505A5E5064A7361D675AD4C472EC4CD4D* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	TransformU5BU5D_tBB9C5F5686CAE82E3D97D43DF0F3D68ABF75EC24* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<KickMaster.StateMachine.Transition>
struct List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	TransitionU5BU5D_t68BA8F934E8F5630BAAAC381059754F1B04C2ECB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	TransitionU5BU5D_t68BA8F934E8F5630BAAAC381059754F1B04C2ECB* ___s_emptyArray_5;
};

// UnityEngine.EventSystems.AbstractEventData
struct AbstractEventData_tAE1A127ED657117548181D29FFE4B1B14D8E67F7  : public RuntimeObject
{
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;
};
struct Il2CppArrayBounds;

// UnityEngine.GUILayoutOption
struct GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14  : public RuntimeObject
{
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject* ___value_1;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// KickMaster.StateMachine.DieState/<DieWithDelay>d__3
struct U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0  : public RuntimeObject
{
	// System.Int32 KickMaster.StateMachine.DieState/<DieWithDelay>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object KickMaster.StateMachine.DieState/<DieWithDelay>d__3::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// System.Single KickMaster.StateMachine.DieState/<DieWithDelay>d__3::delay
	float ___delay_2;
	// KickMaster.StateMachine.DieState KickMaster.StateMachine.DieState/<DieWithDelay>d__3::<>4__this
	DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* ___U3CU3E4__this_3;
};

// KickMaster.StateMachine.KillState/<KillWithDelay>d__3
struct U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4  : public RuntimeObject
{
	// System.Int32 KickMaster.StateMachine.KillState/<KillWithDelay>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object KickMaster.StateMachine.KillState/<KillWithDelay>d__3::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// System.Single KickMaster.StateMachine.KillState/<KillWithDelay>d__3::delay
	float ___delay_2;
	// KickMaster.StateMachine.KillState KickMaster.StateMachine.KillState/<KillWithDelay>d__3::<>4__this
	KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* ___U3CU3E4__this_3;
};

// KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12
struct U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C  : public RuntimeObject
{
	// System.Int32 KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// KickMaster.StateMachine.ThrowingState KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::<>4__this
	ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* ___U3CU3E4__this_2;
};

// System.Collections.Generic.List`1/Enumerator<KickMaster.Enemy>
struct Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<KickMaster.Kickable>
struct Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RuntimeObject* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<KickMaster.Projectile>
struct Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<KickMaster.StateMachine.ThrowingState>
struct Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* ____current_3;
};

// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F  : public AbstractEventData_tAE1A127ED657117548181D29FFE4B1B14D8E67F7
{
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t61C51380B105BE9D2C39C4F15B7E655659957707* ___m_EventSystem_1;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.SceneManagement.Scene
struct Scene_tA1DC762B79745EB5140F054C884855B922318356 
{
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0  : public RuntimeObject
{
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	// UnityEngine.Component UnityEngine.Collision::m_Body
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	// System.Int32 UnityEngine.Collision::m_ContactCount
	int32_t ___m_ContactCount_4;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_ReusedContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_ReusedContacts_5;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_LegacyContacts
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* ___m_LegacyContacts_6;
};
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Impulse_0;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_RelativeVelocity_1;
	Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* ___m_Body_2;
	Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___m_Collider_3;
	int32_t ___m_ContactCount_4;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_ReusedContacts_5;
	ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* ___m_LegacyContacts_6;
};

// UnityEngine.ContactPoint
struct ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 
{
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Point
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.ContactPoint::m_Normal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Normal_1;
	// System.Int32 UnityEngine.ContactPoint::m_ThisColliderInstanceID
	int32_t ___m_ThisColliderInstanceID_2;
	// System.Int32 UnityEngine.ContactPoint::m_OtherColliderInstanceID
	int32_t ___m_OtherColliderInstanceID_3;
	// System.Single UnityEngine.ContactPoint::m_Separation
	float ___m_Separation_4;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 
{
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::displayIndex
	int32_t ___displayIndex_10;
};
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023_marshaled_pinvoke
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	int32_t ___displayIndex_10;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023_marshaled_com
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_GameObject_0;
	BaseRaycaster_t7DC8158FD3CA0193455344379DD5FF7CD5F1F832* ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition_7;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldNormal_8;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition_9;
	int32_t ___displayIndex_10;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB  : public BaseEventData_tE03A848325C0AE8E76C6CA15FD86395EBF83364F
{
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerClick>k__BackingField
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___U3CpointerClickU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 ___U3CpointerCurrentRaycastU3Ek__BackingField_8;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_tEC6A7B7CABA99C386F054F01E498AEC426CF8023 ___U3CpointerPressRaycastU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_tB951CE80B58D1BF9650862451D8DAD8C231F207B* ___hovered_10;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_11;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CpositionU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CdeltaU3Ek__BackingField_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CpressPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CworldPositionU3Ek__BackingField_16;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___U3CworldNormalU3Ek__BackingField_17;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_18;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CscrollDeltaU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_21;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_22;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_23;
	// System.Single UnityEngine.EventSystems.PointerEventData::<pressure>k__BackingField
	float ___U3CpressureU3Ek__BackingField_24;
	// System.Single UnityEngine.EventSystems.PointerEventData::<tangentialPressure>k__BackingField
	float ___U3CtangentialPressureU3Ek__BackingField_25;
	// System.Single UnityEngine.EventSystems.PointerEventData::<altitudeAngle>k__BackingField
	float ___U3CaltitudeAngleU3Ek__BackingField_26;
	// System.Single UnityEngine.EventSystems.PointerEventData::<azimuthAngle>k__BackingField
	float ___U3CazimuthAngleU3Ek__BackingField_27;
	// System.Single UnityEngine.EventSystems.PointerEventData::<twist>k__BackingField
	float ___U3CtwistU3Ek__BackingField_28;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<radius>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CradiusU3Ek__BackingField_29;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<radiusVariance>k__BackingField
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3CradiusVarianceU3Ek__BackingField_30;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<fullyExited>k__BackingField
	bool ___U3CfullyExitedU3Ek__BackingField_31;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<reentered>k__BackingField
	bool ___U3CreenteredU3Ek__BackingField_32;
};

// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Events.UnityAction`1<KickMaster.Enemy>
struct UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27  : public MulticastDelegate_t
{
};

// UnityEngine.Events.UnityAction`1<KickMaster.Kickable>
struct UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1  : public MulticastDelegate_t
{
};

// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A  : public MulticastDelegate_t
{
};

// UnityEngine.Events.UnityAction`1<KickMaster.Projectile>
struct UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Collider
struct Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Rigidbody
struct Rigidbody_t268697F5A994213ED97393309870968BC1C7393C  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7  : public MulticastDelegate_t
{
};

// UnityEngine.Animation
struct Animation_t6593B06C39E3B139808B19F2C719C860F3F61040  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.Animator
struct Animator_t8A52E42AE54F76681838FE9E632683EF3952E883  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_StaticFields
{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::preWillRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___preWillRenderCanvases_4;
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_tA4A6E66DBA797DCB45B995DBA449A9D1D80D0FBC* ___willRenderCanvases_5;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externBeginRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternBeginRenderOverlaysU3Ek__BackingField_6;
	// System.Action`2<System.Int32,System.Int32> UnityEngine.Canvas::<externRenderOverlaysBefore>k__BackingField
	Action_2_tD7438462601D3939500ED67463331FE00CFFBDB8* ___U3CexternRenderOverlaysBeforeU3Ek__BackingField_7;
	// System.Action`1<System.Int32> UnityEngine.Canvas::<externEndRenderOverlays>k__BackingField
	Action_1_tD69A6DC9FBE94131E52F5A73B2A9D4AB51EEC404* ___U3CexternEndRenderOverlaysU3Ek__BackingField_8;
};

// UnityEngine.CharacterController
struct CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A  : public Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5  : public Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1
{
};

struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_StaticFields
{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24* ___reapplyDrivenProperties_4;
};

// DemoFree
struct DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.String[] DemoFree::m_animations
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___m_animations_4;
	// UnityEngine.Animator[] DemoFree::m_animators
	AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* ___m_animators_5;
	// FreeCameraLogic DemoFree::m_cameraLogic
	FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* ___m_cameraLogic_6;
};

// KickMaster.EndScreens
struct EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// KickMaster.Level KickMaster.EndScreens::_level
	Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* ____level_4;
	// UnityEngine.GameObject KickMaster.EndScreens::_winScreen
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ____winScreen_5;
	// UnityEngine.GameObject KickMaster.EndScreens::_defeatScreen
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ____defeatScreen_6;
};

// KickMaster.Enemy
struct Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction`1<KickMaster.Enemy> KickMaster.Enemy::Died
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* ___Died_4;
	// System.Int32 KickMaster.Enemy::_health
	int32_t ____health_5;
	// KickMaster.Player KickMaster.Enemy::_target
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ____target_6;
};

// KickMaster.StateMachine.EnemyStateMachine
struct EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// KickMaster.StateMachine.State KickMaster.StateMachine.EnemyStateMachine::_initialState
	State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* ____initialState_4;
	// KickMaster.Player KickMaster.StateMachine.EnemyStateMachine::_target
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ____target_5;
	// KickMaster.StateMachine.State KickMaster.StateMachine.EnemyStateMachine::_currentState
	State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* ____currentState_6;
};

// FreeCameraLogic
struct FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Transform FreeCameraLogic::m_currentTarget
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___m_currentTarget_4;
	// System.Single FreeCameraLogic::m_distance
	float ___m_distance_5;
	// System.Single FreeCameraLogic::m_height
	float ___m_height_6;
	// System.Single FreeCameraLogic::m_lookAtAroundAngle
	float ___m_lookAtAroundAngle_7;
	// System.Collections.Generic.List`1<UnityEngine.Transform> FreeCameraLogic::m_targets
	List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* ___m_targets_8;
	// System.Int32 FreeCameraLogic::m_currentIndex
	int32_t ___m_currentIndex_9;
};

// Ground
struct Ground_t3A6E265B327D38866FB20EEC69AB4FE1D6A0598C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// KickMaster.Hitable
struct Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction KickMaster.Hitable::Hited
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___Hited_4;
};

// KickMaster.Input
struct Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction KickMaster.Input::FingerUp
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___FingerUp_4;
	// System.Boolean KickMaster.Input::_isTouch
	bool ____isTouch_5;
};

// Joystick
struct Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single Joystick::handleRange
	float ___handleRange_4;
	// System.Single Joystick::deadZone
	float ___deadZone_5;
	// AxisOptions Joystick::axisOptions
	int32_t ___axisOptions_6;
	// System.Boolean Joystick::snapX
	bool ___snapX_7;
	// System.Boolean Joystick::snapY
	bool ___snapY_8;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___background_9;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___handle_10;
	// UnityEngine.RectTransform Joystick::baseRect
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___baseRect_11;
	// UnityEngine.Canvas Joystick::canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___canvas_12;
	// UnityEngine.Camera Joystick::cam
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam_13;
	// UnityEngine.Vector2 Joystick::input
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___input_14;
};

// JoystickPlayerExample
struct JoystickPlayerExample_tD6DD431AB5B17F44428C240223A7B161AECC474B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single JoystickPlayerExample::speed
	float ___speed_4;
	// VariableJoystick JoystickPlayerExample::variableJoystick
	VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* ___variableJoystick_5;
	// UnityEngine.Rigidbody JoystickPlayerExample::rb
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___rb_6;
};

// JoystickSetterExample
struct JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// VariableJoystick JoystickSetterExample::variableJoystick
	VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* ___variableJoystick_4;
	// UnityEngine.UI.Text JoystickSetterExample::valueText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___valueText_5;
	// UnityEngine.UI.Image JoystickSetterExample::background
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___background_6;
	// UnityEngine.Sprite[] JoystickSetterExample::axisSprites
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___axisSprites_7;
};

// KickMaster.Kickable
struct Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction`1<KickMaster.Kickable> KickMaster.Kickable::Destroyed
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* ___Destroyed_4;
	// UnityEngine.Events.UnityAction KickMaster.Kickable::Kicked
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___Kicked_5;
	// System.Single KickMaster.Kickable::_maxDistance
	float ____maxDistance_6;
	// UnityEngine.Rigidbody KickMaster.Kickable::_rigidbody
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ____rigidbody_7;
};

// KickMaster.Level
struct Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction KickMaster.Level::Won
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___Won_4;
	// UnityEngine.Events.UnityAction KickMaster.Level::Defeated
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___Defeated_5;
	// System.Collections.Generic.List`1<KickMaster.Enemy> KickMaster.Level::_enemies
	List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* ____enemies_6;
	// KickMaster.Player KickMaster.Level::_player
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ____player_7;
};

// KickMaster.Pause
struct Pause_t8089C16F9BD7090C84055E8237A1A845E865867F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// KickMaster.Level KickMaster.Pause::_level
	Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* ____level_4;
};

// KickMaster.Player
struct Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction KickMaster.Player::Died
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___Died_4;
	// System.Int32 KickMaster.Player::_health
	int32_t ____health_5;
};

// KickMaster.PlayerAction
struct PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// KickMaster.PlayerActions
struct PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Animation KickMaster.PlayerActions::_legHitAnimation
	Animation_t6593B06C39E3B139808B19F2C719C860F3F61040* ____legHitAnimation_4;
	// KickMaster.Input KickMaster.PlayerActions::_input
	Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* ____input_5;
	// KickMaster.PlayerAction[] KickMaster.PlayerActions::_actions
	PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE* ____actions_6;
};

// KickMaster.PlayerMove
struct PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single KickMaster.PlayerMove::_moveSpeed
	float ____moveSpeed_4;
	// System.Single KickMaster.PlayerMove::_rotationSpeed
	float ____rotationSpeed_5;
	// Joystick KickMaster.PlayerMove::_joystick
	Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* ____joystick_6;
	// UnityEngine.CharacterController KickMaster.PlayerMove::_controller
	CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* ____controller_7;
};

// KickMaster.Projectile
struct Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Events.UnityAction`1<KickMaster.Projectile> KickMaster.Projectile::Destroyed
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___Destroyed_4;
	// System.Single KickMaster.Projectile::_maxInteractionDistance
	float ____maxInteractionDistance_5;
	// System.Single KickMaster.Projectile::_destroyDistance
	float ____destroyDistance_6;
	// UnityEngine.Vector3 KickMaster.Projectile::_startPosotion
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ____startPosotion_7;
};

// SimpleSampleCharacterControl
struct SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single SimpleSampleCharacterControl::m_moveSpeed
	float ___m_moveSpeed_4;
	// System.Single SimpleSampleCharacterControl::m_turnSpeed
	float ___m_turnSpeed_5;
	// System.Single SimpleSampleCharacterControl::m_jumpForce
	float ___m_jumpForce_6;
	// UnityEngine.Animator SimpleSampleCharacterControl::m_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ___m_animator_7;
	// UnityEngine.Rigidbody SimpleSampleCharacterControl::m_rigidBody
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ___m_rigidBody_8;
	// SimpleSampleCharacterControl/ControlMode SimpleSampleCharacterControl::m_controlMode
	int32_t ___m_controlMode_9;
	// System.Single SimpleSampleCharacterControl::m_currentV
	float ___m_currentV_10;
	// System.Single SimpleSampleCharacterControl::m_currentH
	float ___m_currentH_11;
	// System.Single SimpleSampleCharacterControl::m_interpolation
	float ___m_interpolation_12;
	// System.Single SimpleSampleCharacterControl::m_walkScale
	float ___m_walkScale_13;
	// System.Single SimpleSampleCharacterControl::m_backwardsWalkScale
	float ___m_backwardsWalkScale_14;
	// System.Single SimpleSampleCharacterControl::m_backwardRunScale
	float ___m_backwardRunScale_15;
	// System.Boolean SimpleSampleCharacterControl::m_wasGrounded
	bool ___m_wasGrounded_16;
	// UnityEngine.Vector3 SimpleSampleCharacterControl::m_currentDirection
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_currentDirection_17;
	// System.Single SimpleSampleCharacterControl::m_jumpTimeStamp
	float ___m_jumpTimeStamp_18;
	// System.Single SimpleSampleCharacterControl::m_minJumpInterval
	float ___m_minJumpInterval_19;
	// System.Boolean SimpleSampleCharacterControl::m_jumpInput
	bool ___m_jumpInput_20;
	// System.Boolean SimpleSampleCharacterControl::m_isGrounded
	bool ___m_isGrounded_21;
	// System.Collections.Generic.List`1<UnityEngine.Collider> SimpleSampleCharacterControl::m_collisions
	List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* ___m_collisions_22;
};

// KickMaster.StateMachine.State
struct State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Collections.Generic.List`1<KickMaster.StateMachine.Transition> KickMaster.StateMachine.State::_transitions
	List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* ____transitions_4;
	// KickMaster.Player KickMaster.StateMachine.State::<Target>k__BackingField
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___U3CTargetU3Ek__BackingField_5;
};

// KickMaster.StateMachine.Transition
struct Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// KickMaster.StateMachine.State KickMaster.StateMachine.Transition::_targetState
	State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* ____targetState_4;
	// KickMaster.Player KickMaster.StateMachine.Transition::<Target>k__BackingField
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___U3CTargetU3Ek__BackingField_5;
	// KickMaster.Enemy KickMaster.StateMachine.Transition::<Enemy>k__BackingField
	Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___U3CEnemyU3Ek__BackingField_6;
	// System.Boolean KickMaster.StateMachine.Transition::<NeedTransit>k__BackingField
	bool ___U3CNeedTransitU3Ek__BackingField_7;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// KickMaster.BaseEnemy
struct BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365  : public Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584
{
	// UnityEngine.Animator KickMaster.BaseEnemy::_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ____animator_7;
	// KickMaster.Kickable KickMaster.BaseEnemy::_kickable
	Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* ____kickable_8;
	// KickMaster.Hitable KickMaster.BaseEnemy::_hitable
	Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* ____hitable_9;
};

// KickMaster.BaseProjectile
struct BaseProjectile_t4DA45DEFCD7C1378B3DB667EAC4C2B9FED1D7D00  : public Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3
{
	// UnityEngine.Rigidbody KickMaster.BaseProjectile::_rigidbody
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ____rigidbody_8;
};

// KickMaster.Boss
struct Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412  : public Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584
{
	// KickMaster.Hitable KickMaster.Boss::_hitable
	Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* ____hitable_7;
};

// KickMaster.BossProjectile
struct BossProjectile_t544BE8E12437763EC7481788B2B11751B8F22C9A  : public Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3
{
	// System.Int32 KickMaster.BossProjectile::_damage
	int32_t ____damage_8;
	// UnityEngine.Rigidbody KickMaster.BossProjectile::_rigidbody
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* ____rigidbody_9;
};

// KickMaster.StateMachine.DetectionTransition
struct DetectionTransition_tBEE948ABDBDFD2F492ACAD4A9D093339769038D8  : public Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F
{
	// System.Single KickMaster.StateMachine.DetectionTransition::_targetDetectionDistance
	float ____targetDetectionDistance_8;
};

// KickMaster.StateMachine.DieState
struct DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3  : public State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93
{
	// UnityEngine.Animator KickMaster.StateMachine.DieState::_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ____animator_7;
};

// KickMaster.StateMachine.DieTransition
struct DieTransition_tA7FF60FF626EE5BE5A9E6EBF5243BC9EC6DE7684  : public Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F
{
};

// DynamicJoystick
struct DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10  : public Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A
{
	// System.Single DynamicJoystick::moveThreshold
	float ___moveThreshold_15;
};

// FixedJoystick
struct FixedJoystick_t7AA7F128A16A375A847AD0C7067993A6CC95DD7F  : public Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A
{
};

// FloatingJoystick
struct FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81  : public Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A
{
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_21;
};

// KickMaster.StateMachine.IdleState
struct IdleState_t58E9FE55FF2340984CDD43A38A3FB6BDF803CA68  : public State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93
{
};

// KickMaster.Kick
struct Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D  : public PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02
{
	// System.Single KickMaster.Kick::_force
	float ____force_4;
	// System.Collections.Generic.List`1<KickMaster.Kickable> KickMaster.Kick::_kickables
	List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* ____kickables_5;
	// System.Collections.Generic.List`1<KickMaster.Projectile> KickMaster.Kick::_projectiles
	List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* ____projectiles_6;
	// System.Collections.Generic.List`1<KickMaster.StateMachine.ThrowingState> KickMaster.Kick::_throwingStates
	List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* ____throwingStates_7;
};

// KickMaster.StateMachine.KillState
struct KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632  : public State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93
{
	// UnityEngine.Animator KickMaster.StateMachine.KillState::_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ____animator_7;
};

// KickMaster.StateMachine.KillTransition
struct KillTransition_t681FD7B428DEEF8F9F72FDC0B203922F7E7D8BFA  : public Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F
{
	// System.Single KickMaster.StateMachine.KillTransition::_transitionRange
	float ____transitionRange_8;
	// System.Single KickMaster.StateMachine.KillTransition::_spreadRange
	float ____spreadRange_9;
};

// KickMaster.StateMachine.MoveState
struct MoveState_tB658AA74A33C09F34C680910AE3A80B959935CA0  : public State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93
{
	// System.Single KickMaster.StateMachine.MoveState::_speed
	float ____speed_6;
	// UnityEngine.Animator KickMaster.StateMachine.MoveState::_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ____animator_7;
};

// KickMaster.StateMachine.TargetDieTransition
struct TargetDieTransition_t0935666A05E1F13EAAB8A8DEF693D2FCB5050637  : public Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F
{
};

// KickMaster.StateMachine.ThrowingState
struct ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC  : public State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93
{
	// UnityEngine.Events.UnityAction`1<KickMaster.Projectile> KickMaster.StateMachine.ThrowingState::ProjectileCreated
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___ProjectileCreated_7;
	// KickMaster.Projectile KickMaster.StateMachine.ThrowingState::_projectilePrefab
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ____projectilePrefab_8;
	// System.Single KickMaster.StateMachine.ThrowingState::_delay
	float ____delay_9;
	// UnityEngine.Transform KickMaster.StateMachine.ThrowingState::_holdPoint
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ____holdPoint_10;
	// System.Single KickMaster.StateMachine.ThrowingState::_force
	float ____force_11;
	// UnityEngine.Animator KickMaster.StateMachine.ThrowingState::_animator
	Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* ____animator_12;
	// UnityEngine.Coroutine KickMaster.StateMachine.ThrowingState::_throwCoroutine
	Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ____throwCoroutine_13;
};

// VariableJoystick
struct VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6  : public Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A
{
	// System.Single VariableJoystick::moveThreshold
	float ___moveThreshold_15;
	// JoystickType VariableJoystick::joystickType
	int32_t ___joystickType_16;
	// UnityEngine.Vector2 VariableJoystick::fixedPosition
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___fixedPosition_17;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_35;
};

// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;
};

struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19* ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_42;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_40;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B  : public RuntimeArray
{
	ALIGN_FIELD (8) Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* m_Items[1];

	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Animator[]
struct AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759  : public RuntimeArray
{
	ALIGN_FIELD (8) Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* m_Items[1];

	inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2  : public RuntimeArray
{
	ALIGN_FIELD (8) GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* m_Items[1];

	inline GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411  : public RuntimeArray
{
	ALIGN_FIELD (8) ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 m_Items[1];

	inline ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9 value)
	{
		m_Items[index] = value;
	}
};
// KickMaster.PlayerAction[]
struct PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE  : public RuntimeArray
{
	ALIGN_FIELD (8) PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* m_Items[1];

	inline PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponentInParent_TisRuntimeObject_m6746D6BB99912B1B509746C993906492F86CD119_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Object_FindObjectsOfType_TisRuntimeObject_m1E6D851F6A46D646E0554A94729E9AAE79B0E87A_gshared (const RuntimeMethod* method) ;
// T[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline (const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityAction_1_Invoke_m777839BF9CB9F96B081106B47202D06FB35326CA_gshared_inline (UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* __this, RuntimeObject* ___arg00, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared (UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::TryGetComponent<System.Object>(T&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_TryGetComponent_TisRuntimeObject_m4D430300D2DFB9276DE980D78F60A05D271D3630_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, RuntimeObject** ___component0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_m2A2DD50EC8EB54C91AF71E02DFD6969174D82B08_gshared (RuntimeObject* ___original0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation2, const RuntimeMethod* method) ;

// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_forward_mEBAB24D77FC02FC88ED880738C3B1D47C758B3EB_inline (const RuntimeMethod* method) ;
// System.Single Joystick::get_Vertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline (const RuntimeMethod* method) ;
// System.Single Joystick::get_Horizontal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_fixedDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_fixedDeltaTime_mD7107AF06157FC18A50E14E0755CEE137E9A4088 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC (Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___force0, int32_t ___mode1, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void VariableJoystick::SetMode(JoystickType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, int32_t ___joystickType0, const RuntimeMethod* method) ;
// System.Void Joystick::set_AxisOptions(AxisOptions)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE (Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* __this, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___value0, const RuntimeMethod* method) ;
// System.Void Joystick::set_SnapX(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void Joystick::set_SnapY(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 Joystick::get_Direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Vector2::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Vector2_ToString_mB47B29ECB21FA3A4ACEABEFA18077A5A6BBCCB27 (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Single Joystick::SnapFloat(System.Single,AxisOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, int32_t ___snapAxis1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// AxisOptions Joystick::get_AxisOptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// System.Void Joystick::set_HandleRange(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void Joystick::set_DeadZone(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
inline Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* Component_GetComponentInParent_TisCanvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_m5FB554DD7C0F662DAB84C0F292B221CAE3F0A5B3 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentInParent_TisRuntimeObject_m6746D6BB99912B1B509746C993906492F86CD119_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_pivot_m79D0177D383D432A93C2615F1932B739B1C6E146 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMin_m931442ABE3368D6D4309F43DF1D64AB64B0F52E3 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMax_m52829ABEDD229ABD3DA20BCA676FA1DCA4A39B7D (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) ;
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Canvas_get_renderMode_m1BEF259548C6CAD27E4466F31D20752D246688CC (Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* __this, const RuntimeMethod* method) ;
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* Canvas_get_worldCamera_mD2FDE13B61A5213F4E64B40008EB0A8D2D07B853 (Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransformUtility_WorldToScreenPoint_m5629068CE7C8D2E654F8F529E89DC5802F3452BB (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPoint1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Division_m69F64D545E3C023BE9927397572349A569141EBA_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Subtraction_m664419831773D5BBF06D9DE4E515F6409B2F92B8_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Canvas::get_scaleFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Canvas_get_scaleFactor_m6B8D694A68376EE5E13D9B0B0F037E2E90C99921 (Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Division_mB1CA903ACF933DB0BE2016D105BB2B4702CF1004_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// System.Void Joystick::FormatInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m5C59B4056420AEFDB291AD0914A3F675330A75CE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_normalized_mF6722883AEFB5027690A778DF8ACC20F0FA65297_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_up_mF4D6DB00DEA7D055940165B85FFE1CEF6F7CD3AA_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Angle_m9668B13074D1664DD192669C14B3A8FC01676299_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___from0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___to1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m9E502D410F5B141117D263D4706C426EFA109DC0 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___rect0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPoint1, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam2, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* ___localPoint3, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransform_get_pivot_mA8334AF05AA7FF09A173A2430F2BB9E85E5CBFFF (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransform_get_anchorMax_mEF870BE2A134CEB9C2326930A71D3961271297DB (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) ;
// System.Void DynamicJoystick::set_MoveThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, float ___value0, const RuntimeMethod* method) ;
// System.Void Joystick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition0, const RuntimeMethod* method) ;
// System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) ;
// System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransform_get_anchoredPosition_m38F25A4253B0905BB058BE73DBF43C7172CE0680 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, const RuntimeMethod* method) ;
// System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___magnitude0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___normalised1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___radius2, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam3, const RuntimeMethod* method) ;
// System.Void Joystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) ;
// T[] UnityEngine.Object::FindObjectsOfType<UnityEngine.Animator>()
inline AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* Object_FindObjectsOfType_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m70A4338B4AA009BCDF8C81C96967144DC94383FD (const RuntimeMethod* method)
{
	return ((  AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* (*) (const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m1E6D851F6A46D646E0554A94729E9AAE79B0E87A_gshared)(method);
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3 (int32_t ___key0, const RuntimeMethod* method) ;
// System.Void FreeCameraLogic::PreviousTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) ;
// System.Void FreeCameraLogic::NextTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* GUILayout_Width_m3CD0F9B520A1B7BF065D30844E2F9965277E1DAA (float ___width0, const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_BeginVertical_mEA80BF63631637EDAAD761D32BAFC49A404F0842 (GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* ___options0, const RuntimeMethod* method) ;
// T[] System.Array::Empty<UnityEngine.GUILayoutOption>()
inline GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline (const RuntimeMethod* method)
{
	return ((  GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline)(method);
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37 (GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* ___options0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GUILayout_Button_m6D4E3D32A001EF42DB5C2052B4C19AB3B518566C (String_t* ___text0, GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* ___options1, const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::EndHorizontal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC (const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::Space(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_Space_m9991854C4545EA58CAD8F949BF1FC1F89CF119FE (float ___pixels0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30 (Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* __this, String_t* ___name0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.GUI::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GUI_get_color_m641A7661D421929DB60FD1AC40E43F960CEC81C1 (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUI_set_color_mBB4E17B3600770E2EEEA61AA956D2207EAF112C7 (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::FlexibleSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE (const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_Label_mCB93C0DE81ECE87DE34C8B959C5885E9B6E4FEBA (String_t* ___text0, GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* ___options1, const RuntimeMethod* method) ;
// System.Void UnityEngine.GUILayout::EndVertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUILayout_EndVertical_mF2C806265D9B04E715EC1656FA9392332C59EEBC (const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
inline int32_t List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline (List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// T System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
inline Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA (List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* (*) (List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// System.Void FreeCameraLogic::SwitchTarget(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, int32_t ___step0, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_op_Multiply_mF1348668A6CCD46FBFF98D39182F89358ED74AC0 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___point1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_mFEF7353E4CAEB85D5F7CEEF9276C3B8D6E314C6C (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___worldPosition0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___exists0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* Collision_get_contacts_m2E8E27E0399230DFA4303A4F4D81C1BD55CBC473 (Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ContactPoint_get_normal_mD7F0567CA2FD68644F7C6FE318E10C4D15F92AD6 (ContactPoint_t241857959C0D517C21F541BB04B63FA6C1EAB3F9* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_m4688A1A524306675DBDB1E6D483F35E85E3CE6D8_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.Collider UnityEngine.Collision::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D (Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Collider>::Contains(T)
inline bool List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40 (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* __this, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76*, const RuntimeMethod*))List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Collider>::Add(T)
inline void List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_inline (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* __this, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Collider>::Remove(T)
inline bool List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* __this, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*, Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Collider>::get_Count()
inline int32_t List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_inline (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A (int32_t ___key0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m6F8D4FAF0770CD4EC1F54406249785DE7391E42B (Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method) ;
// System.Void SimpleSampleCharacterControl::DirectUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_DirectUpdate_m107B99848528F674872DB7A277D33514A3863D24 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) ;
// System.Void SimpleSampleCharacterControl::TankUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_TankUpdate_m3D7D92631D2653E40AD1EADDFD46395C099B2568 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4 (String_t* ___axisName0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_m10C78733FAFC7AFEDBDACC48B7C66D3A35A0A7FE (Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* __this, String_t* ___name0, float ___value1, const RuntimeMethod* method) ;
// System.Void SimpleSampleCharacterControl::JumpingAndLanding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) ;
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43 (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_right_mC6DC057C23313802E2186A9E0DB760D795A758A4 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m6A7FB1C9E9DE194708997BFA24C6E238D92D908E_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Slerp_mBA32C7EAC64C56C7D68480549FA9A892FA5C1728 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, float ___t2, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_LookRotation_m8C0F294E5143F93D378E020EAD9DA2288A5907A3 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forward0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m0BEE9AACD0723FE414465B77C9C64D12263675F3 (const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.Collider>::.ctor()
inline void List_1__ctor_m0CDD6F02F45026B4267E7117C5DDC188F87EE7BE (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<KickMaster.Hitable>()
inline Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<KickMaster.Kickable>()
inline Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* Component_GetComponent_TisKickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9_m909A71EC628FD746DBAE6837FEC8E63E358A6333 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131 (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void KickMaster.Kickable::add_Kicked(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_add_Kicked_m990B81847DFB334DE467B5F1A1E290CB133BA8A8 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Hitable::add_Hited(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Kickable::remove_Kicked(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_remove_Kicked_mE39AC028E50A7C98DDDA75E64C47C85736FB909E (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Hitable::remove_Hited(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85 (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m0E1B4CF8C29EB7FC8658C2C84C57F49C0DD12C91 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, float ___t1, const RuntimeMethod* method) ;
// System.Void KickMaster.Enemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.Boss::TakeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C (Delegate_t* ___a0, Delegate_t* ___b1, const RuntimeMethod* method) ;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116 (Delegate_t* ___source0, Delegate_t* ___value1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Enemy>::Invoke(T0)
inline void UnityAction_1_Invoke_mB4C2A9DB9EF0B918BA57AF733B43DD6DF6B8DDA4_inline (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584*, const RuntimeMethod*))UnityAction_1_Invoke_m777839BF9CB9F96B081106B47202D06FB35326CA_gshared_inline)(__this, ___arg00, method);
}
// System.Void KickMaster.Input::HandleTouchUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_HandleTouchUp_mE47FD3BE40F65821590376B7D7E1F8C279E71959 (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Input::get_touchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m7B8EAAB3449A6DC2D90AF3BA36AF226D97C020CF (const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction::Invoke()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<KickMaster.Enemy>::GetEnumerator()
inline Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 (*) (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<KickMaster.Enemy>::Dispose()
inline void Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<KickMaster.Enemy>::get_Current()
inline Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_inline (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63* __this, const RuntimeMethod* method)
{
	return ((  Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* (*) (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Enemy>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_mC4ED61FE700AFBEBC481EB5F15F0204D2CC9D298 (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*, RuntimeObject*, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void KickMaster.Enemy::add_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_add_Died_m96CD283CAC5DB65801E7397F01499B412C578F44 (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<KickMaster.Enemy>::MoveNext()
inline bool Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void KickMaster.Player::add_Died(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_add_Died_mDDA2A70A8728F794B1836BDE39FE0B873EE1F00F (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Enemy::remove_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_remove_Died_m775324A6A3CAC3EF965CEBD934F3BD61BDD1484E (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Player::remove_Died(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_remove_Died_mFC24CDAE84983636EADDA30EDEA10C7014D21713 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_tA1DC762B79745EB5140F054C884855B922318356 SceneManager_GetActiveScene_m2DB2A1ACB84805968A4B6396BFDFB92C0AF32BCE (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_m82B6E0C96C85C952B7A2D794DB73CDA99AA9A57E (Scene_tA1DC762B79745EB5140F054C884855B922318356* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mE00D17D79AD74B307F913BBF296A36115548DB6D (int32_t ___sceneBuildIndex0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<KickMaster.Enemy>::Remove(T)
inline bool List_1_Remove_m8EBB6F6ED60A4A01CC524FEFACF137C9C335DC2B (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B*, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<KickMaster.Enemy>::get_Count()
inline int32_t List_1_get_Count_m21D8D394A4B96B49B33EC956B7AFE650AB1572AB_inline (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Boolean UnityEngine.GameObject::TryGetComponent<KickMaster.Enemy>(T&)
inline bool GameObject_TryGetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_mFEF05EA9F9117D5DD9F989DAC47DD639ED62F6C2 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_m4D430300D2DFB9276DE980D78F60A05D271D3630_gshared)(__this, ___component0, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
// System.Void KickMaster.Projectile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::TryGetComponent<KickMaster.Player>(T&)
inline bool GameObject_TryGetComponent_TisPlayer_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5_m0A68D908EF4D6E6D6F7D73B2FA3EAB82A6BD1024 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_m4D430300D2DFB9276DE980D78F60A05D271D3630_gshared)(__this, ___component0, method);
}
// System.Void KickMaster.Player::TakeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_TakeDamage_mDBDD4B6DFAE12095463ED5CA009C5D36B1FB757A (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::TryGetComponent<UnityEngine.Rigidbody>(T&)
inline bool GameObject_TryGetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_mCA867A56E93C824FB9C272E0DBB67655CB275941 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, Rigidbody_t268697F5A994213ED97393309870968BC1C7393C** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Rigidbody_t268697F5A994213ED97393309870968BC1C7393C**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_m4D430300D2DFB9276DE980D78F60A05D271D3630_gshared)(__this, ___component0, method);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Rigidbody_get_velocity_mAE331303E7214402C93E2183D0AA1198F425F843 (Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_mE4031DF1C2C1CCE889F2AC9D8871D83795BB0D62 (Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Collision_get_gameObject_m846FADBCA43E1849D3FE4D5EA44C02D055A70B3E (Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::TryGetComponent<KickMaster.Projectile>(T&)
inline bool GameObject_TryGetComponent_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_mEEDC9F8E08419F16EA0E30DD83FBAC6238635A28 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_m4D430300D2DFB9276DE980D78F60A05D271D3630_gshared)(__this, ___component0, method);
}
// T UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Kickable>::Invoke(T0)
inline void UnityAction_1_Invoke_m984AA9226F8E104E628101144CA0C6054925C662_inline (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* __this, Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*, Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9*, const RuntimeMethod*))UnityAction_1_Invoke_m777839BF9CB9F96B081106B47202D06FB35326CA_gshared_inline)(__this, ___arg00, method);
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Projectile>::Invoke(T0)
inline void UnityAction_1_Invoke_mC1007382E3AC52C15BE12F47ADE7E9951F6559F1_inline (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3*, const RuntimeMethod*))UnityAction_1_Invoke_m777839BF9CB9F96B081106B47202D06FB35326CA_gshared_inline)(__this, ___arg00, method);
}
// System.Void KickMaster.Pause::ResetPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_ResetPause_m12F854B48693949D669F711CA5DDFD71D2FDD98F (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.Level::add_Won(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Level::add_Defeated(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Level::remove_Won(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Level::remove_Defeated(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_mD6CAA4968D796C4AF198ACFB2267BDBD06DB349C (float ___value0, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<KickMaster.StateMachine.ThrowingState>::GetEnumerator()
inline Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2 (List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 (*) (List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<KickMaster.StateMachine.ThrowingState>::Dispose()
inline void Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530 (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<KickMaster.StateMachine.ThrowingState>::get_Current()
inline ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_inline (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313* __this, const RuntimeMethod* method)
{
	return ((  ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* (*) (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Projectile>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*, RuntimeObject*, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void KickMaster.StateMachine.ThrowingState::add_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_add_ProjectileCreated_m393F0C3F7147E89601AE303379137A2922597AE0 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<KickMaster.StateMachine.ThrowingState>::MoveNext()
inline bool Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<KickMaster.Projectile>::GetEnumerator()
inline Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522 (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB (*) (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<KickMaster.Projectile>::Dispose()
inline void Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4 (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<KickMaster.Projectile>::get_Current()
inline Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_inline (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB* __this, const RuntimeMethod* method)
{
	return ((  Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* (*) (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Void KickMaster.Projectile::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<KickMaster.Projectile>::MoveNext()
inline bool Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06 (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<KickMaster.Kickable>::GetEnumerator()
inline Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 (*) (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<KickMaster.Kickable>::Dispose()
inline void Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<KickMaster.Kickable>::get_Current()
inline Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_inline (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4* __this, const RuntimeMethod* method)
{
	return ((  Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* (*) (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Events.UnityAction`1<KickMaster.Kickable>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_mF4C17AF6C00B0D1959A3ED418CEFB50431B3E4A9 (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*, RuntimeObject*, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m0C2FC6B483B474AE9596A43EBA7FF6E85503A92A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void KickMaster.Kickable::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_add_Destroyed_m26D1607171283C13B73D0DECE05776DD7FD3894B (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* ___value0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<KickMaster.Kickable>::MoveNext()
inline bool Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1 (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void KickMaster.StateMachine.ThrowingState::remove_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_remove_ProjectileCreated_mCC5531DCD64836A19173BCE84635F2FD5EFDE086 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Projectile::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Kickable::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* ___value0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<KickMaster.Projectile>::Add(T)
inline void List_1_Add_m7C6294F5B3C8ECA89999B8E4578ABBEB1A3B77C3_inline (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F*, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<KickMaster.Projectile>::Remove(T)
inline bool List_1_Remove_m48E0BD9D0F4E6BF369753EFFE945A863DAFF8ABC (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F*, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<KickMaster.Kickable>::Remove(T)
inline bool List_1_Remove_m4873E203826D9E39239EDDF86822BF6E8C4DDB1E (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* __this, Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3*, Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// System.Boolean KickMaster.Projectile::IsInRange(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Projectile_IsInRange_m06E3AD8E3349516B0C3AA623100F84073083784F (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___source0, const RuntimeMethod* method) ;
// System.Boolean KickMaster.Kickable::IsInRange(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Kickable_IsInRange_m0AEC3C5E9368DD943DAD6456F6A6669612E8E6B9 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___source0, const RuntimeMethod* method) ;
// System.Void KickMaster.Kickable::Kick(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_Kick_mF8DB1FD38E7E1778FBFA3DDB8E02BB7D94EC4B9E (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___force0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<KickMaster.Kickable>::.ctor()
inline void List_1__ctor_mAC29D9D5108AA6D94BA97759E1E1680504442F9F (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<KickMaster.Projectile>::.ctor()
inline void List_1__ctor_mAEC3C87A864E42B46F6D42A695E4F28235434A1A (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<KickMaster.StateMachine.ThrowingState>::.ctor()
inline void List_1__ctor_mEE327D7F83780A435BB6EDDDA3F1898A8EF032E8 (List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void KickMaster.PlayerAction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAction__ctor_m2F60E6DB5734572075E34991A6DD818B68C9BF8B (PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.Player::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Die_m42DB081342293F085036E90E510C58BAA26DD741 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.Input::add_FingerUp(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_add_FingerUp_m4BC4A64D65E35BB62295E3C201DD91FA52A9A389 (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.Input::remove_FingerUp(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_remove_FingerUp_m2B42E32D216FFC3D5A2167ABBA3B7C0F0E79E21D (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Animation::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Animation_Play_m717560D2F561D9E12583AB3B435E6BC996448C3E (Animation_t6593B06C39E3B139808B19F2C719C860F3F61040* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
inline CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* Component_GetComponent_TisCharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A_m96B48A644EDC97C5C82F154D1FEA551B2E392040 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void KickMaster.PlayerMove::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove_Move_mB2A2C255A530412FC82230FC4C496BFFE0EB2AAE (PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CharacterController_Move_mE3F7AC1B4A2D6955980811C088B68ED3A31D2DA4 (CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___motion0, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<KickMaster.Enemy>()
inline Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// KickMaster.Player KickMaster.Enemy::get_Target()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294_inline (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.EnemyStateMachine::ResetState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_ResetState_m4B33D21E43676185CCA7A69618BD8510FB73FD48 (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) ;
// KickMaster.StateMachine.State KickMaster.StateMachine.State::GetNextState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* State_GetNextState_m6BC7F69BE96489665470DB3B0126DF69D5E3E9E8 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.EnemyStateMachine::Transit(KickMaster.StateMachine.State)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_Transit_mAC3F54831E0C7B635B3DFD3D8B348D576BA75BAB (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* ___state0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.State::Enter(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___target0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.State::Exit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State_Exit_m21D27445EFB2088E0D3CAD411B95DF23C3583918 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_mAAC9F15E9EBF552217A5AE2681589CC0BFA300C1 (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.State::set_Target(KickMaster.Player)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0_inline (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, bool ___value0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<KickMaster.StateMachine.Transition>::get_Item(System.Int32)
inline Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3 (List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* (*) (List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// KickMaster.Player KickMaster.StateMachine.State::get_Target()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.Transition::Init(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_Init_m3735DD0482CD276E2D9D25FF21691DA308A116A0 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___target0, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<KickMaster.StateMachine.Transition>::get_Count()
inline int32_t List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_inline (List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Boolean KickMaster.StateMachine.Transition::get_NeedTransit()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) ;
// KickMaster.StateMachine.State KickMaster.StateMachine.Transition::get_TargetState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator KickMaster.StateMachine.DieState::DieWithDelay(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DieState_DieWithDelay_mBC37AC486AD23B02A6CB6F553592B07C792CE6F5 (DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* __this, float ___delay0, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDieWithDelayU3Ed__3__ctor_m3F40ED80C5889B224BCC341C9877B2E5648EB663 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.State::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___seconds0, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator KickMaster.StateMachine.KillState::KillWithDelay(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* KillState_KillWithDelay_mA5485FB293E5DE76F1758611233AB762CFF6D1F7 (KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* __this, float ___delay0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithDelayU3Ed__3__ctor_mF0D47A051DBE7501949B51D317BA36AAF27ADDBC (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void KickMaster.Player::Kill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Kill_mD2AACC78D1E870C57334FB25CBA424C9CB984025 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) ;
// System.Collections.IEnumerator KickMaster.StateMachine.ThrowingState::StartThrowing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ThrowingState_StartThrowing_m30EADAC80898A10EA73D3345393788483FB1C95E (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_mB0FC91BE84203BD8E360B3FBAE5B958B4C5ED22A (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ___routine0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartThrowingU3Ed__12__ctor_mF45E77C4F928D1D962A53C49DF89DA1B9424C263 (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline (const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<KickMaster.Projectile>(T,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* Object_Instantiate_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_m74F486DA40756F8EFD2B5EAE87CB856E511F4BF9 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___original0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation2, const RuntimeMethod* method)
{
	return ((  Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* (*) (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m2A2DD50EC8EB54C91AF71E02DFD6969174D82B08_gshared)(___original0, ___position1, ___rotation2, method);
}
// KickMaster.Projectile KickMaster.StateMachine.ThrowingState::CreateProjectile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ThrowingState_CreateProjectile_m0C9DE1A31F8FF958DB30D428DF7679343F1821FB (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.ThrowingState::ThrowProjectile(KickMaster.Projectile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_ThrowProjectile_mEA5D18F8A1AA4ED87A1ADB5A7569B20CB8385793 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___projectile0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.Transition::set_Target(KickMaster.Player)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.Transition::set_Enemy(KickMaster.Enemy)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___value0, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.Transition::set_NeedTransit(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, bool ___value0, const RuntimeMethod* method) ;
// KickMaster.Player KickMaster.StateMachine.Transition::get_Target()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) ;
// System.Void KickMaster.StateMachine.Transition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) ;
// KickMaster.Enemy KickMaster.StateMachine.Transition::get_Enemy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) ;
// System.Int32 KickMaster.Enemy::get_Health()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68_inline (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mF26F26EB446B76823B4815C91FA0907B484DF02B (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::Normalize()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2_Normalize_m56DABCAB5967DF37A6B96710477D3660D800C652_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mBF0FA0B529C821F4733DDC3AD366B07CD27625F4_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_m6120F119433C5B60BBB28731D3D4A0DA50A84DDD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m15951D1B53E3BE36C9D265E229090020FBD72EBB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m6AD0BEBF88AAF98188A851E62D7A32CB5B7830EF_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mD7200D6D432BAFC4135C5B17A0B0A812203B0270_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JoystickPlayerExample::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickPlayerExample_FixedUpdate_m9AEDBA111F95D67A006A5D3821956048224541B7 (JoystickPlayerExample_tD6DD431AB5B17F44428C240223A7B161AECC474B* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_forward_mEBAB24D77FC02FC88ED880738C3B1D47C758B3EB_inline(NULL);
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_1 = __this->___variableJoystick_5;
		NullCheck(L_1);
		float L_2;
		L_2 = Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE(L_1, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_0, L_2, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline(NULL);
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_5 = __this->___variableJoystick_5;
		NullCheck(L_5);
		float L_6;
		L_6 = Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA(L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_4, L_6, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_3, L_7, NULL);
		V_0 = L_8;
		// rb.AddForce(direction * speed * Time.fixedDeltaTime, ForceMode.VelocityChange);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_9 = __this->___rb_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		float L_11 = __this->___speed_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_10, L_11, NULL);
		float L_13;
		L_13 = Time_get_fixedDeltaTime_mD7107AF06157FC18A50E14E0755CEE137E9A4088(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		L_14 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_12, L_13, NULL);
		NullCheck(L_9);
		Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC(L_9, L_14, 2, NULL);
		// }
		return;
	}
}
// System.Void JoystickPlayerExample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickPlayerExample__ctor_m702422E0AE29402330CF41FDDBEE76F0506342E2 (JoystickPlayerExample_tD6DD431AB5B17F44428C240223A7B161AECC474B* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JoystickSetterExample::ModeChanged(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample_ModeChanged_m35AF30EE3E6C8CEBF064A7AB80F5795E9AF06D23 (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, int32_t ___index0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0013;
			}
			case 1:
			{
				goto IL_0020;
			}
			case 2:
			{
				goto IL_002d;
			}
		}
	}
	{
		return;
	}

IL_0013:
	{
		// variableJoystick.SetMode(JoystickType.Fixed);
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_1 = __this->___variableJoystick_4;
		NullCheck(L_1);
		VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE(L_1, 0, NULL);
		// break;
		return;
	}

IL_0020:
	{
		// variableJoystick.SetMode(JoystickType.Floating);
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_2 = __this->___variableJoystick_4;
		NullCheck(L_2);
		VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE(L_2, 1, NULL);
		// break;
		return;
	}

IL_002d:
	{
		// variableJoystick.SetMode(JoystickType.Dynamic);
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_3 = __this->___variableJoystick_4;
		NullCheck(L_3);
		VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE(L_3, 2, NULL);
		// }
		return;
	}
}
// System.Void JoystickSetterExample::AxisChanged(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample_AxisChanged_m5CA220FEA14E06BD8A445E31C5B66E4601C5E404 (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, int32_t ___index0, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0013;
			}
			case 1:
			{
				goto IL_0033;
			}
			case 2:
			{
				goto IL_0053;
			}
		}
	}
	{
		return;
	}

IL_0013:
	{
		// variableJoystick.AxisOptions = AxisOptions.Both;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_1 = __this->___variableJoystick_4;
		NullCheck(L_1);
		Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6_inline(L_1, 0, NULL);
		// background.sprite = axisSprites[index];
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_2 = __this->___background_6;
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_3 = __this->___axisSprites_7;
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_2);
		Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE(L_2, L_6, NULL);
		// break;
		return;
	}

IL_0033:
	{
		// variableJoystick.AxisOptions = AxisOptions.Horizontal;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_7 = __this->___variableJoystick_4;
		NullCheck(L_7);
		Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6_inline(L_7, 1, NULL);
		// background.sprite = axisSprites[index];
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_8 = __this->___background_6;
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_9 = __this->___axisSprites_7;
		int32_t L_10 = ___index0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_8);
		Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE(L_8, L_12, NULL);
		// break;
		return;
	}

IL_0053:
	{
		// variableJoystick.AxisOptions = AxisOptions.Vertical;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_13 = __this->___variableJoystick_4;
		NullCheck(L_13);
		Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6_inline(L_13, 2, NULL);
		// background.sprite = axisSprites[index];
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_14 = __this->___background_6;
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_15 = __this->___axisSprites_7;
		int32_t L_16 = ___index0;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_14);
		Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE(L_14, L_18, NULL);
		// }
		return;
	}
}
// System.Void JoystickSetterExample::SnapX(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample_SnapX_m25A77C3DE4C6DBBE3B4A58E2DE8CD44B1773D6A1 (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// variableJoystick.SnapX = value;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_0 = __this->___variableJoystick_4;
		bool L_1 = ___value0;
		NullCheck(L_0);
		Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A_inline(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void JoystickSetterExample::SnapY(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample_SnapY_m54FE8DCB2CE8D2BF5D2CDF84C68DE263F0E25B1B (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// variableJoystick.SnapY = value;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_0 = __this->___variableJoystick_4;
		bool L_1 = ___value0;
		NullCheck(L_0);
		Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86_inline(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void JoystickSetterExample::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample_Update_m99B2432D22FE669B4DC3209696AD4A62096C7D41 (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFDA32DC2E96C00474CE484C62A98501A5FB8786E);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// valueText.text = "Current Value: " + variableJoystick.Direction;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___valueText_5;
		VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* L_1 = __this->___variableJoystick_4;
		NullCheck(L_1);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC(L_1, NULL);
		V_0 = L_2;
		String_t* L_3;
		L_3 = Vector2_ToString_mB47B29ECB21FA3A4ACEABEFA18077A5A6BBCCB27((&V_0), NULL);
		String_t* L_4;
		L_4 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralFDA32DC2E96C00474CE484C62A98501A5FB8786E, L_3, NULL);
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		// }
		return;
	}
}
// System.Void JoystickSetterExample::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void JoystickSetterExample__ctor_m2A3D66E05BCDF78D0F116348094717BEBA73EC91 (JoystickSetterExample_t496DF774BC6E49244893C8EBE104A7972836817E* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Joystick::get_Horizontal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public float Horizontal { get { return (snapX) ? SnapFloat(input.x, AxisOptions.Horizontal) : input.x; } }
		bool L_0 = __this->___snapX_7;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_1 = (&__this->___input_14);
		float L_2 = L_1->___x_0;
		return L_2;
	}

IL_0014:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_3 = (&__this->___input_14);
		float L_4 = L_3->___x_0;
		float L_5;
		L_5 = Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987(__this, L_4, 1, NULL);
		return L_5;
	}
}
// System.Single Joystick::get_Vertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public float Vertical { get { return (snapY) ? SnapFloat(input.y, AxisOptions.Vertical) : input.y; } }
		bool L_0 = __this->___snapY_8;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_1 = (&__this->___input_14);
		float L_2 = L_1->___y_1;
		return L_2;
	}

IL_0014:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_3 = (&__this->___input_14);
		float L_4 = L_3->___y_1;
		float L_5;
		L_5 = Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987(__this, L_4, 2, NULL);
		return L_5;
	}
}
// UnityEngine.Vector2 Joystick::get_Direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public Vector2 Direction { get { return new Vector2(Horizontal, Vertical); } }
		float L_0;
		L_0 = Joystick_get_Horizontal_m78CF4472B86063E54254AC8AE0A52126E4008AFA(__this, NULL);
		float L_1;
		L_1 = Joystick_get_Vertical_mA2B0917896CF9CE47A6D342D1734E43441C3D4BE(__this, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single Joystick::get_HandleRange()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_HandleRange_mB38F0D3B6ADE2D1557D7A3D6759C469F17509D77 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// get { return handleRange; }
		float L_0 = __this->___handleRange_4;
		return L_0;
	}
}
// System.Void Joystick::set_HandleRange(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { handleRange = Mathf.Abs(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = fabsf(L_0);
		__this->___handleRange_4 = L_1;
		// set { handleRange = Mathf.Abs(value); }
		return;
	}
}
// System.Single Joystick::get_DeadZone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_get_DeadZone_mCE52B635A8CF24F6DD867C14E34094515DE6AEFC (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// get { return deadZone; }
		float L_0 = __this->___deadZone_5;
		return L_0;
	}
}
// System.Void Joystick::set_DeadZone(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// set { deadZone = Mathf.Abs(value); }
		float L_0 = ___value0;
		float L_1;
		L_1 = fabsf(L_0);
		__this->___deadZone_5 = L_1;
		// set { deadZone = Mathf.Abs(value); }
		return;
	}
}
// AxisOptions Joystick::get_AxisOptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public AxisOptions AxisOptions { get { return AxisOptions; } set { axisOptions = value; } }
		int32_t L_0;
		L_0 = Joystick_get_AxisOptions_mA74F5FEE31C158E5179F0B108272ED28A661E388(__this, NULL);
		return L_0;
	}
}
// System.Void Joystick::set_AxisOptions(AxisOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// public AxisOptions AxisOptions { get { return AxisOptions; } set { axisOptions = value; } }
		int32_t L_0 = ___value0;
		__this->___axisOptions_6 = L_0;
		// public AxisOptions AxisOptions { get { return AxisOptions; } set { axisOptions = value; } }
		return;
	}
}
// System.Boolean Joystick::get_SnapX()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Joystick_get_SnapX_m51CAFDCC399606BA82986908700AAA45668A0F40 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public bool SnapX { get { return snapX; } set { snapX = value; } }
		bool L_0 = __this->___snapX_7;
		return L_0;
	}
}
// System.Void Joystick::set_SnapX(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool SnapX { get { return snapX; } set { snapX = value; } }
		bool L_0 = ___value0;
		__this->___snapX_7 = L_0;
		// public bool SnapX { get { return snapX; } set { snapX = value; } }
		return;
	}
}
// System.Boolean Joystick::get_SnapY()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Joystick_get_SnapY_m35AFC1AD1DF5EDE5FCE8BAFEBE91AD51D7451613 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// public bool SnapY { get { return snapY; } set { snapY = value; } }
		bool L_0 = __this->___snapY_8;
		return L_0;
	}
}
// System.Void Joystick::set_SnapY(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool SnapY { get { return snapY; } set { snapY = value; } }
		bool L_0 = ___value0;
		__this->___snapY_8 = L_0;
		// public bool SnapY { get { return snapY; } set { snapY = value; } }
		return;
	}
}
// System.Void Joystick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInParent_TisCanvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_m5FB554DD7C0F662DAB84C0F292B221CAE3F0A5B3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3C95DC36BC35028D124872E120F7701E290A66F6);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// HandleRange = handleRange;
		float L_0 = __this->___handleRange_4;
		Joystick_set_HandleRange_m686B579A1F02EFCD4878BEA27EA606FC23CD2505(__this, L_0, NULL);
		// DeadZone = deadZone;
		float L_1 = __this->___deadZone_5;
		Joystick_set_DeadZone_mD5699A14E5284026F303C8AF8D3457DFA9920F19(__this, L_1, NULL);
		// baseRect = GetComponent<RectTransform>();
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_2;
		L_2 = Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4(__this, Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var);
		__this->___baseRect_11 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___baseRect_11), (void*)L_2);
		// canvas = GetComponentInParent<Canvas>();
		Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* L_3;
		L_3 = Component_GetComponentInParent_TisCanvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_m5FB554DD7C0F662DAB84C0F292B221CAE3F0A5B3(__this, Component_GetComponentInParent_TisCanvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26_m5FB554DD7C0F662DAB84C0F292B221CAE3F0A5B3_RuntimeMethod_var);
		__this->___canvas_12 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___canvas_12), (void*)L_3);
		// if (canvas == null)
		Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* L_4 = __this->___canvas_12;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		// Debug.LogError("The Joystick is not placed inside a canvas");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(_stringLiteral3C95DC36BC35028D124872E120F7701E290A66F6, NULL);
	}

IL_0048:
	{
		// Vector2 center = new Vector2(0.5f, 0.5f);
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), (0.5f), (0.5f), NULL);
		// background.pivot = center;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_6 = __this->___background_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		NullCheck(L_6);
		RectTransform_set_pivot_m79D0177D383D432A93C2615F1932B739B1C6E146(L_6, L_7, NULL);
		// handle.anchorMin = center;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_8 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		NullCheck(L_8);
		RectTransform_set_anchorMin_m931442ABE3368D6D4309F43DF1D64AB64B0F52E3(L_8, L_9, NULL);
		// handle.anchorMax = center;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_10 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11 = V_0;
		NullCheck(L_10);
		RectTransform_set_anchorMax_m52829ABEDD229ABD3DA20BCA676FA1DCA4A39B7D(L_10, L_11, NULL);
		// handle.pivot = center;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_12 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13 = V_0;
		NullCheck(L_12);
		RectTransform_set_pivot_m79D0177D383D432A93C2615F1932B739B1C6E146(L_12, L_13, NULL);
		// handle.anchoredPosition = Vector2.zero;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_14 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		L_15 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		NullCheck(L_14);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_14, L_15, NULL);
		// }
		return;
	}
}
// System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// OnDrag(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_0 = ___eventData0;
		Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnDrag_m39E69636AEDC0E471EAD1371A99F4694ECDBA1E9 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// cam = null;
		__this->___cam_13 = (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cam_13), (void*)(Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184*)NULL);
		// if (canvas.renderMode == RenderMode.ScreenSpaceCamera)
		Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* L_0 = __this->___canvas_12;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Canvas_get_renderMode_m1BEF259548C6CAD27E4466F31D20752D246688CC(L_0, NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0026;
		}
	}
	{
		// cam = canvas.worldCamera;
		Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* L_2 = __this->___canvas_12;
		NullCheck(L_2);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_3;
		L_3 = Canvas_get_worldCamera_mD2FDE13B61A5213F4E64B40008EB0A8D2D07B853(L_2, NULL);
		__this->___cam_13 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cam_13), (void*)L_3);
	}

IL_0026:
	{
		// Vector2 position = RectTransformUtility.WorldToScreenPoint(cam, background.position);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_4 = __this->___cam_13;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_5 = __this->___background_9;
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_5, NULL);
		il2cpp_codegen_runtime_class_init_inline(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_il2cpp_TypeInfo_var);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7;
		L_7 = RectTransformUtility_WorldToScreenPoint_m5629068CE7C8D2E654F8F529E89DC5802F3452BB(L_4, L_6, NULL);
		V_0 = L_7;
		// Vector2 radius = background.sizeDelta / 2;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_8 = __this->___background_9;
		NullCheck(L_8);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9;
		L_9 = RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB(L_8, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10;
		L_10 = Vector2_op_Division_m69F64D545E3C023BE9927397572349A569141EBA_inline(L_9, (2.0f), NULL);
		V_1 = L_10;
		// input = (eventData.position - position) / (radius * canvas.scaleFactor);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_11 = ___eventData0;
		NullCheck(L_11);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		L_12 = PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline(L_11, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14;
		L_14 = Vector2_op_Subtraction_m664419831773D5BBF06D9DE4E515F6409B2F92B8_inline(L_12, L_13, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15 = V_1;
		Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* L_16 = __this->___canvas_12;
		NullCheck(L_16);
		float L_17;
		L_17 = Canvas_get_scaleFactor_m6B8D694A68376EE5E13D9B0B0F037E2E90C99921(L_16, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18;
		L_18 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_15, L_17, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_19;
		L_19 = Vector2_op_Division_mB1CA903ACF933DB0BE2016D105BB2B4702CF1004_inline(L_14, L_18, NULL);
		__this->___input_14 = L_19;
		// FormatInput();
		Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342(__this, NULL);
		// HandleInput(input.magnitude, input.normalized, radius, cam);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_20 = (&__this->___input_14);
		float L_21;
		L_21 = Vector2_get_magnitude_m5C59B4056420AEFDB291AD0914A3F675330A75CE_inline(L_20, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_22 = (&__this->___input_14);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_23;
		L_23 = Vector2_get_normalized_mF6722883AEFB5027690A778DF8ACC20F0FA65297_inline(L_22, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_24 = V_1;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_25 = __this->___cam_13;
		VirtualActionInvoker4< float, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* >::Invoke(9 /* System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera) */, __this, L_21, L_23, L_24, L_25);
		// handle.anchoredPosition = input * radius * handleRange;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_26 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_27 = __this->___input_14;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_28 = V_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_29;
		L_29 = Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline(L_27, L_28, NULL);
		float L_30 = __this->___handleRange_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31;
		L_31 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_29, L_30, NULL);
		NullCheck(L_26);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_26, L_31, NULL);
		// }
		return;
	}
}
// System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___magnitude0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___normalised1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___radius2, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam3, const RuntimeMethod* method) 
{
	{
		// if (magnitude > deadZone)
		float L_0 = ___magnitude0;
		float L_1 = __this->___deadZone_5;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0019;
		}
	}
	{
		// if (magnitude > 1)
		float L_2 = ___magnitude0;
		if ((!(((float)L_2) > ((float)(1.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		// input = normalised;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___normalised1;
		__this->___input_14 = L_3;
		return;
	}

IL_0019:
	{
		// input = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		L_4 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___input_14 = L_4;
	}

IL_0024:
	{
		// }
		return;
	}
}
// System.Void Joystick::FormatInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_FormatInput_mDDF7AF40138CF227F0106811C8749180FBF45342 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// if (axisOptions == AxisOptions.Horizontal)
		int32_t L_0 = __this->___axisOptions_6;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0025;
		}
	}
	{
		// input = new Vector2(input.x, 0f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_1 = (&__this->___input_14);
		float L_2 = L_1->___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_3), L_2, (0.0f), /*hidden argument*/NULL);
		__this->___input_14 = L_3;
		return;
	}

IL_0025:
	{
		// else if (axisOptions == AxisOptions.Vertical)
		int32_t L_4 = __this->___axisOptions_6;
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_0049;
		}
	}
	{
		// input = new Vector2(0f, input.y);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* L_5 = (&__this->___input_14);
		float L_6 = L_5->___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_7), (0.0f), L_6, /*hidden argument*/NULL);
		__this->___input_14 = L_7;
	}

IL_0049:
	{
		// }
		return;
	}
}
// System.Single Joystick::SnapFloat(System.Single,AxisOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Joystick_SnapFloat_mADE5AF21C67A2298A08CD12F9A8ED73AFA866987 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, float ___value0, int32_t ___snapAxis1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	int32_t G_B10_0 = 0;
	int32_t G_B18_0 = 0;
	{
		// if (value == 0)
		float L_0 = ___value0;
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_000a;
		}
	}
	{
		// return value;
		float L_1 = ___value0;
		return L_1;
	}

IL_000a:
	{
		// if (axisOptions == AxisOptions.Both)
		int32_t L_2 = __this->___axisOptions_6;
		if (L_2)
		{
			goto IL_0075;
		}
	}
	{
		// float angle = Vector2.Angle(input, Vector2.up);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = __this->___input_14;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		L_4 = Vector2_get_up_mF4D6DB00DEA7D055940165B85FFE1CEF6F7CD3AA_inline(NULL);
		float L_5;
		L_5 = Vector2_Angle_m9668B13074D1664DD192669C14B3A8FC01676299_inline(L_3, L_4, NULL);
		V_0 = L_5;
		// if (snapAxis == AxisOptions.Horizontal)
		int32_t L_6 = ___snapAxis1;
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_004b;
		}
	}
	{
		// if (angle < 22.5f || angle > 157.5f)
		float L_7 = V_0;
		if ((((float)L_7) < ((float)(22.5f))))
		{
			goto IL_0037;
		}
	}
	{
		float L_8 = V_0;
		if ((!(((float)L_8) > ((float)(157.5f)))))
		{
			goto IL_003d;
		}
	}

IL_0037:
	{
		// return 0;
		return (0.0f);
	}

IL_003d:
	{
		// return (value > 0) ? 1 : -1;
		float L_9 = ___value0;
		if ((((float)L_9) > ((float)(0.0f))))
		{
			goto IL_0048;
		}
	}
	{
		G_B10_0 = (-1);
		goto IL_0049;
	}

IL_0048:
	{
		G_B10_0 = 1;
	}

IL_0049:
	{
		return ((float)G_B10_0);
	}

IL_004b:
	{
		// else if (snapAxis == AxisOptions.Vertical)
		int32_t L_10 = ___snapAxis1;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0073;
		}
	}
	{
		// if (angle > 67.5f && angle < 112.5f)
		float L_11 = V_0;
		if ((!(((float)L_11) > ((float)(67.5f)))))
		{
			goto IL_0065;
		}
	}
	{
		float L_12 = V_0;
		if ((!(((float)L_12) < ((float)(112.5f)))))
		{
			goto IL_0065;
		}
	}
	{
		// return 0;
		return (0.0f);
	}

IL_0065:
	{
		// return (value > 0) ? 1 : -1;
		float L_13 = ___value0;
		if ((((float)L_13) > ((float)(0.0f))))
		{
			goto IL_0070;
		}
	}
	{
		G_B18_0 = (-1);
		goto IL_0071;
	}

IL_0070:
	{
		G_B18_0 = 1;
	}

IL_0071:
	{
		return ((float)G_B18_0);
	}

IL_0073:
	{
		// return value;
		float L_14 = ___value0;
		return L_14;
	}

IL_0075:
	{
		// if (value > 0)
		float L_15 = ___value0;
		if ((!(((float)L_15) > ((float)(0.0f)))))
		{
			goto IL_0083;
		}
	}
	{
		// return 1;
		return (1.0f);
	}

IL_0083:
	{
		// if (value < 0)
		float L_16 = ___value0;
		if ((!(((float)L_16) < ((float)(0.0f)))))
		{
			goto IL_0091;
		}
	}
	{
		// return -1;
		return (-1.0f);
	}

IL_0091:
	{
		// return 0;
		return (0.0f);
	}
}
// System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// input = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___input_14 = L_0;
		// handle.anchoredPosition = Vector2.zero;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_1 = __this->___handle_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_1, L_2, NULL);
		// }
		return;
	}
}
// UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024 (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___screenPosition0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector2 localPoint = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		V_0 = L_0;
		// if (RectTransformUtility.ScreenPointToLocalPointInRectangle(baseRect, screenPosition, cam, out localPoint))
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_1 = __this->___baseRect_11;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___screenPosition0;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_3 = __this->___cam_13;
		il2cpp_codegen_runtime_class_init_inline(RectTransformUtility_t65C00A84A72F17D78B81F2E7D88C2AA98AB61244_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m9E502D410F5B141117D263D4706C426EFA109DC0(L_1, L_2, L_3, (&V_0), NULL);
		if (!L_4)
		{
			goto IL_0060;
		}
	}
	{
		// Vector2 pivotOffset = baseRect.pivot * baseRect.sizeDelta;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_5 = __this->___baseRect_11;
		NullCheck(L_5);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		L_6 = RectTransform_get_pivot_mA8334AF05AA7FF09A173A2430F2BB9E85E5CBFFF(L_5, NULL);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_7 = __this->___baseRect_11;
		NullCheck(L_7);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		L_8 = RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB(L_7, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9;
		L_9 = Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline(L_6, L_8, NULL);
		V_1 = L_9;
		// return localPoint - (background.anchorMax * baseRect.sizeDelta) + pivotOffset;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10 = V_0;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_11 = __this->___background_9;
		NullCheck(L_11);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		L_12 = RectTransform_get_anchorMax_mEF870BE2A134CEB9C2326930A71D3961271297DB(L_11, NULL);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_13 = __this->___baseRect_11;
		NullCheck(L_13);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14;
		L_14 = RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB(L_13, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		L_15 = Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline(L_12, L_14, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		L_16 = Vector2_op_Subtraction_m664419831773D5BBF06D9DE4E515F6409B2F92B8_inline(L_10, L_15, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17 = V_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18;
		L_18 = Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline(L_16, L_17, NULL);
		return L_18;
	}

IL_0060:
	{
		// return Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_19;
		L_19 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		return L_19;
	}
}
// System.Void Joystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, const RuntimeMethod* method) 
{
	{
		// [SerializeField] private float handleRange = 1;
		__this->___handleRange_4 = (1.0f);
		// private Vector2 input = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___input_14 = L_0;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single DynamicJoystick::get_MoveThreshold()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float DynamicJoystick_get_MoveThreshold_m16C670C1DA0A45E83F6F87C4304F459EDDEEDD5A (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, const RuntimeMethod* method) 
{
	{
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		float L_0 = __this->___moveThreshold_15;
		return L_0;
	}
}
// System.Void DynamicJoystick::set_MoveThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9 (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		float L_0 = ___value0;
		float L_1;
		L_1 = fabsf(L_0);
		__this->___moveThreshold_15 = L_1;
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		return;
	}
}
// System.Void DynamicJoystick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_Start_mFE16C6CE0B753F08E79A2AEC75782DEEF3B96F72 (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, const RuntimeMethod* method) 
{
	{
		// MoveThreshold = moveThreshold;
		float L_0 = __this->___moveThreshold_15;
		DynamicJoystick_set_MoveThreshold_m000C1AE325C0B9C33172E4202F2AFB59820517F9(__this, L_0, NULL);
		// base.Start();
		Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C(__this, NULL);
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_1 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_OnPointerDown_mBFA3026A0DA4A6B53C0E747A1E892CBC7F43E136 (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_1 = ___eventData0;
		NullCheck(L_1);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline(L_1, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024(__this, L_2, NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_0, L_3, NULL);
		// background.gameObject.SetActive(true);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_4 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// base.OnPointerDown(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_6 = ___eventData0;
		Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6(__this, L_6, NULL);
		// }
		return;
	}
}
// System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_OnPointerUp_m10389907A9D3362F6B75FDC5F35AF37A5DD5AE7C (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// base.OnPointerUp(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_2 = ___eventData0;
		Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick_HandleInput_m3F157F4825BE6682228C8E135581C6404D268506 (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, float ___magnitude0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___normalised1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___radius2, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam3, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (magnitude > moveThreshold)
		float L_0 = ___magnitude0;
		float L_1 = __this->___moveThreshold_15;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0035;
		}
	}
	{
		// Vector2 difference = normalised * (magnitude - moveThreshold) * radius;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___normalised1;
		float L_3 = ___magnitude0;
		float L_4 = __this->___moveThreshold_15;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5;
		L_5 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_2, ((float)il2cpp_codegen_subtract(L_3, L_4)), NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___radius2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7;
		L_7 = Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline(L_5, L_6, NULL);
		V_0 = L_7;
		// background.anchoredPosition += difference;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_8 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_9 = L_8;
		NullCheck(L_9);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10;
		L_10 = RectTransform_get_anchoredPosition_m38F25A4253B0905BB058BE73DBF43C7172CE0680(L_9, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		L_12 = Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline(L_10, L_11, NULL);
		NullCheck(L_9);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_9, L_12, NULL);
	}

IL_0035:
	{
		// base.HandleInput(magnitude, normalised, radius, cam);
		float L_13 = ___magnitude0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_14 = ___normalised1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15 = ___radius2;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_16 = ___cam3;
		Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C(__this, L_13, L_14, L_15, L_16, NULL);
		// }
		return;
	}
}
// System.Void DynamicJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicJoystick__ctor_m9DDA6263314BD7B97679DF14A4664358BD3E58CB (DynamicJoystick_t98EE04BCEB0A14E5B15863006BAA5361C2D59E10* __this, const RuntimeMethod* method) 
{
	{
		// [SerializeField] private float moveThreshold = 1;
		__this->___moveThreshold_15 = (1.0f);
		Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FixedJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FixedJoystick__ctor_m8C8BB74E5EA8CA2C3DD2AE084301EC91F519AD24 (FixedJoystick_t7AA7F128A16A375A847AD0C7067993A6CC95DD7F* __this, const RuntimeMethod* method) 
{
	{
		Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FloatingJoystick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatingJoystick_Start_mB22059CD82AF77A8F94AC72E81A8BAE969399E81 (FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81* __this, const RuntimeMethod* method) 
{
	{
		// base.Start();
		Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C(__this, NULL);
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatingJoystick_OnPointerDown_mFE5B4F54D5BBCA72F9729AB64765F558FA5C7A54 (FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_1 = ___eventData0;
		NullCheck(L_1);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline(L_1, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024(__this, L_2, NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_0, L_3, NULL);
		// background.gameObject.SetActive(true);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_4 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		// base.OnPointerDown(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_6 = ___eventData0;
		Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6(__this, L_6, NULL);
		// }
		return;
	}
}
// System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatingJoystick_OnPointerUp_m80ABA9C914E1953F91DBF74853CE84879352280D (FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_0, NULL);
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// base.OnPointerUp(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_2 = ___eventData0;
		Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void FloatingJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatingJoystick__ctor_m6B72425996D46B025F9E9D22121E9D01BEC6BD3C (FloatingJoystick_t78A6A544FB6B2F38883EEC66D9FABF6E481E1A81* __this, const RuntimeMethod* method) 
{
	{
		Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single VariableJoystick::get_MoveThreshold()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VariableJoystick_get_MoveThreshold_m8C9D3A63DB3B6CF1F0139C3504EC2E7AC4E7CF99 (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, const RuntimeMethod* method) 
{
	{
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		float L_0 = __this->___moveThreshold_15;
		return L_0;
	}
}
// System.Void VariableJoystick::set_MoveThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_set_MoveThreshold_m23DC4187B405EB690D297379E2113568B40C705A (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, float ___value0, const RuntimeMethod* method) 
{
	{
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		float L_0 = ___value0;
		float L_1;
		L_1 = fabsf(L_0);
		__this->___moveThreshold_15 = L_1;
		// public float MoveThreshold { get { return moveThreshold; } set { moveThreshold = Mathf.Abs(value); } }
		return;
	}
}
// System.Void VariableJoystick::SetMode(JoystickType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, int32_t ___joystickType0, const RuntimeMethod* method) 
{
	{
		// this.joystickType = joystickType;
		int32_t L_0 = ___joystickType0;
		__this->___joystickType_16 = L_0;
		// if(joystickType == JoystickType.Fixed)
		int32_t L_1 = ___joystickType0;
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		// background.anchoredPosition = fixedPosition;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_2 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = __this->___fixedPosition_17;
		NullCheck(L_2);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_2, L_3, NULL);
		// background.gameObject.SetActive(true);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_4 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_4);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		NullCheck(L_5);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)1, NULL);
		return;
	}

IL_002d:
	{
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_6 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_6);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_6, NULL);
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void VariableJoystick::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_Start_m21743760641EA0317ACCD95949B99825446FE74D (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, const RuntimeMethod* method) 
{
	{
		// base.Start();
		Joystick_Start_m5E46F090910AB69BE9838BFDB91A4F6E6934480C(__this, NULL);
		// fixedPosition = background.anchoredPosition;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_0);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1;
		L_1 = RectTransform_get_anchoredPosition_m38F25A4253B0905BB058BE73DBF43C7172CE0680(L_0, NULL);
		__this->___fixedPosition_17 = L_1;
		// SetMode(joystickType);
		int32_t L_2 = __this->___joystickType_16;
		VariableJoystick_SetMode_mB9D0B9B6E2E4F431E36AED6F5AC989305ADDB1EE(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_OnPointerDown_m8ABE5C8EFBC8DB3A2EE135FFF3C0F67C533AF4B5 (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// if(joystickType != JoystickType.Fixed)
		int32_t L_0 = __this->___joystickType_16;
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		// background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_1 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_2 = ___eventData0;
		NullCheck(L_2);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3;
		L_3 = PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline(L_2, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		L_4 = Joystick_ScreenPointToAnchoredPosition_mC1EB7560E844BF682674E4E7BD640604BC12B024(__this, L_3, NULL);
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_1, L_4, NULL);
		// background.gameObject.SetActive(true);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_5 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_5, NULL);
		NullCheck(L_6);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)1, NULL);
	}

IL_0030:
	{
		// base.OnPointerDown(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_7 = ___eventData0;
		Joystick_OnPointerDown_mF176903D532D9129C90BBBD00FD7714BA3A0D8E6(__this, L_7, NULL);
		// }
		return;
	}
}
// System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_OnPointerUp_m65296D82A6C2E1BDC2D622B9C922FAE8E4544526 (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* ___eventData0, const RuntimeMethod* method) 
{
	{
		// if(joystickType != JoystickType.Fixed)
		int32_t L_0 = __this->___joystickType_16;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// background.gameObject.SetActive(false);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_1 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		NullCheck(L_1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_1, NULL);
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
	}

IL_0019:
	{
		// base.OnPointerUp(eventData);
		PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* L_3 = ___eventData0;
		Joystick_OnPointerUp_mEDED4DA77D954CBAC11CF82B57AB7A4DBFCDE22C(__this, L_3, NULL);
		// }
		return;
	}
}
// System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick_HandleInput_mD1BCF9A068737A9C057EE8CEB7E6DEB682CC03AB (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, float ___magnitude0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___normalised1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___radius2, Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ___cam3, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (joystickType == JoystickType.Dynamic && magnitude > moveThreshold)
		int32_t L_0 = __this->___joystickType_16;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}
	{
		float L_1 = ___magnitude0;
		float L_2 = __this->___moveThreshold_15;
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_003e;
		}
	}
	{
		// Vector2 difference = normalised * (magnitude - moveThreshold) * radius;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___normalised1;
		float L_4 = ___magnitude0;
		float L_5 = __this->___moveThreshold_15;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		L_6 = Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline(L_3, ((float)il2cpp_codegen_subtract(L_4, L_5)), NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = ___radius2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		L_8 = Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline(L_6, L_7, NULL);
		V_0 = L_8;
		// background.anchoredPosition += difference;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_9 = ((Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A*)__this)->___background_9;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_10 = L_9;
		NullCheck(L_10);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11;
		L_11 = RectTransform_get_anchoredPosition_m38F25A4253B0905BB058BE73DBF43C7172CE0680(L_10, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13;
		L_13 = Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline(L_11, L_12, NULL);
		NullCheck(L_10);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_10, L_13, NULL);
	}

IL_003e:
	{
		// base.HandleInput(magnitude, normalised, radius, cam);
		float L_14 = ___magnitude0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15 = ___normalised1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16 = ___radius2;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_17 = ___cam3;
		Joystick_HandleInput_m15A4E86369A1AF0A4A5727DEC0FD93212A99901C(__this, L_14, L_15, L_16, L_17, NULL);
		// }
		return;
	}
}
// System.Void VariableJoystick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VariableJoystick__ctor_m6C7B41973BE20A94F16DB5DCC9AA805C5D8DF852 (VariableJoystick_t95B6EAEAADB3D548017BF6A0EB1C6456C9FD30A6* __this, const RuntimeMethod* method) 
{
	{
		// [SerializeField] private float moveThreshold = 1;
		__this->___moveThreshold_15 = (1.0f);
		// private Vector2 fixedPosition = Vector2.zero;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0;
		L_0 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		__this->___fixedPosition_17 = L_0;
		Joystick__ctor_m9BBE494CA4714F24227F33CB54C10B4DA78BF06B(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DemoFree::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoFree_Start_mBF4E573159D1922BE6935057FBAA2431CA143555 (DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectsOfType_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m70A4338B4AA009BCDF8C81C96967144DC94383FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_animators = FindObjectsOfType<Animator>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* L_0;
		L_0 = Object_FindObjectsOfType_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m70A4338B4AA009BCDF8C81C96967144DC94383FD(Object_FindObjectsOfType_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_m70A4338B4AA009BCDF8C81C96967144DC94383FD_RuntimeMethod_var);
		__this->___m_animators_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_animators_5), (void*)L_0);
		// }
		return;
	}
}
// System.Void DemoFree::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoFree_Update_mB7202DC0B8016DB4563824A605990F8E153C1455 (DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC* __this, const RuntimeMethod* method) 
{
	{
		// if (Input.GetKeyDown(KeyCode.Q))
		bool L_0;
		L_0 = Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3(((int32_t)113), NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// m_cameraLogic.PreviousTarget();
		FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* L_1 = __this->___m_cameraLogic_6;
		NullCheck(L_1);
		FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77(L_1, NULL);
	}

IL_0014:
	{
		// if (Input.GetKeyDown(KeyCode.E))
		bool L_2;
		L_2 = Input_GetKeyDown_m0D59B7EBC3A782C9FBBF107FBCD4B72B38D993B3(((int32_t)101), NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		// m_cameraLogic.NextTarget();
		FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* L_3 = __this->___m_cameraLogic_6;
		NullCheck(L_3);
		FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D(L_3, NULL);
	}

IL_0028:
	{
		// }
		return;
	}
}
// System.Void DemoFree::OnGUI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoFree_OnGUI_mC1CA7B562FB8504C3385EA9316A428CA5EE60933 (DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2D052436E75E077A15CF127A860870F6B99DFB7D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6141244FEB8CC92D7C3D8EC0BC1EABD9FA4BB959);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral61576FC535CE67A4234F725315CB796CEC925011);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5FEB5D60EDA237BAB0D4BA09C512761205128CA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF8A646A0F5BE4AC603D518083703FBC3E2AF75F2);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// GUILayout.BeginVertical(GUILayout.Width(Screen.width));
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_0 = (GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2*)(GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2*)SZArrayNew(GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_1 = L_0;
		int32_t L_2;
		L_2 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14* L_3;
		L_3 = GUILayout_Width_m3CD0F9B520A1B7BF065D30844E2F9965277E1DAA(((float)L_2), NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14*)L_3);
		GUILayout_BeginVertical_mEA80BF63631637EDAAD761D32BAFC49A404F0842(L_1, NULL);
		// GUILayout.BeginHorizontal();
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_4;
		L_4 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_4, NULL);
		// if (GUILayout.Button("Previous character (Q)"))
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_5;
		L_5 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		bool L_6;
		L_6 = GUILayout_Button_m6D4E3D32A001EF42DB5C2052B4C19AB3B518566C(_stringLiteral6141244FEB8CC92D7C3D8EC0BC1EABD9FA4BB959, L_5, NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		// m_cameraLogic.PreviousTarget();
		FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* L_7 = __this->___m_cameraLogic_6;
		NullCheck(L_7);
		FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77(L_7, NULL);
	}

IL_003f:
	{
		// if (GUILayout.Button("Next character (E)"))
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_8;
		L_8 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		bool L_9;
		L_9 = GUILayout_Button_m6D4E3D32A001EF42DB5C2052B4C19AB3B518566C(_stringLiteralF8A646A0F5BE4AC603D518083703FBC3E2AF75F2, L_8, NULL);
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		// m_cameraLogic.NextTarget();
		FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* L_10 = __this->___m_cameraLogic_6;
		NullCheck(L_10);
		FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D(L_10, NULL);
	}

IL_005b:
	{
		// GUILayout.EndHorizontal();
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		// GUILayout.Space(16);
		GUILayout_Space_m9991854C4545EA58CAD8F949BF1FC1F89CF119FE((16.0f), NULL);
		// for (int i = 0; i < m_animations.Length; i++)
		V_0 = 0;
		goto IL_00eb;
	}

IL_006e:
	{
		// if (i == 0) { GUILayout.BeginHorizontal(); }
		int32_t L_11 = V_0;
		if (L_11)
		{
			goto IL_007b;
		}
	}
	{
		// if (i == 0) { GUILayout.BeginHorizontal(); }
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_12;
		L_12 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_12, NULL);
	}

IL_007b:
	{
		// if (GUILayout.Button(m_animations[i]))
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = __this->___m_animations_4;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		String_t* L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_17;
		L_17 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		bool L_18;
		L_18 = GUILayout_Button_m6D4E3D32A001EF42DB5C2052B4C19AB3B518566C(L_16, L_17, NULL);
		if (!L_18)
		{
			goto IL_00b7;
		}
	}
	{
		// for (int j = 0; j < m_animators.Length; j++)
		V_1 = 0;
		goto IL_00ac;
	}

IL_0093:
	{
		// m_animators[j].SetTrigger(m_animations[i]);
		AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* L_19 = __this->___m_animators_5;
		int32_t L_20 = V_1;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = __this->___m_animations_4;
		int32_t L_24 = V_0;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		String_t* L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_22);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_22, L_26, NULL);
		// for (int j = 0; j < m_animators.Length; j++)
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_27, 1));
	}

IL_00ac:
	{
		// for (int j = 0; j < m_animators.Length; j++)
		int32_t L_28 = V_1;
		AnimatorU5BU5D_tC96418EE48F8DA017567CAE0235488C27640E759* L_29 = __this->___m_animators_5;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)((int32_t)(((RuntimeArray*)L_29)->max_length)))))
		{
			goto IL_0093;
		}
	}

IL_00b7:
	{
		// if (i == m_animations.Length - 1) { GUILayout.EndHorizontal(); }
		int32_t L_30 = V_0;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = __this->___m_animations_4;
		NullCheck(L_31);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_31)->max_length)), 1))))))
		{
			goto IL_00cb;
		}
	}
	{
		// if (i == m_animations.Length - 1) { GUILayout.EndHorizontal(); }
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		goto IL_00e7;
	}

IL_00cb:
	{
		// else if (i == (m_animations.Length / 2)) { GUILayout.EndHorizontal(); GUILayout.BeginHorizontal(); }
		int32_t L_32 = V_0;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_33 = __this->___m_animations_4;
		NullCheck(L_33);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)(((int32_t)(((RuntimeArray*)L_33)->max_length))/2))))))
		{
			goto IL_00e7;
		}
	}
	{
		// else if (i == (m_animations.Length / 2)) { GUILayout.EndHorizontal(); GUILayout.BeginHorizontal(); }
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		// else if (i == (m_animations.Length / 2)) { GUILayout.EndHorizontal(); GUILayout.BeginHorizontal(); }
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_34;
		L_34 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_34, NULL);
	}

IL_00e7:
	{
		// for (int i = 0; i < m_animations.Length; i++)
		int32_t L_35 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_35, 1));
	}

IL_00eb:
	{
		// for (int i = 0; i < m_animations.Length; i++)
		int32_t L_36 = V_0;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_37 = __this->___m_animations_4;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)((int32_t)(((RuntimeArray*)L_37)->max_length)))))
		{
			goto IL_006e;
		}
	}
	{
		// GUILayout.Space(16);
		GUILayout_Space_m9991854C4545EA58CAD8F949BF1FC1F89CF119FE((16.0f), NULL);
		// Color oldColor = GUI.color;
		il2cpp_codegen_runtime_class_init_inline(GUI_tA9CDB3D69DB13D51AD83ABDB587EF95947EC2D2A_il2cpp_TypeInfo_var);
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_38;
		L_38 = GUI_get_color_m641A7661D421929DB60FD1AC40E43F960CEC81C1(NULL);
		// GUI.color = Color.black;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_39;
		L_39 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
		GUI_set_color_mBB4E17B3600770E2EEEA61AA956D2207EAF112C7(L_39, NULL);
		// GUILayout.BeginHorizontal();
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_40;
		L_40 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_40, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.Label("WASD or arrows: Move");
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_41;
		L_41 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_Label_mCB93C0DE81ECE87DE34C8B959C5885E9B6E4FEBA(_stringLiteral61576FC535CE67A4234F725315CB796CEC925011, L_41, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.EndHorizontal();
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		// GUILayout.BeginHorizontal();
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_42;
		L_42 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_42, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.Label("Left Shift: Walk");
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_43;
		L_43 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_Label_mCB93C0DE81ECE87DE34C8B959C5885E9B6E4FEBA(_stringLiteral2D052436E75E077A15CF127A860870F6B99DFB7D, L_43, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.EndHorizontal();
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		// GUILayout.BeginHorizontal();
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_44;
		L_44 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_BeginHorizontal_mB753A68BD1357463FEA9F7273FED94085A79BA37(L_44, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.Label("Space: Jump");
		GUILayoutOptionU5BU5D_t24AB80AB9355D784F2C65E12A4D0CC2E0C914CA2* L_45;
		L_45 = Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_inline(Array_Empty_TisGUILayoutOption_t8B0AA056521747053A3176FCC43E9C3608940A14_m6FDA82C3DA1AB43D1DABFC8B9C8E827950925220_RuntimeMethod_var);
		GUILayout_Label_mCB93C0DE81ECE87DE34C8B959C5885E9B6E4FEBA(_stringLiteralD5FEB5D60EDA237BAB0D4BA09C512761205128CA, L_45, NULL);
		// GUILayout.FlexibleSpace();
		GUILayout_FlexibleSpace_m034CCB8A75D407325DB670958160C057FC7A5EAE(NULL);
		// GUILayout.EndHorizontal();
		GUILayout_EndHorizontal_mCB61DC8BE8359CAB9911BDD4F2AB6819AB75DBEC(NULL);
		// GUI.color = oldColor;
		GUI_set_color_mBB4E17B3600770E2EEEA61AA956D2207EAF112C7(L_38, NULL);
		// GUILayout.EndVertical();
		GUILayout_EndVertical_mF2C806265D9B04E715EC1656FA9392332C59EEBC(NULL);
		// }
		return;
	}
}
// System.Void DemoFree::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoFree__ctor_mF73EF24C19F0C17A01A23C3021D3A409348CD734 (DemoFree_tC8ADC7B4F43847D13D26748104F3E14278CB8BAC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral024C60BC25F851E8B9F5531CC461C68CD60D3F30);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9EE0F8466DB982097F181BF01C645F3FC0C20D63);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private readonly string[] m_animations = { "Pickup", "Wave" };
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral9EE0F8466DB982097F181BF01C645F3FC0C20D63);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral9EE0F8466DB982097F181BF01C645F3FC0C20D63);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral024C60BC25F851E8B9F5531CC461C68CD60D3F30);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral024C60BC25F851E8B9F5531CC461C68CD60D3F30);
		__this->___m_animations_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_animations_4), (void*)L_2);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FreeCameraLogic::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_Start_mAEFB06A72DABA63B02A26D04B6B59F1739D0CBD2 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_targets.Count > 0)
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_0 = __this->___m_targets_8;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline(L_0, List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		// m_currentIndex = 0;
		__this->___m_currentIndex_9 = 0;
		// m_currentTarget = m_targets[m_currentIndex];
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_2 = __this->___m_targets_8;
		int32_t L_3 = __this->___m_currentIndex_9;
		NullCheck(L_2);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA(L_2, L_3, List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA_RuntimeMethod_var);
		__this->___m_currentTarget_4 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_currentTarget_4), (void*)L_4);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void FreeCameraLogic::SwitchTarget(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, int32_t ___step0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_targets.Count == 0) { return; }
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_0 = __this->___m_targets_8;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline(L_0, List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if (m_targets.Count == 0) { return; }
		return;
	}

IL_000e:
	{
		// m_currentIndex += step;
		int32_t L_2 = __this->___m_currentIndex_9;
		int32_t L_3 = ___step0;
		__this->___m_currentIndex_9 = ((int32_t)il2cpp_codegen_add(L_2, L_3));
		// if (m_currentIndex > m_targets.Count - 1) { m_currentIndex = 0; }
		int32_t L_4 = __this->___m_currentIndex_9;
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_5 = __this->___m_targets_8;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline(L_5, List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)il2cpp_codegen_subtract(L_6, 1)))))
		{
			goto IL_0038;
		}
	}
	{
		// if (m_currentIndex > m_targets.Count - 1) { m_currentIndex = 0; }
		__this->___m_currentIndex_9 = 0;
	}

IL_0038:
	{
		// if (m_currentIndex < 0) { m_currentIndex = m_targets.Count - 1; }
		int32_t L_7 = __this->___m_currentIndex_9;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		// if (m_currentIndex < 0) { m_currentIndex = m_targets.Count - 1; }
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_8 = __this->___m_targets_8;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline(L_8, List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		__this->___m_currentIndex_9 = ((int32_t)il2cpp_codegen_subtract(L_9, 1));
	}

IL_0054:
	{
		// m_currentTarget = m_targets[m_currentIndex];
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_10 = __this->___m_targets_8;
		int32_t L_11 = __this->___m_currentIndex_9;
		NullCheck(L_10);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_12;
		L_12 = List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA(L_10, L_11, List_1_get_Item_m8EAA91B4CE37CBB6C720FD238E4505097B29FFDA_RuntimeMethod_var);
		__this->___m_currentTarget_4 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_currentTarget_4), (void*)L_12);
		// }
		return;
	}
}
// System.Void FreeCameraLogic::NextTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_NextTarget_m0DE90234A9600E7679D39CD646BFD054BF88B10D (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	{
		// public void NextTarget() { SwitchTarget(1); }
		FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0(__this, 1, NULL);
		// public void NextTarget() { SwitchTarget(1); }
		return;
	}
}
// System.Void FreeCameraLogic::PreviousTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_PreviousTarget_mDC0F96CCECE8F41D441AE028FA8A5E71FC1B5E77 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	{
		// public void PreviousTarget() { SwitchTarget(-1); }
		FreeCameraLogic_SwitchTarget_m2748E87C4B575E1F43C6DA55EEA92E5462D553B0(__this, (-1), NULL);
		// public void PreviousTarget() { SwitchTarget(-1); }
		return;
	}
}
// System.Void FreeCameraLogic::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_Update_m6CF3D1604A8FAC90ECFC85B3C893CAAD54C29A0A (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_targets.Count == 0) { return; }
		List_1_t991BBC5A1D51F59A450367DF944DAA207F22D06D* L_0 = __this->___m_targets_8;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_inline(L_0, List_1_get_Count_mB5E64608D47703A98476E026480AE38671047C87_RuntimeMethod_var);
		// if (m_targets.Count == 0) { return; }
		return;
	}
}
// System.Void FreeCameraLogic::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic_LateUpdate_m9C2597C9E05C7E13E677EF3BAC1E9CD500B4928C (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// if (m_currentTarget == null) { return; }
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___m_currentTarget_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_currentTarget == null) { return; }
		return;
	}

IL_000f:
	{
		// float targetHeight = m_currentTarget.position.y + m_height;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2 = __this->___m_currentTarget_4;
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		float L_4 = L_3.___y_3;
		float L_5 = __this->___m_height_6;
		V_0 = ((float)il2cpp_codegen_add(L_4, L_5));
		// float currentRotationAngle = m_lookAtAroundAngle;
		float L_6 = __this->___m_lookAtAroundAngle_7;
		V_1 = L_6;
		// Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
		float L_7 = V_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_8;
		L_8 = Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline((0.0f), L_7, (0.0f), NULL);
		V_2 = L_8;
		// Vector3 position = m_currentTarget.position;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9 = __this->___m_currentTarget_4;
		NullCheck(L_9);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_9, NULL);
		V_3 = L_10;
		// position -= currentRotation * Vector3.forward * m_distance;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = V_3;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12 = V_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector3_get_forward_mEBAB24D77FC02FC88ED880738C3B1D47C758B3EB_inline(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		L_14 = Quaternion_op_Multiply_mF1348668A6CCD46FBFF98D39182F89358ED74AC0(L_12, L_13, NULL);
		float L_15 = __this->___m_distance_5;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_16;
		L_16 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_14, L_15, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17;
		L_17 = Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline(L_11, L_16, NULL);
		V_3 = L_17;
		// position.y = targetHeight;
		float L_18 = V_0;
		(&V_3)->___y_3 = L_18;
		// transform.position = position;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_19;
		L_19 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20 = V_3;
		NullCheck(L_19);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_19, L_20, NULL);
		// transform.LookAt(m_currentTarget.position + new Vector3(0, m_height, 0));
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_21;
		L_21 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22 = __this->___m_currentTarget_4;
		NullCheck(L_22);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_22, NULL);
		float L_24 = __this->___m_height_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_25), (0.0f), L_24, (0.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_23, L_25, NULL);
		NullCheck(L_21);
		Transform_LookAt_mFEF7353E4CAEB85D5F7CEEF9276C3B8D6E314C6C(L_21, L_26, NULL);
		// }
		return;
	}
}
// System.Void FreeCameraLogic::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FreeCameraLogic__ctor_m6898614914A5493AC854E75698C66A94D706ACF3 (FreeCameraLogic_t933A1F5D3D9A4CE572D2975E722453D484D2E98D* __this, const RuntimeMethod* method) 
{
	{
		// private float m_distance = 2f;
		__this->___m_distance_5 = (2.0f);
		// private float m_height = 1;
		__this->___m_height_6 = (1.0f);
		// private float m_lookAtAroundAngle = 180;
		__this->___m_lookAtAroundAngle_7 = (180.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleSampleCharacterControl::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_Awake_mD46E7A1ACA21666FFA49FBE92A43D056813737E8 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!m_animator) { gameObject.GetComponent<Animator>(); }
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___m_animator_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_0, NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		// if (!m_animator) { gameObject.GetComponent<Animator>(); }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_3;
		L_3 = GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3(L_2, GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3_RuntimeMethod_var);
	}

IL_0019:
	{
		// if (!m_rigidBody) { gameObject.GetComponent<Animator>(); }
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_4 = __this->___m_rigidBody_8;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_4, NULL);
		if (L_5)
		{
			goto IL_0032;
		}
	}
	{
		// if (!m_rigidBody) { gameObject.GetComponent<Animator>(); }
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_6);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_7;
		L_7 = GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3(L_6, GameObject_GetComponent_TisAnimator_t8A52E42AE54F76681838FE9E632683EF3952E883_mB84A0931B2081CCADE7C5D459B2A8FAA6D3D3BD3_RuntimeMethod_var);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::OnCollisionEnter(UnityEngine.Collision)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_OnCollisionEnter_m9D9D4CB0CA4992269C200C11BADBF77AABA4C8EB (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* ___collision0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// ContactPoint[] contactPoints = collision.contacts;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_0 = ___collision0;
		NullCheck(L_0);
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_1;
		L_1 = Collision_get_contacts_m2E8E27E0399230DFA4303A4F4D81C1BD55CBC473(L_0, NULL);
		V_0 = L_1;
		// for (int i = 0; i < contactPoints.Length; i++)
		V_1 = 0;
		goto IL_0057;
	}

IL_000b:
	{
		// if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = ContactPoint_get_normal_mD7F0567CA2FD68644F7C6FE318E10C4D15F92AD6(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline(NULL);
		float L_6;
		L_6 = Vector3_Dot_m4688A1A524306675DBDB1E6D483F35E85E3CE6D8_inline(L_4, L_5, NULL);
		if ((!(((float)L_6) > ((float)(0.5f)))))
		{
			goto IL_0053;
		}
	}
	{
		// if (!m_collisions.Contains(collision.collider))
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_7 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_8 = ___collision0;
		NullCheck(L_8);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_9;
		L_9 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_8, NULL);
		NullCheck(L_7);
		bool L_10;
		L_10 = List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40(L_7, L_9, List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_004c;
		}
	}
	{
		// m_collisions.Add(collision.collider);
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_11 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_12 = ___collision0;
		NullCheck(L_12);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_13;
		L_13 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_12, NULL);
		NullCheck(L_11);
		List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_inline(L_11, L_13, List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_RuntimeMethod_var);
	}

IL_004c:
	{
		// m_isGrounded = true;
		__this->___m_isGrounded_21 = (bool)1;
	}

IL_0053:
	{
		// for (int i = 0; i < contactPoints.Length; i++)
		int32_t L_14 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_0057:
	{
		// for (int i = 0; i < contactPoints.Length; i++)
		int32_t L_15 = V_1;
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_16 = V_0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::OnCollisionStay(UnityEngine.Collision)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_OnCollisionStay_mD367B960863C0010FCF2C481E76D579E7959E49E (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* ___collision0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	{
		// ContactPoint[] contactPoints = collision.contacts;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_0 = ___collision0;
		NullCheck(L_0);
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_1;
		L_1 = Collision_get_contacts_m2E8E27E0399230DFA4303A4F4D81C1BD55CBC473(L_0, NULL);
		V_0 = L_1;
		// bool validSurfaceNormal = false;
		V_1 = (bool)0;
		// for (int i = 0; i < contactPoints.Length; i++)
		V_2 = 0;
		goto IL_0032;
	}

IL_000d:
	{
		// if (Vector3.Dot(contactPoints[i].normal, Vector3.up) > 0.5f)
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_2 = V_0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = ContactPoint_get_normal_mD7F0567CA2FD68644F7C6FE318E10C4D15F92AD6(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline(NULL);
		float L_6;
		L_6 = Vector3_Dot_m4688A1A524306675DBDB1E6D483F35E85E3CE6D8_inline(L_4, L_5, NULL);
		if ((!(((float)L_6) > ((float)(0.5f)))))
		{
			goto IL_002e;
		}
	}
	{
		// validSurfaceNormal = true; break;
		V_1 = (bool)1;
		// validSurfaceNormal = true; break;
		goto IL_0038;
	}

IL_002e:
	{
		// for (int i = 0; i < contactPoints.Length; i++)
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_7, 1));
	}

IL_0032:
	{
		// for (int i = 0; i < contactPoints.Length; i++)
		int32_t L_8 = V_2;
		ContactPointU5BU5D_t3570603E8D0685B71B3D8BA07031674B00C5E411* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))
		{
			goto IL_000d;
		}
	}

IL_0038:
	{
		// if (validSurfaceNormal)
		bool L_10 = V_1;
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		// m_isGrounded = true;
		__this->___m_isGrounded_21 = (bool)1;
		// if (!m_collisions.Contains(collision.collider))
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_11 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_12 = ___collision0;
		NullCheck(L_12);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_13;
		L_13 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_12, NULL);
		NullCheck(L_11);
		bool L_14;
		L_14 = List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40(L_11, L_13, List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		// m_collisions.Add(collision.collider);
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_15 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_16 = ___collision0;
		NullCheck(L_16);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_17;
		L_17 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_16, NULL);
		NullCheck(L_15);
		List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_inline(L_15, L_17, List_1_Add_m67ADCB698F31486B35CF5DB4CFB1E97EB807FEFD_RuntimeMethod_var);
		return;
	}

IL_0067:
	{
		// if (m_collisions.Contains(collision.collider))
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_18 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_19 = ___collision0;
		NullCheck(L_19);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_20;
		L_20 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_19, NULL);
		NullCheck(L_18);
		bool L_21;
		L_21 = List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40(L_18, L_20, List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		// m_collisions.Remove(collision.collider);
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_22 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_23 = ___collision0;
		NullCheck(L_23);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_24;
		L_24 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_23, NULL);
		NullCheck(L_22);
		bool L_25;
		L_25 = List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC(L_22, L_24, List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC_RuntimeMethod_var);
	}

IL_008c:
	{
		// if (m_collisions.Count == 0) { m_isGrounded = false; }
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_26 = __this->___m_collisions_22;
		NullCheck(L_26);
		int32_t L_27;
		L_27 = List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_inline(L_26, List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_RuntimeMethod_var);
		if (L_27)
		{
			goto IL_00a0;
		}
	}
	{
		// if (m_collisions.Count == 0) { m_isGrounded = false; }
		__this->___m_isGrounded_21 = (bool)0;
	}

IL_00a0:
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::OnCollisionExit(UnityEngine.Collision)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_OnCollisionExit_m46D7433CAAD5DCD3D54A465C56D9C81BCBCFAAE4 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* ___collision0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_collisions.Contains(collision.collider))
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_0 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_1 = ___collision0;
		NullCheck(L_1);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_2;
		L_2 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_1, NULL);
		NullCheck(L_0);
		bool L_3;
		L_3 = List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40(L_0, L_2, List_1_Contains_m1F3CEA5349E6590255D80D8F1483E5B155E36D40_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		// m_collisions.Remove(collision.collider);
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_4 = __this->___m_collisions_22;
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_5 = ___collision0;
		NullCheck(L_5);
		Collider_t1CC3163924FCD6C4CC2E816373A929C1E3D55E76* L_6;
		L_6 = Collision_get_collider_mBB5A086C78FE4BE0589E216F899B611673ADD25D(L_5, NULL);
		NullCheck(L_4);
		bool L_7;
		L_7 = List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC(L_4, L_6, List_1_Remove_m033842E17A41459D42A3E0F26F50C4541AE57BFC_RuntimeMethod_var);
	}

IL_0025:
	{
		// if (m_collisions.Count == 0) { m_isGrounded = false; }
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_8 = __this->___m_collisions_22;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_inline(L_8, List_1_get_Count_m8D408B57AE7C2D791BB61817BBC93B84537AB617_RuntimeMethod_var);
		if (L_9)
		{
			goto IL_0039;
		}
	}
	{
		// if (m_collisions.Count == 0) { m_isGrounded = false; }
		__this->___m_isGrounded_21 = (bool)0;
	}

IL_0039:
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_Update_mA0057186C94EACB27E3E5FB5853B7BD125770FF1 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	{
		// if (!m_jumpInput && Input.GetKey(KeyCode.Space))
		bool L_0 = __this->___m_jumpInput_20;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1;
		L_1 = Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A(((int32_t)32), NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// m_jumpInput = true;
		__this->___m_jumpInput_20 = (bool)1;
	}

IL_0018:
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_FixedUpdate_m95FB469558AACEAF9B3383935A6E2C1D98E1D494 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5F23E0C606C3923E7D0C5C95679CF58C71B45C12);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// m_animator.SetBool("Grounded", m_isGrounded);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->___m_animator_7;
		bool L_1 = __this->___m_isGrounded_21;
		NullCheck(L_0);
		Animator_SetBool_m6F8D4FAF0770CD4EC1F54406249785DE7391E42B(L_0, _stringLiteralEB1018EBBD330B231ADCF3E0D809C0C4A7F770D4, L_1, NULL);
		// switch (m_controlMode)
		int32_t L_2 = __this->___m_controlMode_9;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0034;
		}
	}
	{
		// DirectUpdate();
		SimpleSampleCharacterControl_DirectUpdate_m107B99848528F674872DB7A277D33514A3863D24(__this, NULL);
		// break;
		goto IL_003e;
	}

IL_002c:
	{
		// TankUpdate();
		SimpleSampleCharacterControl_TankUpdate_m3D7D92631D2653E40AD1EADDFD46395C099B2568(__this, NULL);
		// break;
		goto IL_003e;
	}

IL_0034:
	{
		// Debug.LogError("Unsupported state");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_m059825802BB6AF7EA9693FEBEEB0D85F59A3E38E(_stringLiteral5F23E0C606C3923E7D0C5C95679CF58C71B45C12, NULL);
	}

IL_003e:
	{
		// m_wasGrounded = m_isGrounded;
		bool L_5 = __this->___m_isGrounded_21;
		__this->___m_wasGrounded_16 = L_5;
		// m_jumpInput = false;
		__this->___m_jumpInput_20 = (bool)0;
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::TankUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_TankUpdate_m3D7D92631D2653E40AD1EADDFD46395C099B2568 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDB546722AF594A8BDD41950B5C210E49FB115982);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		// float v = Input.GetAxis("Vertical");
		float L_0;
		L_0 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, NULL);
		V_0 = L_0;
		// float h = Input.GetAxis("Horizontal");
		float L_1;
		L_1 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, NULL);
		V_1 = L_1;
		// bool walk = Input.GetKey(KeyCode.LeftShift);
		bool L_2;
		L_2 = Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A(((int32_t)304), NULL);
		V_2 = L_2;
		// if (v < 0)
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0042;
		}
	}
	{
		// if (walk) { v *= m_backwardsWalkScale; }
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		// if (walk) { v *= m_backwardsWalkScale; }
		float L_5 = V_0;
		float L_6 = __this->___m_backwardsWalkScale_14;
		V_0 = ((float)il2cpp_codegen_multiply(L_5, L_6));
		goto IL_004e;
	}

IL_0037:
	{
		// else { v *= m_backwardRunScale; }
		float L_7 = V_0;
		float L_8 = __this->___m_backwardRunScale_15;
		V_0 = ((float)il2cpp_codegen_multiply(L_7, L_8));
		goto IL_004e;
	}

IL_0042:
	{
		// else if (walk)
		bool L_9 = V_2;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		// v *= m_walkScale;
		float L_10 = V_0;
		float L_11 = __this->___m_walkScale_13;
		V_0 = ((float)il2cpp_codegen_multiply(L_10, L_11));
	}

IL_004e:
	{
		// m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
		float L_12 = __this->___m_currentV_10;
		float L_13 = V_0;
		float L_14;
		L_14 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_15 = __this->___m_interpolation_12;
		float L_16;
		L_16 = Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline(L_12, L_13, ((float)il2cpp_codegen_multiply(L_14, L_15)), NULL);
		__this->___m_currentV_10 = L_16;
		// m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);
		float L_17 = __this->___m_currentH_11;
		float L_18 = V_1;
		float L_19;
		L_19 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_20 = __this->___m_interpolation_12;
		float L_21;
		L_21 = Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline(L_17, L_18, ((float)il2cpp_codegen_multiply(L_19, L_20)), NULL);
		__this->___m_currentH_11 = L_21;
		// transform.position += transform.forward * m_currentV * m_moveSpeed * Time.deltaTime;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22;
		L_22 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_23 = L_22;
		NullCheck(L_23);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		L_24 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_23, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_25;
		L_25 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_25);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_25, NULL);
		float L_27 = __this->___m_currentV_10;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28;
		L_28 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_26, L_27, NULL);
		float L_29 = __this->___m_moveSpeed_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
		L_30 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_28, L_29, NULL);
		float L_31;
		L_31 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_30, L_31, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_24, L_32, NULL);
		NullCheck(L_23);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_23, L_33, NULL);
		// transform.Rotate(0, m_currentH * m_turnSpeed * Time.deltaTime, 0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_34;
		L_34 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_35 = __this->___m_currentH_11;
		float L_36 = __this->___m_turnSpeed_5;
		float L_37;
		L_37 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		NullCheck(L_34);
		Transform_Rotate_m7EA47AD57F43D478CCB0523D179950EE49CDA3E2(L_34, (0.0f), ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_35, L_36)), L_37)), (0.0f), NULL);
		// m_animator.SetFloat("MoveSpeed", m_currentV);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_38 = __this->___m_animator_7;
		float L_39 = __this->___m_currentV_10;
		NullCheck(L_38);
		Animator_SetFloat_m10C78733FAFC7AFEDBDACC48B7C66D3A35A0A7FE(L_38, _stringLiteralDB546722AF594A8BDD41950B5C210E49FB115982, L_39, NULL);
		// JumpingAndLanding();
		SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201(__this, NULL);
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::DirectUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_DirectUpdate_m107B99848528F674872DB7A277D33514A3863D24 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDB546722AF594A8BDD41950B5C210E49FB115982);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* V_2 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		// float v = Input.GetAxis("Vertical");
		float L_0;
		L_0 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, NULL);
		V_0 = L_0;
		// float h = Input.GetAxis("Horizontal");
		float L_1;
		L_1 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, NULL);
		V_1 = L_1;
		// Transform camera = Camera.main.transform;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_2;
		L_2 = Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43(NULL);
		NullCheck(L_2);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_2, NULL);
		V_2 = L_3;
		// if (Input.GetKey(KeyCode.LeftShift))
		bool L_4;
		L_4 = Input_GetKey_m0BF0499CADC378F02B6BEE2399FB945AB929B81A(((int32_t)304), NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		// v *= m_walkScale;
		float L_5 = V_0;
		float L_6 = __this->___m_walkScale_13;
		V_0 = ((float)il2cpp_codegen_multiply(L_5, L_6));
		// h *= m_walkScale;
		float L_7 = V_1;
		float L_8 = __this->___m_walkScale_13;
		V_1 = ((float)il2cpp_codegen_multiply(L_7, L_8));
	}

IL_003f:
	{
		// m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
		float L_9 = __this->___m_currentV_10;
		float L_10 = V_0;
		float L_11;
		L_11 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_12 = __this->___m_interpolation_12;
		float L_13;
		L_13 = Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline(L_9, L_10, ((float)il2cpp_codegen_multiply(L_11, L_12)), NULL);
		__this->___m_currentV_10 = L_13;
		// m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);
		float L_14 = __this->___m_currentH_11;
		float L_15 = V_1;
		float L_16;
		L_16 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_17 = __this->___m_interpolation_12;
		float L_18;
		L_18 = Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline(L_14, L_15, ((float)il2cpp_codegen_multiply(L_16, L_17)), NULL);
		__this->___m_currentH_11 = L_18;
		// Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_19 = V_2;
		NullCheck(L_19);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_20;
		L_20 = Transform_get_forward_mFCFACF7165FDAB21E80E384C494DF278386CEE2F(L_19, NULL);
		float L_21 = __this->___m_currentV_10;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_22;
		L_22 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_20, L_21, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_23 = V_2;
		NullCheck(L_23);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24;
		L_24 = Transform_get_right_mC6DC057C23313802E2186A9E0DB760D795A758A4(L_23, NULL);
		float L_25 = __this->___m_currentH_11;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_26;
		L_26 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_24, L_25, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
		L_27 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_22, L_26, NULL);
		V_3 = L_27;
		// float directionLength = direction.magnitude;
		float L_28;
		L_28 = Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline((&V_3), NULL);
		V_4 = L_28;
		// direction.y = 0;
		(&V_3)->___y_3 = (0.0f);
		// direction = direction.normalized * directionLength;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline((&V_3), NULL);
		float L_30 = V_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_31;
		L_31 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_29, L_30, NULL);
		V_3 = L_31;
		// if (direction != Vector3.zero)
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = V_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		bool L_34;
		L_34 = Vector3_op_Inequality_m6A7FB1C9E9DE194708997BFA24C6E238D92D908E_inline(L_32, L_33, NULL);
		if (!L_34)
		{
			goto IL_0150;
		}
	}
	{
		// m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_35 = __this->___m_currentDirection_17;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_36 = V_3;
		float L_37;
		L_37 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		float L_38 = __this->___m_interpolation_12;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_39;
		L_39 = Vector3_Slerp_mBA32C7EAC64C56C7D68480549FA9A892FA5C1728(L_35, L_36, ((float)il2cpp_codegen_multiply(L_37, L_38)), NULL);
		__this->___m_currentDirection_17 = L_39;
		// transform.rotation = Quaternion.LookRotation(m_currentDirection);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_40;
		L_40 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_41 = __this->___m_currentDirection_17;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_42;
		L_42 = Quaternion_LookRotation_m8C0F294E5143F93D378E020EAD9DA2288A5907A3(L_41, NULL);
		NullCheck(L_40);
		Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D(L_40, L_42, NULL);
		// transform.position += m_currentDirection * m_moveSpeed * Time.deltaTime;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_43;
		L_43 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_44 = L_43;
		NullCheck(L_44);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_45;
		L_45 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_44, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_46 = __this->___m_currentDirection_17;
		float L_47 = __this->___m_moveSpeed_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_48;
		L_48 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_46, L_47, NULL);
		float L_49;
		L_49 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_50;
		L_50 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_48, L_49, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_51;
		L_51 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_45, L_50, NULL);
		NullCheck(L_44);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_44, L_51, NULL);
		// m_animator.SetFloat("MoveSpeed", direction.magnitude);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_52 = __this->___m_animator_7;
		float L_53;
		L_53 = Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline((&V_3), NULL);
		NullCheck(L_52);
		Animator_SetFloat_m10C78733FAFC7AFEDBDACC48B7C66D3A35A0A7FE(L_52, _stringLiteralDB546722AF594A8BDD41950B5C210E49FB115982, L_53, NULL);
	}

IL_0150:
	{
		// JumpingAndLanding();
		SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201(__this, NULL);
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::JumpingAndLanding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl_JumpingAndLanding_mE19580C04014DF21450A977993BFBF14A02C4201 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62DCA95296ED08F48DDF8762225138C798CBEA96);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bool jumpCooldownOver = (Time.time - m_jumpTimeStamp) >= m_minJumpInterval;
		float L_0;
		L_0 = Time_get_time_m0BEE9AACD0723FE414465B77C9C64D12263675F3(NULL);
		float L_1 = __this->___m_jumpTimeStamp_18;
		float L_2 = __this->___m_minJumpInterval_19;
		// if (jumpCooldownOver && m_isGrounded && m_jumpInput)
		if (!((((int32_t)((!(((float)((float)il2cpp_codegen_subtract(L_0, L_1))) >= ((float)L_2)))? 1 : 0)) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0050;
		}
	}
	{
		bool L_3 = __this->___m_isGrounded_21;
		if (!L_3)
		{
			goto IL_0050;
		}
	}
	{
		bool L_4 = __this->___m_jumpInput_20;
		if (!L_4)
		{
			goto IL_0050;
		}
	}
	{
		// m_jumpTimeStamp = Time.time;
		float L_5;
		L_5 = Time_get_time_m0BEE9AACD0723FE414465B77C9C64D12263675F3(NULL);
		__this->___m_jumpTimeStamp_18 = L_5;
		// m_rigidBody.AddForce(Vector3.up * m_jumpForce, ForceMode.Impulse);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_6 = __this->___m_rigidBody_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline(NULL);
		float L_8 = __this->___m_jumpForce_6;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_7, L_8, NULL);
		NullCheck(L_6);
		Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC(L_6, L_9, 1, NULL);
	}

IL_0050:
	{
		// if (!m_wasGrounded && m_isGrounded)
		bool L_10 = __this->___m_wasGrounded_16;
		if (L_10)
		{
			goto IL_0070;
		}
	}
	{
		bool L_11 = __this->___m_isGrounded_21;
		if (!L_11)
		{
			goto IL_0070;
		}
	}
	{
		// m_animator.SetTrigger("Land");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_12 = __this->___m_animator_7;
		NullCheck(L_12);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_12, _stringLiteral62DCA95296ED08F48DDF8762225138C798CBEA96, NULL);
	}

IL_0070:
	{
		// if (!m_isGrounded && m_wasGrounded)
		bool L_13 = __this->___m_isGrounded_21;
		if (L_13)
		{
			goto IL_0090;
		}
	}
	{
		bool L_14 = __this->___m_wasGrounded_16;
		if (!L_14)
		{
			goto IL_0090;
		}
	}
	{
		// m_animator.SetTrigger("Jump");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_15 = __this->___m_animator_7;
		NullCheck(L_15);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_15, _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, NULL);
	}

IL_0090:
	{
		// }
		return;
	}
}
// System.Void SimpleSampleCharacterControl::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleSampleCharacterControl__ctor_m4761190D7C96376CB6BD11FC408B321CEA199958 (SimpleSampleCharacterControl_t9C536AB742D123A58FCA7692A542CB9EC89537A3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m0CDD6F02F45026B4267E7117C5DDC188F87EE7BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private float m_moveSpeed = 2;
		__this->___m_moveSpeed_4 = (2.0f);
		// [SerializeField] private float m_turnSpeed = 200;
		__this->___m_turnSpeed_5 = (200.0f);
		// [SerializeField] private float m_jumpForce = 4;
		__this->___m_jumpForce_6 = (4.0f);
		// [SerializeField] private ControlMode m_controlMode = ControlMode.Direct;
		__this->___m_controlMode_9 = 1;
		// private readonly float m_interpolation = 10;
		__this->___m_interpolation_12 = (10.0f);
		// private readonly float m_walkScale = 0.33f;
		__this->___m_walkScale_13 = (0.330000013f);
		// private readonly float m_backwardsWalkScale = 0.16f;
		__this->___m_backwardsWalkScale_14 = (0.159999996f);
		// private readonly float m_backwardRunScale = 0.66f;
		__this->___m_backwardRunScale_15 = (0.660000026f);
		// private Vector3 m_currentDirection = Vector3.zero;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		L_0 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		__this->___m_currentDirection_17 = L_0;
		// private float m_minJumpInterval = 0.25f;
		__this->___m_minJumpInterval_19 = (0.25f);
		// private List<Collider> m_collisions = new List<Collider>();
		List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252* L_1 = (List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252*)il2cpp_codegen_object_new(List_1_t58F89DEDCD7DABB0CFB009AAD9C0CFE061592252_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_m0CDD6F02F45026B4267E7117C5DDC188F87EE7BE(L_1, List_1__ctor_m0CDD6F02F45026B4267E7117C5DDC188F87EE7BE_RuntimeMethod_var);
		__this->___m_collisions_22 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_collisions_22), (void*)L_1);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ground::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ground__ctor_m8594E0C48DFF84D558BEA17A51722E90961ED69A (Ground_t3A6E265B327D38866FB20EEC69AB4FE1D6A0598C* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.BaseEnemy::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseEnemy_Awake_mEE6BBFDBAA3F90B021CC5BBFFBCE2820188D0BD1 (BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisKickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9_m909A71EC628FD746DBAE6837FEC8E63E358A6333_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _hitable = GetComponent<Hitable>();
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_0;
		L_0 = Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707(__this, Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707_RuntimeMethod_var);
		__this->____hitable_9 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____hitable_9), (void*)L_0);
		// _kickable = GetComponent<Kickable>();
		Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_1;
		L_1 = Component_GetComponent_TisKickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9_m909A71EC628FD746DBAE6837FEC8E63E358A6333(__this, Component_GetComponent_TisKickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9_m909A71EC628FD746DBAE6837FEC8E63E358A6333_RuntimeMethod_var);
		__this->____kickable_8 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____kickable_8), (void*)L_1);
		// }
		return;
	}
}
// System.Void KickMaster.BaseEnemy::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseEnemy_OnEnable_m3ADDFF00FCE8C08BD3B4AFA5F2E59D59FAB30245 (BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _kickable.Kicked += Die;
		Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_0 = __this->____kickable_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 4)), NULL);
		NullCheck(L_0);
		Kickable_add_Kicked_m990B81847DFB334DE467B5F1A1E290CB133BA8A8(L_0, L_1, NULL);
		// _hitable.Hited += Die;
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_2 = __this->____hitable_9;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 4)), NULL);
		NullCheck(L_2);
		Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.BaseEnemy::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseEnemy_OnDisable_m696F449BB983D8AE7375EB913288C9C8D46F1B44 (BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _kickable.Kicked -= Die;
		Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_0 = __this->____kickable_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 4)), NULL);
		NullCheck(L_0);
		Kickable_remove_Kicked_mE39AC028E50A7C98DDDA75E64C47C85736FB909E(L_0, L_1, NULL);
		// _hitable.Hited -= Die;
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_2 = __this->____hitable_9;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)GetVirtualMethodInfo(__this, 4)), NULL);
		NullCheck(L_2);
		Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.BaseEnemy::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseEnemy_Die_m299473781EA8B21D60AEE342EDCD3ACB2CDA8787 (BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral08EF1409295A748F17C961491378A3B6215AA838);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator.SetTrigger("Die");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->____animator_7;
		NullCheck(L_0);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_0, _stringLiteral08EF1409295A748F17C961491378A3B6215AA838, NULL);
		// Destroy(gameObject, 2f);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_m0E1B4CF8C29EB7FC8658C2C84C57F49C0DD12C91(L_1, (2.0f), NULL);
		// }
		return;
	}
}
// System.Void KickMaster.BaseEnemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseEnemy__ctor_mC97CA975036B3AE7540028E60285B1775154EF72 (BaseEnemy_tEDE8A632BCED813FECEC5522675C5FC62E072365* __this, const RuntimeMethod* method) 
{
	{
		Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Boss::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_Awake_mCF43015BC755662F6014620782DB1E9B6202ED0B (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _hitable = GetComponent<Hitable>();
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_0;
		L_0 = Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707(__this, Component_GetComponent_TisHitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4_mB1AA6E45D61912D30ED3148B6C1B61F229C3E707_RuntimeMethod_var);
		__this->____hitable_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____hitable_7), (void*)L_0);
		// }
		return;
	}
}
// System.Void KickMaster.Boss::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_OnEnable_mF5E24AE5A7E6C117BE40E289FC6E86E5BDF3AC75 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _hitable.Hited += () => TakeDamage(1);
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_0 = __this->____hitable_7;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Boss::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_OnDisable_mA099BDCFBAA8B4A66F6B4C50EEC0B0F63FC3A2D9 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _hitable.Hited -= () => TakeDamage(1);
		Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* L_0 = __this->____hitable_7;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Boss::TakeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// _health -= value;
		int32_t L_0 = ((Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584*)__this)->____health_5;
		int32_t L_1 = ___value0;
		((Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584*)__this)->____health_5 = ((int32_t)il2cpp_codegen_subtract(L_0, L_1));
		// if (_health <= 0)
		int32_t L_2 = ((Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584*)__this)->____health_5;
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		// Die();
		VirtualActionInvoker0::Invoke(4 /* System.Void KickMaster.Enemy::Die() */, __this);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Boss::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_Die_m7FE73A8FFBA993FA30CBF52D5A7AA84C136BDF12 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject, 2f);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_m0E1B4CF8C29EB7FC8658C2C84C57F49C0DD12C91(L_0, (2.0f), NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Boss::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss__ctor_m0C69A1D5F4C285CAD67E74C06433ED8FF59BE7E9 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	{
		Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD(__this, NULL);
		return;
	}
}
// System.Void KickMaster.Boss::<OnEnable>b__2_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_U3COnEnableU3Eb__2_0_mD99B3D7C259314B02DC861D23918440E5E38A156 (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	{
		// _hitable.Hited += () => TakeDamage(1);
		Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47(__this, 1, NULL);
		return;
	}
}
// System.Void KickMaster.Boss::<OnDisable>b__3_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Boss_U3COnDisableU3Eb__3_0_m316A2EA87CC32E8B8311CF12935221E6566A60AD (Boss_tDE03C89497527470C8A192DA1D6FEA5F9B6D6412* __this, const RuntimeMethod* method) 
{
	{
		// _hitable.Hited -= () => TakeDamage(1);
		Boss_TakeDamage_m878479C51C8E476A24A5739DC2F067B75F5E1B47(__this, 1, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Enemy::add_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_add_Died_m96CD283CAC5DB65801E7397F01499B412C578F44 (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_0 = NULL;
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_1 = NULL;
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_2 = NULL;
	{
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_0 = __this->___Died_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_2 = V_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)Castclass((RuntimeObject*)L_4, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var));
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27** L_5 = (&__this->___Died_4);
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_6 = V_2;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_7 = V_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_9 = V_0;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)L_9) == ((RuntimeObject*)(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Enemy::remove_Died(UnityEngine.Events.UnityAction`1<KickMaster.Enemy>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_remove_Died_m775324A6A3CAC3EF965CEBD934F3BD61BDD1484E (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_0 = NULL;
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_1 = NULL;
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* V_2 = NULL;
	{
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_0 = __this->___Died_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_2 = V_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)Castclass((RuntimeObject*)L_4, UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var));
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27** L_5 = (&__this->___Died_4);
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_6 = V_2;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_7 = V_1;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_9 = V_0;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)L_9) == ((RuntimeObject*)(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// KickMaster.Player KickMaster.Enemy::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294 (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	{
		// public Player Target => _target;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->____target_6;
		return L_0;
	}
}
// System.Int32 KickMaster.Enemy::get_Health()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68 (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	{
		// public int Health => _health;
		int32_t L_0 = __this->____health_5;
		return L_0;
	}
}
// System.Void KickMaster.Enemy::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_OnDestroy_m05D3CB65796ECD271B848EE8A869A39B0825C54C (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* G_B2_0 = NULL;
	UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* G_B1_0 = NULL;
	{
		// Died?.Invoke(this);
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_0 = __this->___Died_4;
		UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		NullCheck(G_B2_0);
		UnityAction_1_Invoke_mB4C2A9DB9EF0B918BA57AF733B43DD6DF6B8DDA4_inline(G_B2_0, __this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Enemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy__ctor_mBE04A1F7982733273E3BA67EB97A6FDC1D0F87DD (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Input::add_FingerUp(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_add_FingerUp_m4BC4A64D65E35BB62295E3C201DD91FA52A9A389 (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___FingerUp_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___FingerUp_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Input::remove_FingerUp(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_remove_FingerUp_m2B42E32D216FFC3D5A2167ABBA3B7C0F0E79E21D (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___FingerUp_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___FingerUp_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Input::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_Update_m767E0E4D068044B18454BAE32F4A53832CB8107A (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, const RuntimeMethod* method) 
{
	{
		// HandleTouchUp();
		Input_HandleTouchUp_mE47FD3BE40F65821590376B7D7E1F8C279E71959(__this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Input::HandleTouchUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input_HandleTouchUp_mE47FD3BE40F65821590376B7D7E1F8C279E71959 (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, const RuntimeMethod* method) 
{
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B5_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B4_0 = NULL;
	{
		// if (UnityEngine.Input.touchCount > 0)
		int32_t L_0;
		L_0 = Input_get_touchCount_m7B8EAAB3449A6DC2D90AF3BA36AF226D97C020CF(NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		// _isTouch = true;
		__this->____isTouch_5 = (bool)1;
		return;
	}

IL_0010:
	{
		// else if (_isTouch == true)
		bool L_1 = __this->____isTouch_5;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		// _isTouch = false;
		__this->____isTouch_5 = (bool)0;
		// FingerUp?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = __this->___FingerUp_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = L_2;
		G_B4_0 = L_3;
		if (L_3)
		{
			G_B5_0 = L_3;
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		NullCheck(G_B5_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B5_0, NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Input::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Input__ctor_m38106ACA8F618985D9260FE04C65CD94C965A8D7 (Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Level::add_Won(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Won_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Won_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Level::remove_Won(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Won_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Won_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Level::add_Defeated(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Defeated_5;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Defeated_5);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Level::remove_Defeated(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Defeated_5;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Defeated_5);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Level::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_OnEnable_m7DFB053941053ADC8D667FFB2FFE22D3C68F93E0 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// foreach (var enemy in _enemies)
		List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* L_0 = __this->____enemies_6;
		NullCheck(L_0);
		Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 L_1;
		L_1 = List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B(L_0, List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0031:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD((&V_0), Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0026_1;
			}

IL_000e_1:
			{
				// foreach (var enemy in _enemies)
				Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_2;
				L_2 = Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_inline((&V_0), Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_RuntimeMethod_var);
				// enemy.Died += OnEnemyDied;
				UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_3 = (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)il2cpp_codegen_object_new(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
				NullCheck(L_3);
				UnityAction_1__ctor_mC4ED61FE700AFBEBC481EB5F15F0204D2CC9D298(L_3, __this, (intptr_t)((void*)Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18_RuntimeMethod_var), NULL);
				NullCheck(L_2);
				Enemy_add_Died_m96CD283CAC5DB65801E7397F01499B412C578F44(L_2, L_3, NULL);
			}

IL_0026_1:
			{
				// foreach (var enemy in _enemies)
				bool L_4;
				L_4 = Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA((&V_0), Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA_RuntimeMethod_var);
				if (L_4)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_003f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003f:
	{
		// _player.Died += OnPlayerDied;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_5 = __this->____player_7;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_6, __this, (intptr_t)((void*)Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82_RuntimeMethod_var), NULL);
		NullCheck(L_5);
		Player_add_Died_mDDA2A70A8728F794B1836BDE39FE0B873EE1F00F(L_5, L_6, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Level::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_OnDisable_mCA4A40A216AA3DB70B00B1F17B143E356C4C75B4 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// foreach (var enemy in _enemies)
		List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* L_0 = __this->____enemies_6;
		NullCheck(L_0);
		Enumerator_t5DF295FB8B3067D3CDC0ABABB5B4729D116EEB63 L_1;
		L_1 = List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B(L_0, List_1_GetEnumerator_mD98240FED04DFE00D42A5DC1CFEE5492E0A3CC3B_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0031:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD((&V_0), Enumerator_Dispose_mE10838AD48B967BD716099CD5011B55A404ABEFD_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0026_1;
			}

IL_000e_1:
			{
				// foreach (var enemy in _enemies)
				Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_2;
				L_2 = Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_inline((&V_0), Enumerator_get_Current_m0A5090E94530DAD6ADDA8430751E250BE8BA5CEC_RuntimeMethod_var);
				// enemy.Died -= OnEnemyDied;
				UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27* L_3 = (UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27*)il2cpp_codegen_object_new(UnityAction_1_t34020C137CC1943371045B124011425D5FD33A27_il2cpp_TypeInfo_var);
				NullCheck(L_3);
				UnityAction_1__ctor_mC4ED61FE700AFBEBC481EB5F15F0204D2CC9D298(L_3, __this, (intptr_t)((void*)Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18_RuntimeMethod_var), NULL);
				NullCheck(L_2);
				Enemy_remove_Died_m775324A6A3CAC3EF965CEBD934F3BD61BDD1484E(L_2, L_3, NULL);
			}

IL_0026_1:
			{
				// foreach (var enemy in _enemies)
				bool L_4;
				L_4 = Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA((&V_0), Enumerator_MoveNext_m64BBF3E367B264F16036C2D0881A4F4E500E7EBA_RuntimeMethod_var);
				if (L_4)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_003f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003f:
	{
		// _player.Died -= OnPlayerDied;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_5 = __this->____player_7;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_6);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_6, __this, (intptr_t)((void*)Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82_RuntimeMethod_var), NULL);
		NullCheck(L_5);
		Player_remove_Died_mFC24CDAE84983636EADDA30EDEA10C7014D21713(L_5, L_6, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Level::Restart()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_Restart_m8B6AD2AF91323B954D3768D29AF60CA6613F9ACD (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Scene_tA1DC762B79745EB5140F054C884855B922318356 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		Scene_tA1DC762B79745EB5140F054C884855B922318356 L_0;
		L_0 = SceneManager_GetActiveScene_m2DB2A1ACB84805968A4B6396BFDFB92C0AF32BCE(NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = Scene_get_buildIndex_m82B6E0C96C85C952B7A2D794DB73CDA99AA9A57E((&V_0), NULL);
		SceneManager_LoadScene_mE00D17D79AD74B307F913BBF296A36115548DB6D(L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Level::OnEnemyDied(KickMaster.Enemy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_OnEnemyDied_m6D25AA613CEB16328E786904559F142A619A8B18 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___enemy0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m8EBB6F6ED60A4A01CC524FEFACF137C9C335DC2B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m21D8D394A4B96B49B33EC956B7AFE650AB1572AB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B3_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B2_0 = NULL;
	{
		// _enemies.Remove(enemy);
		List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* L_0 = __this->____enemies_6;
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_1 = ___enemy0;
		NullCheck(L_0);
		bool L_2;
		L_2 = List_1_Remove_m8EBB6F6ED60A4A01CC524FEFACF137C9C335DC2B(L_0, L_1, List_1_Remove_m8EBB6F6ED60A4A01CC524FEFACF137C9C335DC2B_RuntimeMethod_var);
		// if (_enemies.Count == 0)
		List_1_t7AED9FEE130B2B7A26D98448D2F4719506C1779B* L_3 = __this->____enemies_6;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = List_1_get_Count_m21D8D394A4B96B49B33EC956B7AFE650AB1572AB_inline(L_3, List_1_get_Count_m21D8D394A4B96B49B33EC956B7AFE650AB1572AB_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		// Won?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_5 = __this->___Won_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = L_5;
		G_B2_0 = L_6;
		if (L_6)
		{
			G_B3_0 = L_6;
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		NullCheck(G_B3_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B3_0, NULL);
	}

IL_002a:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Level::OnPlayerDied()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level_OnPlayerDied_m18EC6DE110967D41727F2168AAF8117E34D44A82 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, const RuntimeMethod* method) 
{
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B2_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B1_0 = NULL;
	{
		// Defeated?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Defeated_5;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		NullCheck(G_B2_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B2_0, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Level::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Level__ctor_m42177C607252949FB75A1584E63EBD1BE9584FD4 (Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.BaseProjectile::Hit(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProjectile_Hit_m95E6A0D2DF3247E406DFA5E4F37B62934BD4F102 (BaseProjectile_t4DA45DEFCD7C1378B3DB667EAC4C2B9FED1D7D00* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hit0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_mFEF05EA9F9117D5DD9F989DAC47DD639ED62F6C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* V_0 = NULL;
	{
		// if (hit.TryGetComponent(out Enemy enemy))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___hit0;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_TryGetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_mFEF05EA9F9117D5DD9F989DAC47DD639ED62F6C2(L_0, (&V_0), GameObject_TryGetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_mFEF05EA9F9117D5DD9F989DAC47DD639ED62F6C2_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_2, NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void KickMaster.BaseProjectile::Throw(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProjectile_Throw_m8B2E205E455E9722374583D07A04E10FF590B33E (BaseProjectile_t4DA45DEFCD7C1378B3DB667EAC4C2B9FED1D7D00* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___force0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_rigidbody == null)
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_0 = __this->____rigidbody_8;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// _rigidbody = gameObject.AddComponent<Rigidbody>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_3;
		L_3 = GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093(L_2, GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093_RuntimeMethod_var);
		__this->____rigidbody_8 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____rigidbody_8), (void*)L_3);
	}

IL_001f:
	{
		// _rigidbody.AddForce(force, ForceMode.Impulse);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_4 = __this->____rigidbody_8;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___force0;
		NullCheck(L_4);
		Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC(L_4, L_5, 1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.BaseProjectile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseProjectile__ctor_m0CBA0B95C4E07CFD7572288A684A017F9A46028A (BaseProjectile_t4DA45DEFCD7C1378B3DB667EAC4C2B9FED1D7D00* __this, const RuntimeMethod* method) 
{
	{
		Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.BossProjectile::Hit(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BossProjectile_Hit_m7AB0977A90C96E3B7073DB46CB9AE63D4725C347 (BossProjectile_t544BE8E12437763EC7481788B2B11751B8F22C9A* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___hit0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisPlayer_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5_m0A68D908EF4D6E6D6F7D73B2FA3EAB82A6BD1024_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* V_0 = NULL;
	{
		// if (hit.TryGetComponent(out Player player))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___hit0;
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_TryGetComponent_TisPlayer_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5_m0A68D908EF4D6E6D6F7D73B2FA3EAB82A6BD1024(L_0, (&V_0), GameObject_TryGetComponent_TisPlayer_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5_m0A68D908EF4D6E6D6F7D73B2FA3EAB82A6BD1024_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		// player.TakeDamage(_damage);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_2 = V_0;
		int32_t L_3 = __this->____damage_8;
		NullCheck(L_2);
		Player_TakeDamage_mDBDD4B6DFAE12095463ED5CA009C5D36B1FB757A(L_2, L_3, NULL);
		// Destroy(gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_4, NULL);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Void KickMaster.BossProjectile::Throw(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BossProjectile_Throw_m7E020484613940C782FF562391DD68314CE0BE67 (BossProjectile_t544BE8E12437763EC7481788B2B11751B8F22C9A* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___force0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_mCA867A56E93C824FB9C272E0DBB67655CB275941_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* V_0 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (gameObject.TryGetComponent(out Rigidbody rigidbody) == false)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_0);
		bool L_1;
		L_1 = GameObject_TryGetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_mCA867A56E93C824FB9C272E0DBB67655CB275941(L_0, (&V_0), GameObject_TryGetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_mCA867A56E93C824FB9C272E0DBB67655CB275941_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		// _rigidbody = gameObject.AddComponent<Rigidbody>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_2);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_3;
		L_3 = GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093(L_2, GameObject_AddComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m03A761629A3F71B0248F1B26EF612F592B757093_RuntimeMethod_var);
		__this->____rigidbody_9 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____rigidbody_9), (void*)L_3);
	}

IL_0020:
	{
		// if (_rigidbody.velocity.magnitude > 0)
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_4 = __this->____rigidbody_9;
		NullCheck(L_4);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Rigidbody_get_velocity_mAE331303E7214402C93E2183D0AA1198F425F843(L_4, NULL);
		V_1 = L_5;
		float L_6;
		L_6 = Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline((&V_1), NULL);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_004a;
		}
	}
	{
		// _rigidbody.velocity = Vector3.zero;
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_7 = __this->____rigidbody_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		NullCheck(L_7);
		Rigidbody_set_velocity_mE4031DF1C2C1CCE889F2AC9D8871D83795BB0D62(L_7, L_8, NULL);
	}

IL_004a:
	{
		// _rigidbody.AddForce(force, ForceMode.Impulse);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_9 = __this->____rigidbody_9;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___force0;
		NullCheck(L_9);
		Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC(L_9, L_10, 1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.BossProjectile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BossProjectile__ctor_m246570CAE5D6F1F06846DADABBFC89C6D61C485E (BossProjectile_t544BE8E12437763EC7481788B2B11751B8F22C9A* __this, const RuntimeMethod* method) 
{
	{
		Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Hitable::add_Hited(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable_add_Hited_mB073E3E72F3CE7CB3A539D8C8E8613DE8424217F (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Hited_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Hited_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Hitable::remove_Hited(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable_remove_Hited_m05F3F58E3164370ED182B97B1474241335331C85 (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Hited_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Hited_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Hitable::OnCollisionEnter(UnityEngine.Collision)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable_OnCollisionEnter_mDF96EA81BCFC6F3EFB05A5B55C7EFF570C1D41B7 (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* ___collision0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_TryGetComponent_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_mEEDC9F8E08419F16EA0E30DD83FBAC6238635A28_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B3_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B2_0 = NULL;
	{
		// if (collision.gameObject.TryGetComponent(out Projectile hitObject))
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_0 = ___collision0;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Collision_get_gameObject_m846FADBCA43E1849D3FE4D5EA44C02D055A70B3E(L_0, NULL);
		NullCheck(L_1);
		bool L_2;
		L_2 = GameObject_TryGetComponent_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_mEEDC9F8E08419F16EA0E30DD83FBAC6238635A28(L_1, (&V_0), GameObject_TryGetComponent_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_mEEDC9F8E08419F16EA0E30DD83FBAC6238635A28_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		// Hited?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = __this->___Hited_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_4 = L_3;
		G_B2_0 = L_4;
		if (L_4)
		{
			G_B3_0 = L_4;
			goto IL_001a;
		}
	}
	{
		return;
	}

IL_001a:
	{
		NullCheck(G_B3_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B3_0, NULL);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Hitable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hitable__ctor_m702F1841738A4D3BF6FB73F51620088B1F281910 (Hitable_t98A1A483472BB20DFDB45294C9CD4C3AFBD74DC4* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Kickable::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_add_Destroyed_m26D1607171283C13B73D0DECE05776DD7FD3894B (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_0 = NULL;
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_1 = NULL;
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_2 = NULL;
	{
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_0 = __this->___Destroyed_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_2 = V_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var));
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1** L_5 = (&__this->___Destroyed_4);
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_6 = V_2;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_7 = V_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_9 = V_0;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)L_9) == ((RuntimeObject*)(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Kickable::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Kickable>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_0 = NULL;
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_1 = NULL;
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* V_2 = NULL;
	{
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_0 = __this->___Destroyed_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_2 = V_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var));
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1** L_5 = (&__this->___Destroyed_4);
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_6 = V_2;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_7 = V_1;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_9 = V_0;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)L_9) == ((RuntimeObject*)(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Kickable::add_Kicked(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_add_Kicked_m990B81847DFB334DE467B5F1A1E290CB133BA8A8 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Kicked_5;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Kicked_5);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Kickable::remove_Kicked(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_remove_Kicked_mE39AC028E50A7C98DDDA75E64C47C85736FB909E (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Kicked_5;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Kicked_5);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Kickable::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_Awake_m721C43E029BBB17427FA725DE461F86797F7DF60 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _rigidbody = GetComponent<Rigidbody>();
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_0;
		L_0 = Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8(__this, Component_GetComponent_TisRigidbody_t268697F5A994213ED97393309870968BC1C7393C_m4B5CAD64B52D153BEA96432633CA9A45FA523DD8_RuntimeMethod_var);
		__this->____rigidbody_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____rigidbody_7), (void*)L_0);
		// }
		return;
	}
}
// System.Void KickMaster.Kickable::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_OnDestroy_mC51443C777E311F7444B75C85B736D2503ADEA9B (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, const RuntimeMethod* method) 
{
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* G_B2_0 = NULL;
	UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* G_B1_0 = NULL;
	{
		// Destroyed?.Invoke(this);
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_0 = __this->___Destroyed_4;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		NullCheck(G_B2_0);
		UnityAction_1_Invoke_m984AA9226F8E104E628101144CA0C6054925C662_inline(G_B2_0, __this, NULL);
		// }
		return;
	}
}
// System.Boolean KickMaster.Kickable::IsInRange(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Kickable_IsInRange_m0AEC3C5E9368DD943DAD6456F6A6669612E8E6B9 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___source0, const RuntimeMethod* method) 
{
	{
		// return Vector3.Distance(source.position, transform.position) <= _maxDistance;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = ___source0;
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2;
		L_2 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		float L_4;
		L_4 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_1, L_3, NULL);
		float L_5 = __this->____maxDistance_6;
		return (bool)((((int32_t)((!(((float)L_4) <= ((float)L_5)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void KickMaster.Kickable::Kick(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable_Kick_mF8DB1FD38E7E1778FBFA3DDB8E02BB7D94EC4B9E (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___force0, const RuntimeMethod* method) 
{
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B2_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B1_0 = NULL;
	{
		// _rigidbody.AddForce(force, ForceMode.Impulse);
		Rigidbody_t268697F5A994213ED97393309870968BC1C7393C* L_0 = __this->____rigidbody_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___force0;
		NullCheck(L_0);
		Rigidbody_AddForce_mBDBC288D0E266BC1B62E3649B4FCE46E7EA9CCBC(L_0, L_1, 1, NULL);
		// Kicked?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = __this->___Kicked_5;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		NullCheck(G_B2_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B2_0, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Kickable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kickable__ctor_mE26C76A528EE607F211D1812DEBA0E17160A73D7 (Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Projectile::add_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_1 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_2 = NULL;
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_0 = __this->___Destroyed_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_2 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var));
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1** L_5 = (&__this->___Destroyed_4);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_6 = V_2;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_7 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_9 = V_0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_9) == ((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Projectile::remove_Destroyed(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_1 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_2 = NULL;
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_0 = __this->___Destroyed_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_2 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var));
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1** L_5 = (&__this->___Destroyed_4);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_6 = V_2;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_7 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_9 = V_0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_9) == ((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Projectile::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_Awake_mFC54ADBB9C25E212EE12B819EA8B8F7D56006BC9 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, const RuntimeMethod* method) 
{
	{
		// _startPosotion = transform.position;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		__this->____startPosotion_7 = L_1;
		// }
		return;
	}
}
// System.Void KickMaster.Projectile::OnCollisionEnter(UnityEngine.Collision)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_OnCollisionEnter_m0816DAC6910136BB80E476A9D939E83CC54E4B88 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* ___collision0, const RuntimeMethod* method) 
{
	{
		// Hit(collision.gameObject);
		Collision_tBCC6AEBD9A63E6DA2E50660DAC03CDCB1FF7A9B0* L_0 = ___collision0;
		NullCheck(L_0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Collision_get_gameObject_m846FADBCA43E1849D3FE4D5EA44C02D055A70B3E(L_0, NULL);
		VirtualActionInvoker1< GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* >::Invoke(5 /* System.Void KickMaster.Projectile::Hit(UnityEngine.GameObject) */, __this, L_1);
		// }
		return;
	}
}
// System.Void KickMaster.Projectile::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_Update_mAEE8BB28EE8207A8555570793637C73EB0B33159 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Vector3.Distance(_startPosotion, transform.position) >= _destroyDistance)
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = __this->____startPosotion_7;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_1, NULL);
		float L_3;
		L_3 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_0, L_2, NULL);
		float L_4 = __this->____destroyDistance_6;
		if ((!(((float)L_3) >= ((float)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_5, NULL);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Projectile::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile_OnDestroy_m698170A4ED48DDE50B71F06B41B262A174B96AA3 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, const RuntimeMethod* method) 
{
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* G_B2_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* G_B1_0 = NULL;
	{
		// Destroyed?.Invoke(this);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_0 = __this->___Destroyed_4;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		NullCheck(G_B2_0);
		UnityAction_1_Invoke_mC1007382E3AC52C15BE12F47ADE7E9951F6559F1_inline(G_B2_0, __this, NULL);
		// }
		return;
	}
}
// System.Boolean KickMaster.Projectile::IsInRange(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Projectile_IsInRange_m06E3AD8E3349516B0C3AA623100F84073083784F (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___source0, const RuntimeMethod* method) 
{
	{
		// return Vector3.Distance(transform.position, source.position) <= _maxInteractionDistance;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2 = ___source0;
		NullCheck(L_2);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_2, NULL);
		float L_4;
		L_4 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_1, L_3, NULL);
		float L_5 = __this->____maxInteractionDistance_5;
		return (bool)((((int32_t)((!(((float)L_4) <= ((float)L_5)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void KickMaster.Projectile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Projectile__ctor_m29811E6D0C0726E0AC60289F65BB7053B626BE28 (Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Pause::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_Awake_mEB3BFDABBA5C26125648132B45CA55165A1C41FC (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	{
		// ResetPause();
		Pause_ResetPause_m12F854B48693949D669F711CA5DDFD71D2FDD98F(__this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Pause::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_OnEnable_m6255763E23DE5B809D100D17936EA75E82ED4B3A (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _level.Won += SetPause;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_0 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763(L_0, L_1, NULL);
		// _level.Defeated += SetPause;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_2 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Pause::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_OnDisable_m187B38B8373ADA63FD05B3094E20FF4F83487C87 (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _level.Won -= SetPause;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_0 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389(L_0, L_1, NULL);
		// _level.Defeated -= SetPause;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_2 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Pause::SetPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_SetPause_m2D26744CC0F26F2FDDF3DE686B2160A06C170C5A (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	{
		// Time.timeScale = 0;
		Time_set_timeScale_mD6CAA4968D796C4AF198ACFB2267BDBD06DB349C((0.0f), NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Pause::ResetPause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause_ResetPause_m12F854B48693949D669F711CA5DDFD71D2FDD98F (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	{
		// Time.timeScale = 1;
		Time_set_timeScale_mD6CAA4968D796C4AF198ACFB2267BDBD06DB349C((1.0f), NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Pause::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pause__ctor_m56325ACDF5E0668ED84CCD758AC3A0202B221EC7 (Pause_t8089C16F9BD7090C84055E8237A1A845E865867F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Kick::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_OnEnable_m3242542878250EA24D9E1C711A388413930CE859 (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB V_1;
	memset((&V_1), 0, sizeof(V_1));
	Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// foreach (var throwingState in _throwingStates)
		List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* L_0 = __this->____throwingStates_7;
		NullCheck(L_0);
		Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 L_1;
		L_1 = List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2(L_0, List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0031:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530((&V_0), Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0026_1;
			}

IL_000e_1:
			{
				// foreach (var throwingState in _throwingStates)
				ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_2;
				L_2 = Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_inline((&V_0), Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_RuntimeMethod_var);
				// throwingState.ProjectileCreated += AddProjectile;
				UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
				NullCheck(L_3);
				UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_3, __this, (intptr_t)((void*)Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF_RuntimeMethod_var), NULL);
				NullCheck(L_2);
				ThrowingState_add_ProjectileCreated_m393F0C3F7147E89601AE303379137A2922597AE0(L_2, L_3, NULL);
			}

IL_0026_1:
			{
				// foreach (var throwingState in _throwingStates)
				bool L_4;
				L_4 = Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E((&V_0), Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E_RuntimeMethod_var);
				if (L_4)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_003f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003f:
	{
		// foreach (var projectile in _projectiles)
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_5 = __this->____projectiles_6;
		NullCheck(L_5);
		Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB L_6;
		L_6 = List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522(L_5, List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		V_1 = L_6;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0070:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4((&V_1), Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0065_1;
			}

IL_004d_1:
			{
				// foreach (var projectile in _projectiles)
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_7;
				L_7 = Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_inline((&V_1), Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
				// projectile.Destroyed += RemoveProjectile;
				UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
				NullCheck(L_8);
				UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_8, __this, (intptr_t)((void*)Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var), NULL);
				NullCheck(L_7);
				Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937(L_7, L_8, NULL);
			}

IL_0065_1:
			{
				// foreach (var projectile in _projectiles)
				bool L_9;
				L_9 = Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06((&V_1), Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
				if (L_9)
				{
					goto IL_004d_1;
				}
			}
			{
				goto IL_007e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_007e:
	{
		// foreach (var kickable in _kickables)
		List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* L_10 = __this->____kickables_5;
		NullCheck(L_10);
		Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 L_11;
		L_11 = List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C(L_10, List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		V_2 = L_11;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00af:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF((&V_2), Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00a4_1;
			}

IL_008c_1:
			{
				// foreach (var kickable in _kickables)
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_12;
				L_12 = Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_inline((&V_2), Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
				// kickable.Destroyed += RemoveKickable;
				UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_13 = (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)il2cpp_codegen_object_new(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
				NullCheck(L_13);
				UnityAction_1__ctor_mF4C17AF6C00B0D1959A3ED418CEFB50431B3E4A9(L_13, __this, (intptr_t)((void*)Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var), NULL);
				NullCheck(L_12);
				Kickable_add_Destroyed_m26D1607171283C13B73D0DECE05776DD7FD3894B(L_12, L_13, NULL);
			}

IL_00a4_1:
			{
				// foreach (var kickable in _kickables)
				bool L_14;
				L_14 = Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1((&V_2), Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
				if (L_14)
				{
					goto IL_008c_1;
				}
			}
			{
				goto IL_00bd;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00bd:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Kick::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_OnDisable_m9E64A173E34DBC9AC413BD78D3431AFDB4BD1D03 (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB V_1;
	memset((&V_1), 0, sizeof(V_1));
	Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// foreach (var throwingState in _throwingStates)
		List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* L_0 = __this->____throwingStates_7;
		NullCheck(L_0);
		Enumerator_tFE1D6892EFFD314438B5C1189600A5D288C16313 L_1;
		L_1 = List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2(L_0, List_1_GetEnumerator_m5C86531E5C2BB5A1E945E87237CF317856A59EC2_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0031:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530((&V_0), Enumerator_Dispose_m381B2F022DD91846610AA8B289DA9007C8F58530_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0026_1;
			}

IL_000e_1:
			{
				// foreach (var throwingState in _throwingStates)
				ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_2;
				L_2 = Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_inline((&V_0), Enumerator_get_Current_m33CC243F631AC791836B9B89D54156BB99647BFE_RuntimeMethod_var);
				// throwingState.ProjectileCreated -= AddProjectile;
				UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
				NullCheck(L_3);
				UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_3, __this, (intptr_t)((void*)Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF_RuntimeMethod_var), NULL);
				NullCheck(L_2);
				ThrowingState_remove_ProjectileCreated_mCC5531DCD64836A19173BCE84635F2FD5EFDE086(L_2, L_3, NULL);
			}

IL_0026_1:
			{
				// foreach (var throwingState in _throwingStates)
				bool L_4;
				L_4 = Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E((&V_0), Enumerator_MoveNext_m8D97FA794C707B2B796CA0689E13CA4D0B67B29E_RuntimeMethod_var);
				if (L_4)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_003f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003f:
	{
		// foreach (var projectile in _projectiles)
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_5 = __this->____projectiles_6;
		NullCheck(L_5);
		Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB L_6;
		L_6 = List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522(L_5, List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		V_1 = L_6;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0070:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4((&V_1), Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0065_1;
			}

IL_004d_1:
			{
				// foreach (var projectile in _projectiles)
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_7;
				L_7 = Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_inline((&V_1), Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
				// projectile.Destroyed -= RemoveProjectile;
				UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
				NullCheck(L_8);
				UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_8, __this, (intptr_t)((void*)Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var), NULL);
				NullCheck(L_7);
				Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331(L_7, L_8, NULL);
			}

IL_0065_1:
			{
				// foreach (var projectile in _projectiles)
				bool L_9;
				L_9 = Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06((&V_1), Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
				if (L_9)
				{
					goto IL_004d_1;
				}
			}
			{
				goto IL_007e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_007e:
	{
		// foreach (var kickable in _kickables)
		List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* L_10 = __this->____kickables_5;
		NullCheck(L_10);
		Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 L_11;
		L_11 = List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C(L_10, List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		V_2 = L_11;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00af:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF((&V_2), Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00a4_1;
			}

IL_008c_1:
			{
				// foreach (var kickable in _kickables)
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_12;
				L_12 = Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_inline((&V_2), Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
				// kickable.Destroyed -= RemoveKickable;
				UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_13 = (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)il2cpp_codegen_object_new(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
				NullCheck(L_13);
				UnityAction_1__ctor_mF4C17AF6C00B0D1959A3ED418CEFB50431B3E4A9(L_13, __this, (intptr_t)((void*)Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var), NULL);
				NullCheck(L_12);
				Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018(L_12, L_13, NULL);
			}

IL_00a4_1:
			{
				// foreach (var kickable in _kickables)
				bool L_14;
				L_14 = Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1((&V_2), Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
				if (L_14)
				{
					goto IL_008c_1;
				}
			}
			{
				goto IL_00bd;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00bd:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Kick::AddProjectile(KickMaster.Projectile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_AddProjectile_mB087C8C3D1D475985FF845DAEA3F065253D38CEF (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___projectile0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7C6294F5B3C8ECA89999B8E4578ABBEB1A3B77C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _projectiles.Add(projectile);
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_0 = __this->____projectiles_6;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_1 = ___projectile0;
		NullCheck(L_0);
		List_1_Add_m7C6294F5B3C8ECA89999B8E4578ABBEB1A3B77C3_inline(L_0, L_1, List_1_Add_m7C6294F5B3C8ECA89999B8E4578ABBEB1A3B77C3_RuntimeMethod_var);
		// projectile.Destroyed += RemoveProjectile;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_2 = ___projectile0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_3, __this, (intptr_t)((void*)Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		Projectile_add_Destroyed_m4744B3AA41FB82C297892EE1B585050BC2041937(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Kick::RemoveProjectile(KickMaster.Projectile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3 (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___projectile0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m48E0BD9D0F4E6BF369753EFFE945A863DAFF8ABC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// projectile.Destroyed -= RemoveProjectile;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_0 = ___projectile0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = (UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)il2cpp_codegen_object_new(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction_1__ctor_mFC82C68EE45D66366797EF3087CFC045D7559A6A(L_1, __this, (intptr_t)((void*)Kick_RemoveProjectile_mD1AB39D72EBD1049A3536F72BEA0E82A626D8AC3_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Projectile_remove_Destroyed_m615B9EE334CB14065D7A641A6937D8155193F331(L_0, L_1, NULL);
		// _projectiles.Remove(projectile);
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_2 = __this->____projectiles_6;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_3 = ___projectile0;
		NullCheck(L_2);
		bool L_4;
		L_4 = List_1_Remove_m48E0BD9D0F4E6BF369753EFFE945A863DAFF8ABC(L_2, L_3, List_1_Remove_m48E0BD9D0F4E6BF369753EFFE945A863DAFF8ABC_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void KickMaster.Kick::RemoveKickable(KickMaster.Kickable)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923 (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* ___kickable0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4873E203826D9E39239EDDF86822BF6E8C4DDB1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// kickable.Destroyed -= RemoveKickable;
		Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_0 = ___kickable0;
		UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1* L_1 = (UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1*)il2cpp_codegen_object_new(UnityAction_1_tA3E42F0AC982ACAA7786561F382AC2B27ACC6FA1_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction_1__ctor_mF4C17AF6C00B0D1959A3ED418CEFB50431B3E4A9(L_1, __this, (intptr_t)((void*)Kick_RemoveKickable_m207CC0E719D09724F4338C5237025CBB05BD4923_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Kickable_remove_Destroyed_m6E1F55C513EE10117DB8AD495513AC9C7570D018(L_0, L_1, NULL);
		// _kickables.Remove(kickable);
		List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* L_2 = __this->____kickables_5;
		Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_3 = ___kickable0;
		NullCheck(L_2);
		bool L_4;
		L_4 = List_1_Remove_m4873E203826D9E39239EDDF86822BF6E8C4DDB1E(L_2, L_3, List_1_Remove_m4873E203826D9E39239EDDF86822BF6E8C4DDB1E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void KickMaster.Kick::ApplyOnce()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick_ApplyOnce_mABEB15CF82185C7363539EA16C24088467756181 (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB V_0;
	memset((&V_0), 0, sizeof(V_0));
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* V_1 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 V_3;
	memset((&V_3), 0, sizeof(V_3));
	Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* V_4 = NULL;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		// foreach (var projectile in _projectiles)
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_0 = __this->____projectiles_6;
		NullCheck(L_0);
		Enumerator_t26245AFBD737D244C529B79F2F2159398E1A68BB L_1;
		L_1 = List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522(L_0, List_1_GetEnumerator_mAB45A167AA93B10932A4981C090859E7FF281522_RuntimeMethod_var);
		V_0 = L_1;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_005d:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4((&V_0), Enumerator_Dispose_m1F29731B21FDA998A31E46C542AF6977A3CBFAA4_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0052_1;
			}

IL_000e_1:
			{
				// foreach (var projectile in _projectiles)
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_2;
				L_2 = Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_inline((&V_0), Enumerator_get_Current_m0B2FF59EA133377BA8F74265F935E2C05A6F531A_RuntimeMethod_var);
				V_1 = L_2;
				// if (projectile.IsInRange(transform) == false)
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_3 = V_1;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
				L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
				NullCheck(L_3);
				bool L_5;
				L_5 = Projectile_IsInRange_m06E3AD8E3349516B0C3AA623100F84073083784F(L_3, L_4, NULL);
				if (!L_5)
				{
					goto IL_0052_1;
				}
			}
			{
				// Vector3 forceDirection = projectile.transform.position - transform.position;
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_6 = V_1;
				NullCheck(L_6);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
				L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_6, NULL);
				NullCheck(L_7);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
				L_8 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_7, NULL);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9;
				L_9 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
				NullCheck(L_9);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
				L_10 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_9, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
				L_11 = Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline(L_8, L_10, NULL);
				V_2 = L_11;
				// projectile.Throw(forceDirection * _force);
				Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_12 = V_1;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_2;
				float L_14 = __this->____force_4;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
				L_15 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_13, L_14, NULL);
				NullCheck(L_12);
				VirtualActionInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 >::Invoke(4 /* System.Void KickMaster.Projectile::Throw(UnityEngine.Vector3) */, L_12, L_15);
			}

IL_0052_1:
			{
				// foreach (var projectile in _projectiles)
				bool L_16;
				L_16 = Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06((&V_0), Enumerator_MoveNext_mA1250619BD472096133F77B148D5171C1961EA06_RuntimeMethod_var);
				if (L_16)
				{
					goto IL_000e_1;
				}
			}
			{
				goto IL_006b;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_006b:
	{
		// foreach (var kickable in _kickables)
		List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* L_17 = __this->____kickables_5;
		NullCheck(L_17);
		Enumerator_t51861CF0AF7D712B42F7AAC6581B042D59DCAEA4 L_18;
		L_18 = List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C(L_17, List_1_GetEnumerator_m44DCAA61232277E66443CFC97720BE38BBE2310C_RuntimeMethod_var);
		V_3 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_00d3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF((&V_3), Enumerator_Dispose_mDEF8F561DD9377D11329521834DCE5A8B19D7BCF_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_00c8_1;
			}

IL_0079_1:
			{
				// foreach (var kickable in _kickables)
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_19;
				L_19 = Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_inline((&V_3), Enumerator_get_Current_mF9CA7EA8AB4F898F50C5F58A1D8D1600E9D2884E_RuntimeMethod_var);
				V_4 = L_19;
				// if (kickable.IsInRange(transform) == false)
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_20 = V_4;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_21;
				L_21 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
				NullCheck(L_20);
				bool L_22;
				L_22 = Kickable_IsInRange_m0AEC3C5E9368DD943DAD6456F6A6669612E8E6B9(L_20, L_21, NULL);
				if (!L_22)
				{
					goto IL_00c8_1;
				}
			}
			{
				// Vector3 forceDirection = kickable.transform.position - transform.position;
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_23 = V_4;
				NullCheck(L_23);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_24;
				L_24 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_23, NULL);
				NullCheck(L_24);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
				L_25 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_24, NULL);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_26;
				L_26 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
				NullCheck(L_26);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27;
				L_27 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_26, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_28;
				L_28 = Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline(L_25, L_27, NULL);
				V_5 = L_28;
				// kickable.Kick(forceDirection.normalized * _force);
				Kickable_tB0C1295FF1F9C929B56935A4D4E69FA30B5158C9* L_29 = V_4;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
				L_30 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline((&V_5), NULL);
				float L_31 = __this->____force_4;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
				L_32 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_30, L_31, NULL);
				NullCheck(L_29);
				Kickable_Kick_mF8DB1FD38E7E1778FBFA3DDB8E02BB7D94EC4B9E(L_29, L_32, NULL);
			}

IL_00c8_1:
			{
				// foreach (var kickable in _kickables)
				bool L_33;
				L_33 = Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1((&V_3), Enumerator_MoveNext_m1EE21607C55C3EAF9B142E21611E74FF213776D1_RuntimeMethod_var);
				if (L_33)
				{
					goto IL_0079_1;
				}
			}
			{
				goto IL_00e1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_00e1:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Kick::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Kick__ctor_mCA4BC7B933DC9BAE1E298113D625D0E5A143BE6D (Kick_t072769B200F62ADACD95B1032119EC55CFBECF0D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mAC29D9D5108AA6D94BA97759E1E1680504442F9F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mAEC3C87A864E42B46F6D42A695E4F28235434A1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mEE327D7F83780A435BB6EDDDA3F1898A8EF032E8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private List<Kickable> _kickables = new List<Kickable>();
		List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3* L_0 = (List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3*)il2cpp_codegen_object_new(List_1_t11983F4918E1CE119B82FF594C2BF791D3A427E3_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		List_1__ctor_mAC29D9D5108AA6D94BA97759E1E1680504442F9F(L_0, List_1__ctor_mAC29D9D5108AA6D94BA97759E1E1680504442F9F_RuntimeMethod_var);
		__this->____kickables_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____kickables_5), (void*)L_0);
		// [SerializeField] private List<Projectile> _projectiles = new List<Projectile>();
		List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F* L_1 = (List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F*)il2cpp_codegen_object_new(List_1_t00A88A80B6AF9AF2FBE5C7EAB8BE11C958C75A4F_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		List_1__ctor_mAEC3C87A864E42B46F6D42A695E4F28235434A1A(L_1, List_1__ctor_mAEC3C87A864E42B46F6D42A695E4F28235434A1A_RuntimeMethod_var);
		__this->____projectiles_6 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____projectiles_6), (void*)L_1);
		// [SerializeField] private List<ThrowingState> _throwingStates = new List<ThrowingState>();
		List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0* L_2 = (List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0*)il2cpp_codegen_object_new(List_1_t955F7A43618FF3B798A6EBDC9D1360B6E104C4A0_il2cpp_TypeInfo_var);
		NullCheck(L_2);
		List_1__ctor_mEE327D7F83780A435BB6EDDDA3F1898A8EF032E8(L_2, List_1__ctor_mEE327D7F83780A435BB6EDDDA3F1898A8EF032E8_RuntimeMethod_var);
		__this->____throwingStates_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____throwingStates_7), (void*)L_2);
		PlayerAction__ctor_m2F60E6DB5734572075E34991A6DD818B68C9BF8B(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.PlayerAction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerAction__ctor_m2F60E6DB5734572075E34991A6DD818B68C9BF8B (PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.Player::add_Died(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_add_Died_mDDA2A70A8728F794B1836BDE39FE0B873EE1F00F (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Died_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Died_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.Player::remove_Died(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_remove_Died_mFC24CDAE84983636EADDA30EDEA10C7014D21713 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_1 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* V_2 = NULL;
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Died_4;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = V_0;
		V_1 = L_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_2 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)CastclassSealed((RuntimeObject*)L_4, UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var));
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7** L_5 = (&__this->___Died_4);
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_6 = V_2;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_7 = V_1;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_9 = V_0;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_9) == ((RuntimeObject*)(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 KickMaster.Player::get_Health()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Player_get_Health_m47669E1B8F1F22EFAC58E0684CF7D3FC83787547 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) 
{
	{
		// public int Health => _health;
		int32_t L_0 = __this->____health_5;
		return L_0;
	}
}
// System.Void KickMaster.Player::TakeDamage(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_TakeDamage_mDBDD4B6DFAE12095463ED5CA009C5D36B1FB757A (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// _health -= value;
		int32_t L_0 = __this->____health_5;
		int32_t L_1 = ___value0;
		__this->____health_5 = ((int32_t)il2cpp_codegen_subtract(L_0, L_1));
		// if (_health <= 0)
		int32_t L_2 = __this->____health_5;
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		// Die();
		Player_Die_m42DB081342293F085036E90E510C58BAA26DD741(__this, NULL);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Void KickMaster.Player::Kill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Kill_mD2AACC78D1E870C57334FB25CBA424C9CB984025 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) 
{
	{
		// _health = 0;
		__this->____health_5 = 0;
		// Die();
		Player_Die_m42DB081342293F085036E90E510C58BAA26DD741(__this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Player::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_Die_m42DB081342293F085036E90E510C58BAA26DD741 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) 
{
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B2_0 = NULL;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* G_B1_0 = NULL;
	{
		// Died?.Invoke();
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_0 = __this->___Died_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		NullCheck(G_B2_0);
		UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline(G_B2_0, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.Player::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__ctor_m6F4748DC2C21349A1C2D98BFBE4F770D74955813 (Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.PlayerActions::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerActions_OnEnable_m31D2FFF2A8ED18941AD50AD8FF85B07CBA0752D1 (PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _input.FingerUp += OnFingerUp;
		Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* L_0 = __this->____input_5;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Input_add_FingerUp_m4BC4A64D65E35BB62295E3C201DD91FA52A9A389(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.PlayerActions::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerActions_OnDisable_m2F102E98C0FC8B200A760353B66533557097BDD5 (PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _input.FingerUp -= OnFingerUp;
		Input_tB0071F5F09E633D9991409B1C28CB4803CFD85CB* L_0 = __this->____input_5;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Input_remove_FingerUp_m2B42E32D216FFC3D5A2167ABBA3B7C0F0E79E21D(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.PlayerActions::OnFingerUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerActions_OnFingerUp_mF7A221382EEA98F355D0354BD3B9E4D3FD89A0B9 (PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F* __this, const RuntimeMethod* method) 
{
	PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// _legHitAnimation.Play();
		Animation_t6593B06C39E3B139808B19F2C719C860F3F61040* L_0 = __this->____legHitAnimation_4;
		NullCheck(L_0);
		bool L_1;
		L_1 = Animation_Play_m717560D2F561D9E12583AB3B435E6BC996448C3E(L_0, NULL);
		// foreach (var action in _actions)
		PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE* L_2 = __this->____actions_6;
		V_0 = L_2;
		V_1 = 0;
		goto IL_0023;
	}

IL_0017:
	{
		// foreach (var action in _actions)
		PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		PlayerAction_t6136AD439DC719A346EF56C1451B2F7270BAAD02* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		// action.ApplyOnce();
		NullCheck(L_6);
		VirtualActionInvoker0::Invoke(4 /* System.Void KickMaster.PlayerAction::ApplyOnce() */, L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_7, 1));
	}

IL_0023:
	{
		// foreach (var action in _actions)
		int32_t L_8 = V_1;
		PlayerActionU5BU5D_t61FBAC0FD39ABD7EBCE4CC8F7076286EF2B8B9DE* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)((int32_t)(((RuntimeArray*)L_9)->max_length)))))
		{
			goto IL_0017;
		}
	}
	{
		// }
		return;
	}
}
// System.Void KickMaster.PlayerActions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerActions__ctor_mC807036BD35DA2BF8477407CD931AA4261A4863F (PlayerActions_t57E4DF96EF34D86A53468D7E9B67C9599A6F0D3F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.PlayerMove::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove_Start_m8664D6832C13BFA99D30C93642E9B2ADF00AD97D (PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A_m96B48A644EDC97C5C82F154D1FEA551B2E392040_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _controller = GetComponent<CharacterController>();
		CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* L_0;
		L_0 = Component_GetComponent_TisCharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A_m96B48A644EDC97C5C82F154D1FEA551B2E392040(__this, Component_GetComponent_TisCharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A_m96B48A644EDC97C5C82F154D1FEA551B2E392040_RuntimeMethod_var);
		__this->____controller_7 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____controller_7), (void*)L_0);
		// }
		return;
	}
}
// System.Void KickMaster.PlayerMove::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove_Update_m95D710E55CFBA4FB1E96503554A5DA7AA7797EAF (PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7* __this, const RuntimeMethod* method) 
{
	{
		// Move();
		PlayerMove_Move_mB2A2C255A530412FC82230FC4C496BFFE0EB2AAE(__this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.PlayerMove::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove_Move_mB2A2C255A530412FC82230FC4C496BFFE0EB2AAE (PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (_joystick.Direction != Vector2.zero)
		Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* L_0 = __this->____joystick_6;
		NullCheck(L_0);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1;
		L_1 = Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC(L_0, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2;
		L_2 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		bool L_3;
		L_3 = Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline(L_1, L_2, NULL);
		if (!L_3)
		{
			goto IL_0065;
		}
	}
	{
		// var joystickPosition = new Vector3(_joystick.Direction.x, 0, _joystick.Direction.y);
		Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* L_4 = __this->____joystick_6;
		NullCheck(L_4);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5;
		L_5 = Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC(L_4, NULL);
		float L_6 = L_5.___x_0;
		Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* L_7 = __this->____joystick_6;
		NullCheck(L_7);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		L_8 = Joystick_get_Direction_m52502695D42BDAB6075089BDE442ABE72EAC81EC(L_7, NULL);
		float L_9 = L_8.___y_1;
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&V_0), L_6, (0.0f), L_9, NULL);
		// _controller.Move(joystickPosition * _moveSpeed * Time.deltaTime);
		CharacterController_t847C1A2719F60547D7D6077B648D6CE2D1EF3A6A* L_10 = __this->____controller_7;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11 = V_0;
		float L_12 = __this->____moveSpeed_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_11, L_12, NULL);
		float L_14;
		L_14 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_13, L_14, NULL);
		NullCheck(L_10);
		int32_t L_16;
		L_16 = CharacterController_Move_mE3F7AC1B4A2D6955980811C088B68ED3A31D2DA4(L_10, L_15, NULL);
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void KickMaster.PlayerMove::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerMove__ctor_m6F4650C14C9995809A00DB2191EE0A0FB5C80304 (PlayerMove_tB161D30CC615D397C8F257510AE20609FB37F6E7* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.EndScreens::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndScreens_OnEnable_m38FD85EED973FB006BBA78351DAFBF5890589717 (EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _level.Won += ShowWinScreen;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_0 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Level_add_Won_m5F420C01DB77110EA710C5CA78D18617F99E1763(L_0, L_1, NULL);
		// _level.Defeated += ShowDefeatScreen;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_2 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		Level_add_Defeated_mAC6BB374BA6D38E099B65A8883CD7CACD9D5C180(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.EndScreens::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndScreens_OnDisable_mD6A4BA9F4E297AF3412EC6992301B7C97CB62434 (EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _level.Won -= ShowWinScreen;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_0 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_1 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_1);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_1, __this, (intptr_t)((void*)EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429_RuntimeMethod_var), NULL);
		NullCheck(L_0);
		Level_remove_Won_mFC422EF0360E14E0DB012A4D3EB0244AFAB87389(L_0, L_1, NULL);
		// _level.Defeated -= ShowDefeatScreen;
		Level_tD67DCBE6CA68BF724459842DE02E7FB870AEBB07* L_2 = __this->____level_4;
		UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* L_3 = (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7*)il2cpp_codegen_object_new(UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7_il2cpp_TypeInfo_var);
		NullCheck(L_3);
		UnityAction__ctor_mC53E20D6B66E0D5688CD81B88DBB34F5A58B7131(L_3, __this, (intptr_t)((void*)EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D_RuntimeMethod_var), NULL);
		NullCheck(L_2);
		Level_remove_Defeated_m71CAD1A43FE5D034D64B58D855791809E9046BED(L_2, L_3, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.EndScreens::ShowWinScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndScreens_ShowWinScreen_m77C2531C775CA12022D6C2EFE7D1E9254416A429 (EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6* __this, const RuntimeMethod* method) 
{
	{
		// _winScreen.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->____winScreen_5;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.EndScreens::ShowDefeatScreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndScreens_ShowDefeatScreen_m0A40513C50375B82A17A1FD86DCEEA730D5F2B3D (EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6* __this, const RuntimeMethod* method) 
{
	{
		// _defeatScreen.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->____defeatScreen_6;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.EndScreens::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndScreens__ctor_mCF0C95954053AFE91AC3A318C1DBB9547025CC27 (EndScreens_t8D35B9C51744FABD7F32A4E82C058B100A1C52A6* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// KickMaster.StateMachine.State KickMaster.StateMachine.EnemyStateMachine::get_CurrentState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* EnemyStateMachine_get_CurrentState_mC71C1387E7E632FCDB69969EC93BB43E582D636B (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) 
{
	{
		// public State CurrentState => _currentState;
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____currentState_6;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.EnemyStateMachine::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_Start_mBCD5CCAEEBC5B893C88291C481B6C9131230F2C2 (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _target = GetComponent<Enemy>().Target;
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0;
		L_0 = Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4(__this, Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		NullCheck(L_0);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_1;
		L_1 = Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294_inline(L_0, NULL);
		__this->____target_5 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____target_5), (void*)L_1);
		// ResetState();
		EnemyStateMachine_ResetState_m4B33D21E43676185CCA7A69618BD8510FB73FD48(__this, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.EnemyStateMachine::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_Update_mB78E5B02C84801E8D8E0618478DC4A1E2A2B2691 (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* V_0 = NULL;
	{
		// if (_currentState == null)
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____currentState_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// State nextState = _currentState.GetNextState();
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_2 = __this->____currentState_6;
		NullCheck(L_2);
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_3;
		L_3 = State_GetNextState_m6BC7F69BE96489665470DB3B0126DF69D5E3E9E8(L_2, NULL);
		V_0 = L_3;
		// if (nextState != null)
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_4 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		// Transit(nextState);
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_6 = V_0;
		EnemyStateMachine_Transit_mAC3F54831E0C7B635B3DFD3D8B348D576BA75BAB(__this, L_6, NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.EnemyStateMachine::ResetState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_ResetState_m4B33D21E43676185CCA7A69618BD8510FB73FD48 (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _currentState = _initialState;
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____initialState_4;
		__this->____currentState_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____currentState_6), (void*)L_0);
		// if (_currentState != null)
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_1 = __this->____currentState_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_1, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		// _currentState.Enter(_target);
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_3 = __this->____currentState_6;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_4 = __this->____target_5;
		NullCheck(L_3);
		State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3(L_3, L_4, NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.EnemyStateMachine::Transit(KickMaster.StateMachine.State)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine_Transit_mAC3F54831E0C7B635B3DFD3D8B348D576BA75BAB (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* ___state0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_currentState != null)
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____currentState_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// _currentState.Exit();
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_2 = __this->____currentState_6;
		NullCheck(L_2);
		State_Exit_m21D27445EFB2088E0D3CAD411B95DF23C3583918(L_2, NULL);
	}

IL_0019:
	{
		// _currentState = state;
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_3 = ___state0;
		__this->____currentState_6 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____currentState_6), (void*)L_3);
		// if (_currentState != null)
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_4 = __this->____currentState_6;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_4, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		// _currentState.Enter(_target);
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_6 = __this->____currentState_6;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_7 = __this->____target_5;
		NullCheck(L_6);
		State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3(L_6, L_7, NULL);
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.EnemyStateMachine::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyStateMachine__ctor_mB6EBA9361DF4A9920479BB3D6D676D88B81F4A61 (EnemyStateMachine_t32E89D4E8A0402E27C7A3C2C96AE0F61F4C6E10E* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// KickMaster.Player KickMaster.StateMachine.State::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->___U3CTargetU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.State::set_Target(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = ___value0;
		__this->___U3CTargetU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CTargetU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
// System.Void KickMaster.StateMachine.State::Enter(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State_Enter_m3BB12FA6C11FAB5849B7F031F5BB17D8F6C60CB3 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___target0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (enabled == false)
		bool L_0;
		L_0 = Behaviour_get_enabled_mAAC9F15E9EBF552217A5AE2681589CC0BFA300C1(__this, NULL);
		if (L_0)
		{
			goto IL_0055;
		}
	}
	{
		// Target = target;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_1 = ___target0;
		State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0_inline(__this, L_1, NULL);
		// enabled = true;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)1, NULL);
		// for (int i = 0; i < _transitions.Count; i++)
		V_0 = 0;
		goto IL_0047;
	}

IL_001a:
	{
		// _transitions[i].enabled = true;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_2 = __this->____transitions_4;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* L_4;
		L_4 = List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3(L_2, L_3, List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		NullCheck(L_4);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_4, (bool)1, NULL);
		// _transitions[i].Init(Target);
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_5 = __this->____transitions_4;
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* L_7;
		L_7 = List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3(L_5, L_6, List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_8;
		L_8 = State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline(__this, NULL);
		NullCheck(L_7);
		Transition_Init_m3735DD0482CD276E2D9D25FF21691DA308A116A0(L_7, L_8, NULL);
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0047:
	{
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_10 = V_0;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_11 = __this->____transitions_4;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_inline(L_11, List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_0055:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.State::Exit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State_Exit_m21D27445EFB2088E0D3CAD411B95DF23C3583918 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (enabled == true)
		bool L_0;
		L_0 = Behaviour_get_enabled_mAAC9F15E9EBF552217A5AE2681589CC0BFA300C1(__this, NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		// for (int i = 0; i < _transitions.Count; i++)
		V_0 = 0;
		goto IL_0022;
	}

IL_000c:
	{
		// _transitions[i].enabled = false;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_1 = __this->____transitions_4;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* L_3;
		L_3 = List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3(L_1, L_2, List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		NullCheck(L_3);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_3, (bool)0, NULL);
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_4, 1));
	}

IL_0022:
	{
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_5 = V_0;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_6 = __this->____transitions_4;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_inline(L_6, List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_000c;
		}
	}
	{
		// enabled = false;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
	}

IL_0037:
	{
		// }
		return;
	}
}
// KickMaster.StateMachine.State KickMaster.StateMachine.State::GetNextState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* State_GetNextState_m6BC7F69BE96489665470DB3B0126DF69D5E3E9E8 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < _transitions.Count; i++)
		V_0 = 0;
		goto IL_002d;
	}

IL_0004:
	{
		// if (_transitions[i].NeedTransit)
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_0 = __this->____transitions_4;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* L_2;
		L_2 = List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3(L_0, L_1, List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		NullCheck(L_2);
		bool L_3;
		L_3 = Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B_inline(L_2, NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		// return _transitions[i].TargetState;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_4 = __this->____transitions_4;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* L_6;
		L_6 = List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3(L_4, L_5, List_1_get_Item_m58E886DA1A11FCDD7F00DA9F641FD142DB8404A3_RuntimeMethod_var);
		NullCheck(L_6);
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_7;
		L_7 = Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D_inline(L_6, NULL);
		return L_7;
	}

IL_0029:
	{
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_8, 1));
	}

IL_002d:
	{
		// for (int i = 0; i < _transitions.Count; i++)
		int32_t L_9 = V_0;
		List_1_t2FF20785F3441EAC2441BE8E3C7C80C3CA0E4EDF* L_10 = __this->____transitions_4;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_inline(L_10, List_1_get_Count_m7D1CF2BC556AFDA28DF474776E850BCAA7621D0F_RuntimeMethod_var);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0004;
		}
	}
	{
		// return null;
		return (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93*)NULL;
	}
}
// System.Void KickMaster.StateMachine.State::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06 (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.DieState::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DieState_OnEnable_m381458E2E37ACFBFB89D3A903A61EA4E3E4AD73B (DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral08EF1409295A748F17C961491378A3B6215AA838);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator.SetTrigger("Die");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->____animator_7;
		NullCheck(L_0);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_0, _stringLiteral08EF1409295A748F17C961491378A3B6215AA838, NULL);
		// StartCoroutine(DieWithDelay(_animationDelay));
		RuntimeObject* L_1;
		L_1 = DieState_DieWithDelay_mBC37AC486AD23B02A6CB6F553592B07C792CE6F5(__this, (4.0f), NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_2;
		L_2 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_1, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator KickMaster.StateMachine.DieState::DieWithDelay(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DieState_DieWithDelay_mBC37AC486AD23B02A6CB6F553592B07C792CE6F5 (DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* __this, float ___delay0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* L_0 = (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0*)il2cpp_codegen_object_new(U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CDieWithDelayU3Ed__3__ctor_m3F40ED80C5889B224BCC341C9877B2E5648EB663(L_0, 0, NULL);
		U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_3), (void*)__this);
		U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* L_2 = L_1;
		float L_3 = ___delay0;
		NullCheck(L_2);
		L_2->___delay_2 = L_3;
		return L_2;
	}
}
// System.Void KickMaster.StateMachine.DieState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DieState__ctor_m6753595B421250F06F5285DFEB0616CDADC97A8D (DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* __this, const RuntimeMethod* method) 
{
	{
		State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDieWithDelayU3Ed__3__ctor_m3F40ED80C5889B224BCC341C9877B2E5648EB663 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDieWithDelayU3Ed__3_System_IDisposable_Dispose_m491E338DF924690B9C5BD63FD728D6014E2BE956 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean KickMaster.StateMachine.DieState/<DieWithDelay>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDieWithDelayU3Ed__3_MoveNext_m2416D6D8DB88F7CC482431DCAC0E0FE4D92044F0 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* L_1 = __this->___U3CU3E4__this_3;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForSeconds(delay);
		float L_4 = __this->___delay_2;
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_5 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_5, L_4, NULL);
		__this->___U3CU3E2__current_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_5);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// GetComponent<Enemy>().Die();
		DieState_tAD8F1575B2F3089067A1721AC8D1B5E40FD598F3* L_6 = V_1;
		NullCheck(L_6);
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_7;
		L_7 = Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4(L_6, Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		NullCheck(L_7);
		VirtualActionInvoker0::Invoke(4 /* System.Void KickMaster.Enemy::Die() */, L_7);
		// }
		return (bool)0;
	}
}
// System.Object KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDieWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52DD6C9E4AE18BC10E8DD824B6863010C89F9AF1 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mCBEE65D1315F7F38767FA29FF57029BE8C1EDC4C (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mCBEE65D1315F7F38767FA29FF57029BE8C1EDC4C_RuntimeMethod_var)));
	}
}
// System.Object KickMaster.StateMachine.DieState/<DieWithDelay>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CDieWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_m2DF42E1A0078E5E302AE36FBEA8AA85FAEB3BBE4 (U3CDieWithDelayU3Ed__3_t852E63AECBEE9D9F92769DD0EBE091775E32F7D0* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.IdleState::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IdleState_OnEnable_mA4C3B06BA33823C91DCDF5C157EDC73AB8A715C4 (IdleState_t58E9FE55FF2340984CDD43A38A3FB6BDF803CA68* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.IdleState::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IdleState_OnDisable_m89BD6D412DD387A95018F318318F7513CB20D6AF (IdleState_t58E9FE55FF2340984CDD43A38A3FB6BDF803CA68* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.IdleState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IdleState__ctor_m5D357B6ED2202AD207D2779C2C3A4253EF1EE1A7 (IdleState_t58E9FE55FF2340984CDD43A38A3FB6BDF803CA68* __this, const RuntimeMethod* method) 
{
	{
		State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.KillState::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KillState_OnEnable_m3B70C28E06E9BCD56B2B275DEF32C283631FBD8B (KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51D71BFB388793426FCAD75AE8863C67386257EE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator.SetTrigger("Kick");
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->____animator_7;
		NullCheck(L_0);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_0, _stringLiteral51D71BFB388793426FCAD75AE8863C67386257EE, NULL);
		// StartCoroutine(KillWithDelay(_animationDelay));
		RuntimeObject* L_1;
		L_1 = KillState_KillWithDelay_mA5485FB293E5DE76F1758611233AB762CFF6D1F7(__this, (1.0f), NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_2;
		L_2 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_1, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator KickMaster.StateMachine.KillState::KillWithDelay(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* KillState_KillWithDelay_mA5485FB293E5DE76F1758611233AB762CFF6D1F7 (KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* __this, float ___delay0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* L_0 = (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4*)il2cpp_codegen_object_new(U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CKillWithDelayU3Ed__3__ctor_mF0D47A051DBE7501949B51D317BA36AAF27ADDBC(L_0, 0, NULL);
		U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_3), (void*)__this);
		U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* L_2 = L_1;
		float L_3 = ___delay0;
		NullCheck(L_2);
		L_2->___delay_2 = L_3;
		return L_2;
	}
}
// System.Void KickMaster.StateMachine.KillState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KillState__ctor_m7BF76288D350ECD0139832398A106BED945366D5 (KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* __this, const RuntimeMethod* method) 
{
	{
		State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithDelayU3Ed__3__ctor_mF0D47A051DBE7501949B51D317BA36AAF27ADDBC (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithDelayU3Ed__3_System_IDisposable_Dispose_m819CC3C003E5B4E484AE835447713704AC97C702 (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean KickMaster.StateMachine.KillState/<KillWithDelay>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CKillWithDelayU3Ed__3_MoveNext_mC3B66CEA386DD1C163820461D0E5691B0F1F1457 (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* L_1 = __this->___U3CU3E4__this_3;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForSeconds(delay);
		float L_4 = __this->___delay_2;
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_5 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_5, L_4, NULL);
		__this->___U3CU3E2__current_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_5);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0038:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// Target.Kill();
		KillState_tCBAA5FDB5D084F2BE831054D4C7B9324A10EB632* L_6 = V_1;
		NullCheck(L_6);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_7;
		L_7 = State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline(L_6, NULL);
		NullCheck(L_7);
		Player_Kill_mD2AACC78D1E870C57334FB25CBA424C9CB984025(L_7, NULL);
		// }
		return (bool)0;
	}
}
// System.Object KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CKillWithDelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4CCF15133F9BAB2E1CF858034D74ED7DBC3EE8C5 (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mD4DB2864CA12AB3E6E02E13C81DF1C67A2002FC7 (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_Reset_mD4DB2864CA12AB3E6E02E13C81DF1C67A2002FC7_RuntimeMethod_var)));
	}
}
// System.Object KickMaster.StateMachine.KillState/<KillWithDelay>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CKillWithDelayU3Ed__3_System_Collections_IEnumerator_get_Current_mC1FAA251C1DCCA428B3595CA81FC4D1A8D37A28B (U3CKillWithDelayU3Ed__3_tC521EC703E74C987BE978D31E7C101C3BF294EB4* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.MoveState::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveState_OnEnable_m55085530231E907068A5C4999C4818C5127D46DB (MoveState_tB658AA74A33C09F34C680910AE3A80B959935CA0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral926A646AB5A68B4F9CE13C5B42BF00FE91E8C2D9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator.SetBool("IsWalking", true);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_0 = __this->____animator_7;
		NullCheck(L_0);
		Animator_SetBool_m6F8D4FAF0770CD4EC1F54406249785DE7391E42B(L_0, _stringLiteral926A646AB5A68B4F9CE13C5B42BF00FE91E8C2D9, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.MoveState::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveState_Update_mE53404BDC133FC421DDCFBFEA0E4F1D66C79AB06 (MoveState_tB658AA74A33C09F34C680910AE3A80B959935CA0* __this, const RuntimeMethod* method) 
{
	{
		// transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, _speed * Time.deltaTime);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_1, NULL);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_3;
		L_3 = State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline(__this, NULL);
		NullCheck(L_3);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_3, NULL);
		NullCheck(L_4);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_4, NULL);
		float L_6 = __this->____speed_6;
		float L_7;
		L_7 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline(L_2, L_5, ((float)il2cpp_codegen_multiply(L_6, L_7)), NULL);
		NullCheck(L_0);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_0, L_8, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.MoveState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveState__ctor_m75A267FEA996A0BB96A9853035320AABA5EA0A53 (MoveState_tB658AA74A33C09F34C680910AE3A80B959935CA0* __this, const RuntimeMethod* method) 
{
	{
		State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.ThrowingState::add_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_add_ProjectileCreated_m393F0C3F7147E89601AE303379137A2922597AE0 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_1 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_2 = NULL;
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_0 = __this->___ProjectileCreated_7;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_2 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m8B9D24CED35033C7FC56501DFE650F5CB7FF012C(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var));
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1** L_5 = (&__this->___ProjectileCreated_7);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_6 = V_2;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_7 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_9 = V_0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_9) == ((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState::remove_ProjectileCreated(UnityEngine.Events.UnityAction`1<KickMaster.Projectile>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_remove_ProjectileCreated_mCC5531DCD64836A19173BCE84635F2FD5EFDE086 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* ___value0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_1 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* V_2 = NULL;
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_0 = __this->___ProjectileCreated_7;
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_1 = V_0;
		V_1 = L_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_2 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_3 = ___value0;
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m40506877934EC1AD4ADAE57F5E97AF0BC0F96116(L_2, L_3, NULL);
		V_2 = ((UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)Castclass((RuntimeObject*)L_4, UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1_il2cpp_TypeInfo_var));
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1** L_5 = (&__this->___ProjectileCreated_7);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_6 = V_2;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_7 = V_1;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_8;
		L_8 = InterlockedCompareExchangeImpl<UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*>(L_5, L_6, L_7);
		V_0 = L_8;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_9 = V_0;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_10 = V_1;
		if ((!(((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_9) == ((RuntimeObject*)(UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1*)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_OnEnable_mD110C9DD6474AB919B9FD71322070D9CD0660144 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) 
{
	{
		// _throwCoroutine = StartCoroutine(StartThrowing());
		RuntimeObject* L_0;
		L_0 = ThrowingState_StartThrowing_m30EADAC80898A10EA73D3345393788483FB1C95E(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_1;
		L_1 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_0, NULL);
		__this->____throwCoroutine_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____throwCoroutine_13), (void*)L_1);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_OnDisable_m20AB89A72C3239391A27A6D8AD3B28ABE5FE2570 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) 
{
	{
		// if (_throwCoroutine != null)
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_0 = __this->____throwCoroutine_13;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		// StopCoroutine(_throwCoroutine);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_1 = __this->____throwCoroutine_13;
		MonoBehaviour_StopCoroutine_mB0FC91BE84203BD8E360B3FBAE5B958B4C5ED22A(__this, L_1, NULL);
		// _throwCoroutine = null;
		__this->____throwCoroutine_13 = (Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____throwCoroutine_13), (void*)(Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B*)NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator KickMaster.StateMachine.ThrowingState::StartThrowing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ThrowingState_StartThrowing_m30EADAC80898A10EA73D3345393788483FB1C95E (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* L_0 = (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C*)il2cpp_codegen_object_new(U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CStartThrowingU3Ed__12__ctor_mF45E77C4F928D1D962A53C49DF89DA1B9424C263(L_0, 0, NULL);
		U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// KickMaster.Projectile KickMaster.StateMachine.ThrowingState::CreateProjectile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ThrowingState_CreateProjectile_m0C9DE1A31F8FF958DB30D428DF7679343F1821FB (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_m74F486DA40756F8EFD2B5EAE87CB856E511F4BF9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* V_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* G_B2_0 = NULL;
	UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* G_B1_0 = NULL;
	{
		// Projectile projectile = Instantiate(_projectilePrefab, _holdPoint.position, Quaternion.identity);
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_0 = __this->____projectilePrefab_8;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1 = __this->____holdPoint_10;
		NullCheck(L_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_1, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_3;
		L_3 = Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline(NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_4;
		L_4 = Object_Instantiate_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_m74F486DA40756F8EFD2B5EAE87CB856E511F4BF9(L_0, L_2, L_3, Object_Instantiate_TisProjectile_tFABB875FEB76E663B789217417E7E80D76158DC3_m74F486DA40756F8EFD2B5EAE87CB856E511F4BF9_RuntimeMethod_var);
		V_0 = L_4;
		// ProjectileCreated?.Invoke(projectile);
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_5 = __this->___ProjectileCreated_7;
		UnityAction_1_tB2DB96AE500E80CA352A4AB39551D5BC0B23D2C1* L_6 = L_5;
		G_B1_0 = L_6;
		if (L_6)
		{
			G_B2_0 = L_6;
			goto IL_0028;
		}
	}
	{
		goto IL_002e;
	}

IL_0028:
	{
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_7 = V_0;
		NullCheck(G_B2_0);
		UnityAction_1_Invoke_mC1007382E3AC52C15BE12F47ADE7E9951F6559F1_inline(G_B2_0, L_7, NULL);
	}

IL_002e:
	{
		// return projectile;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_8 = V_0;
		return L_8;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState::ThrowProjectile(KickMaster.Projectile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState_ThrowProjectile_mEA5D18F8A1AA4ED87A1ADB5A7569B20CB8385793 (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* ___projectile0, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 forceDirection = Target.transform.position - _holdPoint.transform.position + Vector3.up;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0;
		L_0 = State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline(__this, NULL);
		NullCheck(L_0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_0, NULL);
		NullCheck(L_1);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_1, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3 = __this->____holdPoint_10;
		NullCheck(L_3);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_3, NULL);
		NullCheck(L_4);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_4, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline(L_2, L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline(L_6, L_7, NULL);
		V_0 = L_8;
		// Vector3 forceValue = forceDirection.normalized * Vector3.Distance(Target.transform.position, _holdPoint.transform.position);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline((&V_0), NULL);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_10;
		L_10 = State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline(__this, NULL);
		NullCheck(L_10);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11;
		L_11 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_10, NULL);
		NullCheck(L_11);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_11, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_13 = __this->____holdPoint_10;
		NullCheck(L_13);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14;
		L_14 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_13, NULL);
		NullCheck(L_14);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_14, NULL);
		float L_16;
		L_16 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_12, L_15, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_17;
		L_17 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_9, L_16, NULL);
		V_1 = L_17;
		// projectile.Throw(forceValue * _force);
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_18 = ___projectile0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19 = V_1;
		float L_20 = __this->____force_11;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_19, L_20, NULL);
		NullCheck(L_18);
		VirtualActionInvoker1< Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 >::Invoke(4 /* System.Void KickMaster.Projectile::Throw(UnityEngine.Vector3) */, L_18, L_21);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowingState__ctor_m754FEDDAB0F8715293A12F43FD64637AFE744A2B (ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* __this, const RuntimeMethod* method) 
{
	{
		State__ctor_m93603AC4092949EF8B51C1BF3E8E16F0FE6A6A06(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartThrowingU3Ed__12__ctor_mF45E77C4F928D1D962A53C49DF89DA1B9424C263 (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartThrowingU3Ed__12_System_IDisposable_Dispose_mF8F2EE26B5D9F53BAA209A3827304D3FC900FE92 (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartThrowingU3Ed__12_MoveNext_mFCA3E740B4019425EA3565D5A91A7ACB93A83B1E (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* V_1 = NULL;
	Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* V_2 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0049;
			}
			case 2:
			{
				goto IL_0079;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_0029:
	{
		// yield return new WaitForSeconds(_delay - animationDelay);
		ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_3 = V_1;
		NullCheck(L_3);
		float L_4 = L_3->____delay_9;
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_5 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_5);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_5, ((float)il2cpp_codegen_subtract(L_4, (0.800000012f))), NULL);
		__this->___U3CU3E2__current_1 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_5);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_0049:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// _animator.SetTrigger("Throw");
		ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_6 = V_1;
		NullCheck(L_6);
		Animator_t8A52E42AE54F76681838FE9E632683EF3952E883* L_7 = L_6->____animator_12;
		NullCheck(L_7);
		Animator_SetTrigger_mC9CD54D627C8843EF6E159E167449D216EF6EB30(L_7, _stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351, NULL);
		// yield return new WaitForSeconds(animationDelay);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_8 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_8);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_8, (0.800000012f), NULL);
		__this->___U3CU3E2__current_1 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_8);
		__this->___U3CU3E1__state_0 = 2;
		return (bool)1;
	}

IL_0079:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// Projectile projectile = CreateProjectile();
		ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_9 = V_1;
		NullCheck(L_9);
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_10;
		L_10 = ThrowingState_CreateProjectile_m0C9DE1A31F8FF958DB30D428DF7679343F1821FB(L_9, NULL);
		V_2 = L_10;
		// ThrowProjectile(projectile);
		ThrowingState_tA27B86A415BCE33189549CDF2676BF545B45C4CC* L_11 = V_1;
		Projectile_tFABB875FEB76E663B789217417E7E80D76158DC3* L_12 = V_2;
		NullCheck(L_11);
		ThrowingState_ThrowProjectile_mEA5D18F8A1AA4ED87A1ADB5A7569B20CB8385793(L_11, L_12, NULL);
		// while (true)
		goto IL_0029;
	}
}
// System.Object KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CStartThrowingU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FF01EF5E97F718AF309CAE0D1723385D7F3A18F (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_Reset_m14CE989A29945418838C62997F132A55F1EC0375 (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_Reset_m14CE989A29945418838C62997F132A55F1EC0375_RuntimeMethod_var)));
	}
}
// System.Object KickMaster.StateMachine.ThrowingState/<StartThrowing>d__12::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CStartThrowingU3Ed__12_System_Collections_IEnumerator_get_Current_m8E96F4A731B971898D69AAA7494B129219CA02A8 (U3CStartThrowingU3Ed__12_tA2D716AD2D80D95E6BD0045B6E5F676AD0611D1C* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// KickMaster.Player KickMaster.StateMachine.Transition::get_Target()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->___U3CTargetU3Ek__BackingField_5;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.Transition::set_Target(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = ___value0;
		__this->___U3CTargetU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CTargetU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
// KickMaster.Enemy KickMaster.StateMachine.Transition::get_Enemy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// protected Enemy Enemy { get; private set; }
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0 = __this->___U3CEnemyU3Ek__BackingField_6;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.Transition::set_Enemy(KickMaster.Enemy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Enemy Enemy { get; private set; }
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0 = ___value0;
		__this->___U3CEnemyU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CEnemyU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
// KickMaster.StateMachine.State KickMaster.StateMachine.Transition::get_TargetState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// public State TargetState => _targetState;
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____targetState_4;
		return L_0;
	}
}
// System.Boolean KickMaster.StateMachine.Transition::get_NeedTransit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// public bool NeedTransit { get; protected set; }
		bool L_0 = __this->___U3CNeedTransitU3Ek__BackingField_7;
		return L_0;
	}
}
// System.Void KickMaster.StateMachine.Transition::set_NeedTransit(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool NeedTransit { get; protected set; }
		bool L_0 = ___value0;
		__this->___U3CNeedTransitU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Void KickMaster.StateMachine.Transition::Init(KickMaster.Player)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_Init_m3735DD0482CD276E2D9D25FF21691DA308A116A0 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___target0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Target = target;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = ___target0;
		Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216_inline(__this, L_0, NULL);
		// Enemy = GetComponent<Enemy>();
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_1;
		L_1 = Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4(__this, Component_GetComponent_TisEnemy_t2AF061A7E551D4EE1CFD2748010D67B280620584_m703BEBB1966DDC1952BE5B4FC59B1E82859118B4_RuntimeMethod_var);
		Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8_inline(__this, L_1, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.Transition::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition_OnEnable_m6AF3C89201035C25B2D0BFFF99F88FE8899CB2B4 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// NeedTransit = false;
		Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline(__this, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.Transition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482 (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.DetectionTransition::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DetectionTransition_Update_m58A3C0047D22C0E812D5FB76FF1DD42DB3AF6885 (DetectionTransition_tBEE948ABDBDFD2F492ACAD4A9D093339769038D8* __this, const RuntimeMethod* method) 
{
	{
		// if (Vector3.Distance(transform.position, Target.transform.position) <= _targetDetectionDistance)
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_2;
		L_2 = Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4_inline(__this, NULL);
		NullCheck(L_2);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_2, NULL);
		NullCheck(L_3);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_3, NULL);
		float L_5;
		L_5 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_1, L_4, NULL);
		float L_6 = __this->____targetDetectionDistance_8;
		if ((!(((float)L_5) <= ((float)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		// NeedTransit = true;
		Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline(__this, (bool)1, NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.DetectionTransition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DetectionTransition__ctor_mE8A9C454E703208425D65AC19FEBFFEAC36A082F (DetectionTransition_tBEE948ABDBDFD2F492ACAD4A9D093339769038D8* __this, const RuntimeMethod* method) 
{
	{
		Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.DieTransition::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DieTransition_Update_m43C77F1C2B998BCF75C3BFCF82B1470F09E80F90 (DieTransition_tA7FF60FF626EE5BE5A9E6EBF5243BC9EC6DE7684* __this, const RuntimeMethod* method) 
{
	{
		// if (Enemy.Health <= 0)
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0;
		L_0 = Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40_inline(__this, NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68_inline(L_0, NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		// NeedTransit = true;
		Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline(__this, (bool)1, NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.DieTransition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DieTransition__ctor_m7F6F7AB1F55BB8B108BDF0481ED3C8A70014CE2E (DieTransition_tA7FF60FF626EE5BE5A9E6EBF5243BC9EC6DE7684* __this, const RuntimeMethod* method) 
{
	{
		Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.KillTransition::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KillTransition_Start_mD1324118480434BD696BAE1943FB750AA5BD1D13 (KillTransition_t681FD7B428DEEF8F9F72FDC0B203922F7E7D8BFA* __this, const RuntimeMethod* method) 
{
	{
		// _transitionRange += Random.Range(-_spreadRange, _spreadRange);
		float L_0 = __this->____transitionRange_8;
		float L_1 = __this->____spreadRange_9;
		float L_2 = __this->____spreadRange_9;
		float L_3;
		L_3 = Random_Range_mF26F26EB446B76823B4815C91FA0907B484DF02B(((-L_1)), L_2, NULL);
		__this->____transitionRange_8 = ((float)il2cpp_codegen_add(L_0, L_3));
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.KillTransition::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KillTransition_Update_m10C19A1A0C55BE6926E0C4F40D35811C8F6B8E15 (KillTransition_t681FD7B428DEEF8F9F72FDC0B203922F7E7D8BFA* __this, const RuntimeMethod* method) 
{
	{
		// if (Vector3.Distance(transform.position, Target.transform.position) < _transitionRange)
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_0);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_0, NULL);
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_2;
		L_2 = Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4_inline(__this, NULL);
		NullCheck(L_2);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_2, NULL);
		NullCheck(L_3);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_3, NULL);
		float L_5;
		L_5 = Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline(L_1, L_4, NULL);
		float L_6 = __this->____transitionRange_8;
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		// NeedTransit = true;
		Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline(__this, (bool)1, NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.KillTransition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void KillTransition__ctor_m89F4DCDB15CCA6F1DDA856B1FACD8421FF204879 (KillTransition_t681FD7B428DEEF8F9F72FDC0B203922F7E7D8BFA* __this, const RuntimeMethod* method) 
{
	{
		Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void KickMaster.StateMachine.TargetDieTransition::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetDieTransition_Update_m04E4872FF447999033E9875E403166646B16D5FE (TargetDieTransition_t0935666A05E1F13EAAB8A8DEF693D2FCB5050637* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Target == null)
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0;
		L_0 = Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4_inline(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// NeedTransit = true;
		Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline(__this, (bool)1, NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void KickMaster.StateMachine.TargetDieTransition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetDieTransition__ctor_m7935B940CAF806DEC4C66F878357289A7292EEB8 (TargetDieTransition_t0935666A05E1F13EAAB8A8DEF693D2FCB5050637* __this, const RuntimeMethod* method) 
{
	{
		Transition__ctor_m6B24D3DDC8E970E9CEBE4FDE0251565F8A45D482(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_forward_mEBAB24D77FC02FC88ED880738C3B1D47C758B3EB_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___forwardVector_11;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___rightVector_10;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Addition_m087D6F0EC60843D455F9F83D25FE42B2433AAD1D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), ((float)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_AxisOptions_m541692280806ECF76F7C2C710973AF9D8AB334C6_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// public AxisOptions AxisOptions { get { return AxisOptions; } set { axisOptions = value; } }
		int32_t L_0 = ___value0;
		__this->___axisOptions_6 = L_0;
		// public AxisOptions AxisOptions { get { return AxisOptions; } set { axisOptions = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_SnapX_mB2090989F6AC933B30823751D74E799FC8D9B87A_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool SnapX { get { return snapX; } set { snapX = value; } }
		bool L_0 = ___value0;
		__this->___snapX_7 = L_0;
		// public bool SnapX { get { return snapX; } set { snapX = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Joystick_set_SnapY_m7419D5EB972285A9E5E446CD668BEC266D11CD86_inline (Joystick_tE3193C48A43E3F5577CBD4E9A8204BBFF2FEEB7A* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool SnapY { get { return snapY; } set { snapY = value; } }
		bool L_0 = ___value0;
		__this->___snapY_8 = L_0;
		// public bool SnapY { get { return snapY; } set { snapY = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___zeroVector_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Division_m69F64D545E3C023BE9927397572349A569141EBA_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		float L_2 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___a0;
		float L_4 = L_3.___y_1;
		float L_5 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)(L_1/L_2)), ((float)(L_4/L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 PointerEventData_get_position_m5BE71C28EB72EFB8435749E4E6E839213AEF458C_inline (PointerEventData_t9670F3C7D823CCB738A1604C72A1EB90292396FB* __this, const RuntimeMethod* method) 
{
	{
		// public Vector2 position { get; set; }
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = __this->___U3CpositionU3Ek__BackingField_13;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Subtraction_m664419831773D5BBF06D9DE4E515F6409B2F92B8_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m4EEB2FF3F4830390A53CE9B6076FB31801D65EED_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		float L_2 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = ___a0;
		float L_4 = L_3.___y_1;
		float L_5 = ___d1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Division_mB1CA903ACF933DB0BE2016D105BB2B4702CF1004_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)(L_1/L_3)), ((float)(L_5/L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m5C59B4056420AEFDB291AD0914A3F675330A75CE_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_0;
		float L_1 = __this->___x_0;
		float L_2 = __this->___y_1;
		float L_3 = __this->___y_1;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_4;
		L_4 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3))))));
		V_0 = ((float)L_4);
		goto IL_0026;
	}

IL_0026:
	{
		float L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_normalized_mF6722883AEFB5027690A778DF8ACC20F0FA65297_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		float L_0 = __this->___x_0;
		float L_1 = __this->___y_1;
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&V_0), L_0, L_1, NULL);
		Vector2_Normalize_m56DABCAB5967DF37A6B96710477D3660D800C652_inline((&V_0), NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = V_0;
		V_1 = L_2;
		goto IL_0020;
	}

IL_0020:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = V_1;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Multiply_m6158066AFB36810D63D98EABF0FABEFFC647B2A0_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_up_mF4D6DB00DEA7D055940165B85FFE1CEF6F7CD3AA_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___upVector_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Angle_m9668B13074D1664DD192669C14B3A8FC01676299_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___from0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___to1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	float V_3 = 0.0f;
	{
		float L_0;
		L_0 = Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline((&___from0), NULL);
		float L_1;
		L_1 = Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline((&___to1), NULL);
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_2;
		L_2 = sqrt(((double)((float)il2cpp_codegen_multiply(L_0, L_1))));
		V_0 = ((float)L_2);
		float L_3 = V_0;
		V_2 = (bool)((((float)L_3) < ((float)(1.0E-15f)))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		V_3 = (0.0f);
		goto IL_0056;
	}

IL_002c:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = ___from0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___to1;
		float L_7;
		L_7 = Vector2_Dot_mBF0FA0B529C821F4733DDC3AD366B07CD27625F4_inline(L_5, L_6, NULL);
		float L_8 = V_0;
		float L_9;
		L_9 = Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline(((float)(L_7/L_8)), (-1.0f), (1.0f), NULL);
		V_1 = L_9;
		float L_10 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_11;
		L_11 = acos(((double)L_10));
		V_3 = ((float)il2cpp_codegen_multiply(((float)L_11), (57.2957802f)));
		goto IL_0056;
	}

IL_0056:
	{
		float L_12 = V_3;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Addition_m704B5B98EAFE885978381E21B7F89D9DF83C2A60_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___a0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___b1, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___a0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___b1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___a0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___b1;
		float L_7 = L_6.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_8), ((float)il2cpp_codegen_add(L_1, L_3)), ((float)il2cpp_codegen_add(L_5, L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_3, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93(L_4, NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Subtraction_m1690F44F6DC92B770A940B6CF8AE0535625A9824_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_12), ((float)il2cpp_codegen_subtract(L_1, L_3)), ((float)il2cpp_codegen_subtract(L_5, L_7)), ((float)il2cpp_codegen_subtract(L_9, L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___upVector_7;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_m4688A1A524306675DBDB1E6D483F35E85E3CE6D8_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___lhs0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___rhs1;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___lhs0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___rhs1;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___lhs0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___rhs1;
		float L_11 = L_10.___z_4;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Lerp_mFB4910B358B986AFB22114ED90458E8341867479_inline (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		float L_4;
		L_4 = Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline(L_3, NULL);
		V_0 = ((float)il2cpp_codegen_add(L_0, ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_subtract(L_1, L_2)), L_4))));
		goto IL_0010;
	}

IL_0010:
	{
		float L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mF0D6017E90B345F1F52D1CC564C640F1A847AF2D_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_2;
		float L_1 = __this->___x_2;
		float L_2 = __this->___y_3;
		float L_3 = __this->___y_3;
		float L_4 = __this->___z_4;
		float L_5 = __this->___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_6;
		L_6 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3)))), ((float)il2cpp_codegen_multiply(L_4, L_5))))));
		V_0 = ((float)L_6);
		goto IL_0034;
	}

IL_0034:
	{
		float L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_normalized_m736BBF65D5CDA7A18414370D15B4DFCC1E466F07_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = (*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_Normalize_m6120F119433C5B60BBB28731D3D4A0DA50A84DDD_inline(L_0, NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___zeroVector_5;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m6A7FB1C9E9DE194708997BFA24C6E238D92D908E_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___lhs0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = ___rhs1;
		bool L_2;
		L_2 = Vector3_op_Equality_m15951D1B53E3BE36C9D265E229090020FBD72EBB_inline(L_0, L_1, NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityAction_Invoke_m5CB9EE17CCDF64D00DE5D96DF3553CDB20D66F70_inline (UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* __this, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Distance_m99C722723EDD875852EF854AD7B7C4F8AC4F84AB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___b1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___b1;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___a0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___b1;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___a0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___b1;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_18;
		L_18 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))))));
		V_3 = ((float)L_18);
		goto IL_0040;
	}

IL_0040:
	{
		float L_19 = V_3;
		return L_19;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___lhs0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___rhs1;
		bool L_2;
		L_2 = Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline(L_0, L_1, NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Enemy_get_Target_m6FE97A89AF6AD417174E8F127436528FC0586294_inline (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	{
		// public Player Target => _target;
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->____target_6;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void State_set_Target_m61B3AB0993443F00949A57539054B5FF0E5096D0_inline (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = ___value0;
		__this->___U3CTargetU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CTargetU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* State_get_Target_mF7585FEFD64B6D65E92C6A78A764AFF1701D4D6F_inline (State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* __this, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->___U3CTargetU3Ek__BackingField_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Transition_get_NeedTransit_m91BCA89DBDC471ACDC2B84570D4C77415F45DF7B_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// public bool NeedTransit { get; protected set; }
		bool L_0 = __this->___U3CNeedTransitU3Ek__BackingField_7;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* Transition_get_TargetState_mBA77B3DAB7DD57A96AFA0F1953CCC03FAD05211D_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// public State TargetState => _targetState;
		State_tBD7746B91D0BBE06C324920DA0C0E58C23D24C93* L_0 = __this->____targetState_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_MoveTowards_m3E2E4E94093F49D09DEB34CA97BF8A632B27F1AD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___current0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	bool V_5 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_6;
	memset((&V_6), 0, sizeof(V_6));
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___target1;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___current0;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___target1;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___current0;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___target1;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___current0;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		V_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))));
		float L_18 = V_3;
		if ((((float)L_18) == ((float)(0.0f))))
		{
			goto IL_0055;
		}
	}
	{
		float L_19 = ___maxDistanceDelta2;
		if ((!(((float)L_19) >= ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		float L_20 = V_3;
		float L_21 = ___maxDistanceDelta2;
		float L_22 = ___maxDistanceDelta2;
		G_B4_0 = ((((int32_t)((!(((float)L_20) <= ((float)((float)il2cpp_codegen_multiply(L_21, L_22)))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0053;
	}

IL_0052:
	{
		G_B4_0 = 0;
	}

IL_0053:
	{
		G_B6_0 = G_B4_0;
		goto IL_0056;
	}

IL_0055:
	{
		G_B6_0 = 1;
	}

IL_0056:
	{
		V_5 = (bool)G_B6_0;
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_24 = ___target1;
		V_6 = L_24;
		goto IL_009b;
	}

IL_0061:
	{
		float L_25 = V_3;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_26;
		L_26 = sqrt(((double)L_25));
		V_4 = ((float)L_26);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_27 = ___current0;
		float L_28 = L_27.___x_2;
		float L_29 = V_0;
		float L_30 = V_4;
		float L_31 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32 = ___current0;
		float L_33 = L_32.___y_3;
		float L_34 = V_1;
		float L_35 = V_4;
		float L_36 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37 = ___current0;
		float L_38 = L_37.___z_4;
		float L_39 = V_2;
		float L_40 = V_4;
		float L_41 = ___maxDistanceDelta2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_42;
		memset((&L_42), 0, sizeof(L_42));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_42), ((float)il2cpp_codegen_add(L_28, ((float)il2cpp_codegen_multiply(((float)(L_29/L_30)), L_31)))), ((float)il2cpp_codegen_add(L_33, ((float)il2cpp_codegen_multiply(((float)(L_34/L_35)), L_36)))), ((float)il2cpp_codegen_add(L_38, ((float)il2cpp_codegen_multiply(((float)(L_39/L_40)), L_41)))), /*hidden argument*/NULL);
		V_6 = L_42;
		goto IL_009b;
	}

IL_009b:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_43 = V_6;
		return L_43;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ((Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var))->___identityQuaternion_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_Target_m06E342FC6E91C482BBB747F3BE966A2C73421216_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = ___value0;
		__this->___U3CTargetU3Ek__BackingField_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CTargetU3Ek__BackingField_5), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_Enemy_m7D2FE7F63EF05064D395FF2D5D24B978B2B331F8_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* ___value0, const RuntimeMethod* method) 
{
	{
		// protected Enemy Enemy { get; private set; }
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0 = ___value0;
		__this->___U3CEnemyU3Ek__BackingField_6 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CEnemyU3Ek__BackingField_6), (void*)L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Transition_set_NeedTransit_m310FB664ABF528FC10950E227EBCAA498EAD3819_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// public bool NeedTransit { get; protected set; }
		bool L_0 = ___value0;
		__this->___U3CNeedTransitU3Ek__BackingField_7 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* Transition_get_Target_m870FD948AB67568072D580C1B71219CD9EBB0BB4_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// protected Player Target { get; private set; }
		Player_t067384CFB5F162DE304E87CFD1FFFD42C2E4CED5* L_0 = __this->___U3CTargetU3Ek__BackingField_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* Transition_get_Enemy_mF205C1E934D3B7CE23EDCC1E188B95752CCF9C40_inline (Transition_t1A6F98FBF90E040061D43D6181BC8188CED00A0F* __this, const RuntimeMethod* method) 
{
	{
		// protected Enemy Enemy { get; private set; }
		Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* L_0 = __this->___U3CEnemyU3Ek__BackingField_6;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enemy_get_Health_m388A26EA1F9F9852A3A7A79937D64C3FE6832E68_inline (Enemy_t2AF061A7E551D4EE1CFD2748010D67B280620584* __this, const RuntimeMethod* method) 
{
	{
		// public int Health => _health;
		int32_t L_0 = __this->____health_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_m55011E8360A8199FB239A5787BA8631CDD6116FC_gshared_inline (const RuntimeMethod* method) 
{
	{
		il2cpp_codegen_runtime_class_init_inline(il2cpp_rgctx_data(method->rgctx_data, 0));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_0 = ((EmptyArray_1_tDF0DD7256B115243AA6BD5558417387A734240EE_StaticFields*)il2cpp_codegen_static_fields_for(il2cpp_rgctx_data(method->rgctx_data, 0)))->___Value_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UnityAction_1_Invoke_m777839BF9CB9F96B081106B47202D06FB35326CA_gshared_inline (UnityAction_1_t9C30BCD020745BF400CBACF22C6F34ADBA2DDA6A* __this, RuntimeObject* ___arg00, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg00, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2_Normalize_m56DABCAB5967DF37A6B96710477D3660D800C652_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		float L_0;
		L_0 = Vector2_get_magnitude_m5C59B4056420AEFDB291AD0914A3F675330A75CE_inline(__this, NULL);
		V_0 = L_0;
		float L_1 = V_0;
		V_1 = (bool)((((float)L_1) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_3 = (*(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7*)__this);
		float L_4 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5;
		L_5 = Vector2_op_Division_m69F64D545E3C023BE9927397572349A569141EBA_inline(L_3, L_4, NULL);
		*(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7*)__this = L_5;
		goto IL_0033;
	}

IL_0028:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		L_6 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		*(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7*)__this = L_6;
	}

IL_0033:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_get_sqrMagnitude_mA16336720C14EEF8BA9B55AE33B98C9EE2082BDC_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->___x_0;
		float L_1 = __this->___x_0;
		float L_2 = __this->___y_1;
		float L_3 = __this->___y_1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_0, L_1)), ((float)il2cpp_codegen_multiply(L_2, L_3))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector2_Dot_mBF0FA0B529C821F4733DDC3AD366B07CD27625F4_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___lhs0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___rhs1;
		float L_3 = L_2.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___lhs0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___rhs1;
		float L_7 = L_6.___y_1;
		V_0 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7))));
		goto IL_001f;
	}

IL_001f:
	{
		float L_8 = V_0;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_m154E404AF275A3B2EC99ECAA3879B4CB9F0606DC_inline (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		float L_3 = ___min1;
		___value0 = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		float L_4 = ___value0;
		float L_5 = ___max2;
		V_1 = (bool)((((float)L_4) > ((float)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___max2;
		___value0 = L_7;
	}

IL_0019:
	{
		float L_8 = ___value0;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp01_mD921B23F47F5347996C56DC789D1DE16EE27D9B1_inline (float ___value0, const RuntimeMethod* method) 
{
	bool V_0 = false;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		float L_0 = ___value0;
		V_0 = (bool)((((float)L_0) < ((float)(0.0f)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_002d;
	}

IL_0015:
	{
		float L_2 = ___value0;
		V_2 = (bool)((((float)L_2) > ((float)(1.0f)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_1 = (1.0f);
		goto IL_002d;
	}

IL_0029:
	{
		float L_4 = ___value0;
		V_1 = L_4;
		goto IL_002d;
	}

IL_002d:
	{
		float L_5 = V_1;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_Normalize_m6120F119433C5B60BBB28731D3D4A0DA50A84DDD_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	bool V_1 = false;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___value0;
		float L_1;
		L_1 = Vector3_Magnitude_m6AD0BEBF88AAF98188A851E62D7A32CB5B7830EF_inline(L_0, NULL);
		V_0 = L_1;
		float L_2 = V_0;
		V_1 = (bool)((((float)L_2) > ((float)(9.99999975E-06f)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___value0;
		float L_5 = V_0;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_op_Division_mD7200D6D432BAFC4135C5B17A0B0A812203B0270_inline(L_4, L_5, NULL);
		V_2 = L_6;
		goto IL_0026;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector3_get_zero_m9D7F7B580B5A276411267E96AA3425736D9BDC83_inline(NULL);
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = V_2;
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m15951D1B53E3BE36C9D265E229090020FBD72EBB_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___lhs0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___lhs0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___rhs1;
		float L_3 = L_2.___x_2;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___lhs0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___rhs1;
		float L_7 = L_6.___y_3;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___lhs0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___rhs1;
		float L_11 = L_10.___z_4;
		V_2 = ((float)il2cpp_codegen_subtract(L_9, L_11));
		float L_12 = V_0;
		float L_13 = V_0;
		float L_14 = V_1;
		float L_15 = V_1;
		float L_16 = V_2;
		float L_17 = V_2;
		V_3 = ((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_12, L_13)), ((float)il2cpp_codegen_multiply(L_14, L_15)))), ((float)il2cpp_codegen_multiply(L_16, L_17))));
		float L_18 = V_3;
		V_4 = (bool)((((float)L_18) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_0043;
	}

IL_0043:
	{
		bool L_19 = V_4;
		return L_19;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___lhs0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___rhs1;
		float L_3 = L_2.___x_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___lhs0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___rhs1;
		float L_7 = L_6.___y_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_8, L_9)), ((float)il2cpp_codegen_multiply(L_10, L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Magnitude_m6AD0BEBF88AAF98188A851E62D7A32CB5B7830EF_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___vector0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___vector0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___vector0;
		float L_3 = L_2.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = ___vector0;
		float L_5 = L_4.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___vector0;
		float L_7 = L_6.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8 = ___vector0;
		float L_9 = L_8.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = ___vector0;
		float L_11 = L_10.___z_4;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_12;
		L_12 = sqrt(((double)((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11))))));
		V_0 = ((float)L_12);
		goto IL_0034;
	}

IL_0034:
	{
		float L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Division_mD7200D6D432BAFC4135C5B17A0B0A812203B0270_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)(L_1/L_2)), ((float)(L_4/L_5)), ((float)(L_7/L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
